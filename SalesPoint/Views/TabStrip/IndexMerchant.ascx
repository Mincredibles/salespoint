﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<SalesPoint.Models.NavigationData>>" %>

<% 
    int position = 0;
    int selectedTabIndex = 0;
%>

<div id="tabs">
    <ul>
        <% foreach(SalesPoint.Models.NavigationData item in Model) { %>
            <% if(item.IsVisible) { %>
                <li><a href="#tabs-<%= position %>"><%: item.Text %></a></li>

                <% 
                if(item.IsContentLoadable) 
                {
                    selectedTabIndex = position;
                }
                position += 1; 
                %>
            <% } %>
        <% } %>
    </ul>

    <% position = 0; %>
    <% foreach(SalesPoint.Models.NavigationData item in Model) { %>
        <% if(item.IsVisible) { %>
            <div id="tabs-<%= position %>" style="height:0px;padding:0px;"></div>
            <% position += 1; %>
        <% } %>
    <% } %>
</div>

<div id="ActiveTabContent">
    <% foreach(SalesPoint.Models.NavigationData item in Model) {
        if(item.IsContentLoadable)
        {
            Html.RenderAction(item.ActionName, item.ControllerName, item.RouteValues);
            break;
        }
    } %>
</div>

<script type="text/javascript">
    $( document ).ready(function() {
        $("#tabs").tabs({
            active: <%= selectedTabIndex%>,
            activate: function (event, ui) {
                switch (ui.newTab.index()) {
                    <% position = 0; %>
                    <% foreach(SalesPoint.Models.NavigationData item in Model) { %>
                        <% if(item.IsVisible) { %>
                    case <%= position %>:
                        window.location.href = '<%= item.PageUrl %>';
                                break;
                                <% position += 1; %>
                                <% } %>
                                <% } %>
                }
            }
        });
    });
</script>