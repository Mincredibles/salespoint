﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>

<% SalesPoint.Core.SelectedNavigationAdministrationLeft selectedItem = (SalesPoint.Core.SelectedNavigationAdministrationLeft)ViewData["SelectedMenuItem"]; %>

<script type="text/javascript">
    $(function () {
        $("#accordion").accordion();
    });
</script>

<ul id="LeftMenu">
    <li><div class="ui-widget-header">Configuration</div></li>
    <li><%: Html.ActionLink(selectedItem == SalesPoint.Core.SelectedNavigationAdministrationLeft.SettingsAndStatus ? "Settings & Status ●" : "Settings & Status", "Edit", "Configuration")%></li>

    <li><div class="ui-widget-header">Locations</div></li>
    <li><%: Html.ActionLink(selectedItem == SalesPoint.Core.SelectedNavigationAdministrationLeft.MerchantChains ? "Chains ●" : "Chains", "Index", "MerchantChain")%></li>
    <li><%: Html.ActionLink(selectedItem == SalesPoint.Core.SelectedNavigationAdministrationLeft.MerchantCategories ? "Categories ●" : "Categories", "Index", "MerchantCategory")%></li>
    <li><%: Html.ActionLink(selectedItem == SalesPoint.Core.SelectedNavigationAdministrationLeft.StructureCategory ? "Structure Categories ●" : "Structure Categories", "Index", "StructureCategory")%></li>
    
    <li><div class="ui-widget-header">Users &amp; Groups</div></li>
    <li><%: Html.ActionLink(selectedItem == SalesPoint.Core.SelectedNavigationAdministrationLeft.Users ? "Users ●" : "Users", "Index", "User")%></li>
    <li><%: Html.ActionLink(selectedItem == SalesPoint.Core.SelectedNavigationAdministrationLeft.UserGroups ? "UserGroups ●" : "UserGroups", "Index", "UserGroup")%></li>
    <li><%: Html.ActionLink(selectedItem == SalesPoint.Core.SelectedNavigationAdministrationLeft.Roles ? "Roles ●" : "Roles", "Index", "Role")%></li>
    <li><%: Html.ActionLink(selectedItem == SalesPoint.Core.SelectedNavigationAdministrationLeft.MasoniteStoreGroups ? "Masonite Store Groups ●" : "Masonite Store Groups", "Index", "MasoniteStoreGroup")%></li>

    <li><div class="ui-widget-header">Geographical</div></li>
    <li><%: Html.ActionLink(selectedItem == SalesPoint.Core.SelectedNavigationAdministrationLeft.States ? "States ●" : "States", "Index", "GeographicalState")%></li>
    <li><%: Html.ActionLink(selectedItem == SalesPoint.Core.SelectedNavigationAdministrationLeft.Regions ? "Regions ●" : "Regions", "Index", "GeographicalRegion")%></li>
    <li><%: Html.ActionLink(selectedItem == SalesPoint.Core.SelectedNavigationAdministrationLeft.Divisions ? "Divisions ●" : "Divisions", "Index", "Division")%></li>
    <li><%: Html.ActionLink(selectedItem == SalesPoint.Core.SelectedNavigationAdministrationLeft.Districts ? "Districts ●" : "Districts", "Index", "GeographicalDistrict")%></li>

    <li><div class="ui-widget-header">Products &amp; Displays</div></li>
    <li><%: Html.ActionLink(selectedItem == SalesPoint.Core.SelectedNavigationAdministrationLeft.ProductLines ? "Product Lines ●" : "Product Lines", "Index", "ProductLine")%></li>
    <li><%: Html.ActionLink(selectedItem == SalesPoint.Core.SelectedNavigationAdministrationLeft.ProductCategories ? "Product Categories ●" : "Product Categories", "Index", "ProductCategory")%></li>
    <li><%: Html.ActionLink(selectedItem == SalesPoint.Core.SelectedNavigationAdministrationLeft.DisplayCategories ? "Display Categories ●" : "Display Categories", "Index", "DisplayCategory")%></li>
    <li><%: Html.ActionLink(selectedItem == SalesPoint.Core.SelectedNavigationAdministrationLeft.DisplayProducts ? "Display Products ●" : "Display Products", "Index", "DisplayProduct")%></li>

    <li><div class="ui-widget-header">ConfigureOne</div></li>
    <li><%: Html.ActionLink(selectedItem == SalesPoint.Core.SelectedNavigationAdministrationLeft.ConfigureOnePriceBooks ? "Price Books ●" : "Price Books", "Index", "ConfigureOnePriceBook")%></li>
    <li><%: Html.ActionLink(selectedItem == SalesPoint.Core.SelectedNavigationAdministrationLeft.ConfigureOneTimeZones ? "Time Zones ●" : "Time Zones", "Index", "ConfigureOneTimeZone")%></li>
    <li><%: Html.ActionLink(selectedItem == SalesPoint.Core.SelectedNavigationAdministrationLeft.ConfigureOneProductLines ? "Product Lines ●" : "Product Lines", "Index", "ConfigureOneProductLine")%></li>

    <li><div class="ui-widget-header">Masonite.com</div></li>
    <li><%: Html.ActionLink(selectedItem == SalesPoint.Core.SelectedNavigationAdministrationLeft.RegistrationCategory ? "Registration Categories ●" : "Registration Categories", "Index", "RegistrationCategory")%></li>
</ul>