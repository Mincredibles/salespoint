﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Masonite.MTier.SalesPoint.MerchantContact>" %>

<% using (Ajax.BeginForm("Edit", "MerchantContact", new { id = ViewData["id"], merchantId = ViewData["MerchantId"] }, new AjaxOptions { HttpMethod = "Post", LoadingElementId = "Loading", OnSuccess = "saveCompleted" })) { %>
    <table class="merchant-edit">
        <tr>
            <td valign="top">
                <div style="float:left;width:420px;">
                    <table style="width:99%;border-style:none;">
                        <tr class="ui-widget-header">
                            <td colspan="2">Details</td>
                        </tr>

                        <tr>
                            <td>First Name</td>
                            <td>
                                <%= Html.TextBoxFor(model => model.FirstName) %>
                                <%= Html.ValidationMessageFor(model => model.FirstName)%>
                            </td>
                        </tr>
                        <tr>
                            <td>Last Name<span class="required"> *</span></td>
                            <td>
                                <%= Html.TextBoxFor(model => model.LastName, new { @onblur = "Validate();" })%>
                                <%= Html.ValidationMessageFor(model => model.LastName)%>
                            </td>
                        </tr>
                        <tr><td colspan="2"><span style="display:none;" id="LastName_validationMessage" class="field-validation-error">Name is a required field. Please provide a valid value.</span></td></tr>
                        <tr>
                            <td>Job Title</td>
                            <td>
                                <%= Html.TextBoxFor(model => model.JobTitle)%>
                                <%= Html.ValidationMessageFor(model => model.JobTitle)%>
                            </td>
                        </tr>
                        <tr>
                            <td>Role<span class="required"> *</span></td>
                            <td>
                                <%= Html.DropDownListFor(model => model.RoleId, new SelectList(Masonite.MTier.SalesPoint.ObjectFactory.RetrieveRoles(), "Id", "Name", Model.RoleId), " ", new { @onblur = "Validate();" })%>
                                <%= Html.ValidationMessageFor(model => model.RoleId)%>
                            </td>
                        </tr>
                        <tr><td colspan="2"><span style="display:none;" id="RoleId_validationMessage" class="field-validation-error">Role is a required field. Please provide a valid value.</span></td></tr>
                        <tr>
                            <td>France Salesperson #</td>
                            <td>
                                <%= Html.TextBoxFor(model => model.MasoniteFranceSalespersonNumber, new { @class = "Text250" })%>
                                <%= Html.ValidationMessageFor(model => model.MasoniteFranceSalespersonNumber)%>
                            </td>
                        </tr>
                        <tr>
                            <td>Distributor Name</td>
                            <td>
                                <%= Html.TextBoxFor(model => model.DistributorName)%>
                                <%= Html.ValidationMessageFor(model => model.DistributorName)%>
                            </td>
                        </tr>
                        <tr>
                            <td>Time Zone</td>
                            <td>
                                <%= Html.DropDownListFor(model => model.ConfigureOneTimeZoneId, new SelectList(Masonite.MTier.SalesPoint.ObjectFactory.RetrieveConfigureOneTimeZones("America/"), "Id", "Name", Model.ConfigureOneTimeZoneId), " ")%>
                                <%= Html.ValidationMessageFor(model => model.ConfigureOneTimeZoneId)%>
                            </td>
                        </tr>
                        <tr>
                            <td>Phone Number</td>
                            <td>
                                <%= Html.TextBoxFor(model => model.PhoneNumber) %>
                                <%= Html.ValidationMessageFor(model => model.PhoneNumber)%>
                            </td>
                        </tr>
                        <tr>
                            <td>Mobile Number</td>
                            <td>
                                <%= Html.TextBoxFor(model => model.MobileNumber) %>
                                <%= Html.ValidationMessageFor(model => model.MobileNumber)%>
                            </td>
                        </tr>
                        <tr>
                            <td>Fax Number</td>
                            <td>
                                <%= Html.TextBoxFor(model => model.FaxNumber) %>
                                <%= Html.ValidationMessageFor(model => model.FaxNumber)%>
                            </td>
                        </tr>
                        <tr>
                            <td>Email<span class="required"> *</span></td>
                            <td>
                                <%= Html.TextBoxFor(model => model.EmailAddress, new { @onblur = "Validate();" })%>
                                <%= Html.ValidationMessageFor(model => model.EmailAddress)%>
                            </td>
                        </tr>
                        <tr><td colspan="2"><span style="display:none;" id="EmailAddress_validationMessage" class="field-validation-error">Email Address is a required field. Please provide a valid value.</span></td></tr>

                        <tr>
                            <td>Rebate Contact?</td>
                            <td><%= Html.CheckBoxFor(model => model.IsSalesRebateContact)%></td>
                        </tr>

                        <tr class="ui-widget-header">
                            <td colspan="2">Address</td>
                        </tr>
                            
                        <tr>
                            <td>Address</td>
                            <td>
                                <%= Html.TextBoxFor(model => model.Address)%> <a href="javascript:void(0);" onclick="locationAddressLookup();" id="LinkGetCompanyAddress">Use company address</a>
                                <%= Html.ValidationMessageFor(model => model.Address)%>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <%= Html.TextBoxFor(model => model.Address2)%> <img id="ImageWaitLocationLookup" alt="Retrieving location address..." title="Retrieving location address..." src="<%=Url.Content("~/Content/Images/Loading.gif")%>" style="display: none;" />
                                <%= Html.ValidationMessageFor(model => model.Address2) %>
                            </td>
                        </tr>
                        <tr>
                            <td>Postal Code</td>
                            <td>
                                <%= Html.TextBoxFor(model => model.PostalCode, new { @class = "Text75", @onblur = "postalCodeLookup(this.value);" })%> <img id="ImageWaitPostalCodeLookup" alt="Checking postal code..." title="Checking postal code..." src="<%=Url.Content("~/Content/Images/Loading.gif")%>" style="display: none;" /> <span id="Zip"></span>
                                <%= Html.ValidationMessageFor(model => model.PostalCode)%>
                            </td>
                        </tr>
                        <tr>
                            <td>City</td>
                            <td>
                                <%= Html.TextBoxFor(model => model.City)%>
                                <%= Html.ValidationMessageFor(model => model.City)%>
                            </td>
                        </tr>
                        <tr>
                            <td>State</td>
                            <td>
                                <%= Html.TextBoxFor(model => model.State) %>
                                <%= Html.ValidationMessageFor(model => model.State)%>
                            </td>
                        </tr>
                        <tr>
                            <td>County</td>
                            <td>
                                <%= Html.TextBoxFor(model => model.County)%>
                                <%= Html.ValidationMessageFor(model => model.County)%>
                            </td>
                        </tr>

                        <tr class="ui-widget-header">
                            <td colspan="2">Mailing Address</td>
                        </tr>

                        <tr>
                            <td>Address</td>
                            <td>
                                <%= Html.TextBoxFor(model => model.MailingAddress)%>
                                <%= Html.ValidationMessageFor(model => model.MailingAddress)%>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <%= Html.TextBoxFor(model => model.MailingAddress2)%>
                                <%= Html.ValidationMessageFor(model => model.MailingAddress2)%>
                            </td>
                        </tr>
                        <tr>
                            <td>City</td>
                            <td>
                                <%= Html.TextBoxFor(model => model.MailingCity)%>
                                <%= Html.ValidationMessageFor(model => model.MailingCity)%>
                            </td>
                        </tr>
                        <tr>
                            <td>State</td>
                            <td>
                                <%= Html.TextBoxFor(model => model.MailingState)%>
                                <%= Html.ValidationMessageFor(model => model.MailingState)%>
                            </td>
                        </tr>
                        <tr>
                            <td>Postal Code</td>
                            <td>
                                <%= Html.TextBoxFor(model => model.MailingPostalCode)%>
                                <%= Html.ValidationMessageFor(model => model.MailingPostalCode)%>
                            </td>
                        </tr>
                        <tr>
                            <td>Do Not Mail?</td>
                            <td>
                                <%= Html.CheckBoxFor(model => model.IsDecliningMail)%>
                                <%= Html.ValidationMessageFor(model => model.IsDecliningMail)%>
                            </td>
                        </tr>

                        <tr class="ui-widget-header">
                            <td colspan="2">History</td>
                        </tr>

                        <tr>
                            <td>Created</td>
                            <td>
                                <% if (Model.Id > 0 && Masonite.MTier.Utility.DateHelper.IsValidSqlDate(Model.DateCreated)) { %>
                                    <%= Model.CreatedBy %> on <%= Model.DateCreated %><br />
                                <% } %>
                            </td>
                        </tr>
                        <tr>
                            <td>Last Modified</td>
                            <td>
                                <% if (Model.Id > 0 && Masonite.MTier.Utility.DateHelper.IsValidSqlDate(Model.DateModified)) { %>
                                    <%= Model.ModifiedBy %> on <%= Model.DateModified %>
                                <% } %>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
            <td valign="top" style="float:left;width:365px;">
                <table style="width:99%;border-style:none;">
                    <% if (Page.User.IsInRole("SalesPoint Administrators") || Page.User.IsInRole("Max Team")) { %>
                    <tr class="ui-widget-header">
                        <td colspan="2">Is Linked To</td>
                    </tr>

                    <tr>
                        <td colspan="2" id="LinkedTo"><% Html.RenderAction("IndexMerchantsPartial", "MerchantMerchantContact", new { merchantContactId = Model.Id, currentMerchantId = ViewData["MerchantId"] }); %></td>
                    </tr>
                    <% } %>

                    <tr class="ui-widget-header">
                        <td colspan="2">MConnect</td>
                    </tr>

                    <% if(Model.IsMasoniteDotComLogonLocked) { %>
                    <tr>
                        <td colspan="2"><span class="TextRed">(Locked)</span><br />This account has been locked because the contact has not logged in within the past 90 days.</td>
                    </tr>
                    <% } %>

                    <tr>
                        <td>Logon Name</td>
                        <td>
                            <% // Disabled when creating a new contact, and when the current user isn't an admin or on the Max team %>
                            <% if (Model.Id > 0 && (Page.User.IsInRole("SalesPoint Administrators") || Page.User.IsInRole("Max Team"))) { %>
                            <%= Html.TextBoxFor(model => model.MasoniteDotComLogon)%>
                            <% } else { %>
                            <%= Html.TextBoxFor(model => model.MasoniteDotComLogon, new { @disabled = "disabled" })%>
                            <% } %>
                            <%= Html.ValidationMessageFor(model => model.MasoniteDotComLogon)%>
                            <img id="ImageWaitGeneratePassword" alt="Generating password..." title="Generating password..." src="<%=Url.Content("~/Content/Images/Loading.gif")%>" style="display:none;margin-bottom:0;margin-top:0" />
                        </td>
                    </tr>
                    <tr>
                        <td>Password</td>
                        <td style="white-space:nowrap;">
                            <input type="password" id="MasoniteDotComPassword" name="MasoniteDotComPassword" placeholder="●●●●●●●●" />
                            <a href="javascript:void(0);" onclick="generateRandomPassword();" title="Generate a random password for me">Random</a>
                        </td>
                    </tr>
                    <tr>
                        <td style="white-space:nowrap;">Require Password Change?</td>
                        <td>
                            <%= Html.CheckBoxFor(model => model.IsPasswordChangeRequired)%>
                            <%= Html.ValidationMessageFor(model => model.IsPasswordChangeRequired)%>
                        </td>
                    </tr>
                    <tr>
                        <td>Last Logon</td>
                        <td><%= Masonite.MTier.Utility.DateHelper.IsValidSqlDate(Model.DateLastLogon) ? Model.DateLastLogon.ToString("G", System.Globalization.CultureInfo.CurrentUICulture) : "[none]" %></td>
                    </tr>
                    <tr>
                        <td>Logon Count</td>
                        <td><%= Model.LogonCount %></td>
                    </tr>

                    <tr class="ui-widget-header">
                        <td colspan="2">MConnect Options</td>
                    </tr>

                    <tr>
                        <td>Access Max&#8480;?</td>
                        <td>
                            <% if(Page.User.IsInRole("SalesPoint Administrators") || Page.User.IsInRole("Max Team")) { %>
                                <%= Html.CheckBoxFor(model => model.IsAllowedAccessToConfigurator, new { @onclick = "validateCanAccessConfigurator(this.checked);" })%>
                            <% } else { %>
                                <%= Html.CheckBoxFor(model => model.IsAllowedAccessToConfigurator, new { @onclick = "return false;" })%>
                            <% } %>
                        </td>
                    </tr>
                    <tr>
                        <td>Max&#8480; Training Date</td>
                        <td>
                            <%= Masonite.MTier.Utility.DateHelper.IsValidSqlDate(Model.DateMaxTraining) ? Model.DateMaxTraining.ToString("d", System.Globalization.CultureInfo.CurrentUICulture) : "[none]"%>
                        </td>
                    </tr>
                    <tr <% if(!Model.IsAllowedAccessToConfigurator) { %>style="display:none;"<% } %> id="RowMaxAdmin">
                        <td>Max&#8480; Admin?</td>
                        <td><%= Html.CheckBoxFor(model => model.IsMaxAdministrator)%></td>
                    </tr>
                    <tr>
                        <td>Access MConnect?</td>
                        <td><%= Html.CheckBoxFor(model => model.IsAllowedAccessToDealerSite)%></td>
                    </tr>
                    <tr>
                        <td>Access Order Tracking?</td>
                        <td><%= Html.CheckBoxFor(model => model.IsAllowedAccessToOrderTracking)%></td>
                    </tr>
                    <tr>
                        <td>Access VMI Order Tracking?</td>
                        <td><%= Html.CheckBoxFor(model => model.IsAllowedAccessToVMIOrderTracking)%></td>
                    </tr>
                    <tr>
                        <td>Access Treasure Chest?</td>
                        <td><%= Html.CheckBoxFor(model => model.IsAllowedAccessToTreasureChest)%></td>
                    </tr>
                    <tr>
                        <td>Access Masonite Store?</td>
                        <td><%= Html.CheckBoxFor(model => model.IsAllowedAccessToMasoniteStore)%></td>
                    </tr>
                    <tr>
                        <td>Access Glass Xpress?</td>
                        <td><%= Html.CheckBoxFor(model => model.IsAllowedAccessToGlassXpress)%></td>
                    </tr>

                    <tr class="ui-widget-header">
                        <td colspan="2">Security Options</td>
                    </tr>

                    <tr>
                        <td>Masonite Store Group</td>
                        <td><%= Html.DropDownListFor(model => model.MasoniteStoreGroupId, new SelectList(Masonite.MTier.SalesPoint.ObjectFactory.RetrieveMasoniteStoreGroups(), "Id", "Name", Model.MasoniteStoreGroupId), " ")%></td>
                    </tr>

                    <tr>
                        <td>Image Library Group</td>
                        <td>
                            <select name="ImageLibraryGroupId" id="ImageLibraryGroupId">
                                <option value="Wholesale"<% if(string.Compare("Wholesale", Model.ImageLibraryGroupId, true, System.Globalization.CultureInfo.CurrentUICulture) == 0) { %>selected = "selected"<% } %>>Wholesale (All)</option>
                                <option value="WholesaleUS"<% if(string.Compare("WholesaleUS", Model.ImageLibraryGroupId, true, System.Globalization.CultureInfo.CurrentUICulture) == 0) { %>selected = "selected"<% } %>>Wholesale (US)</option>
                                <option value="WholesaleCAN"<% if(string.Compare("WholesaleCAN", Model.ImageLibraryGroupId, true, System.Globalization.CultureInfo.CurrentUICulture) == 0) { %>selected = "selected"<% } %>>Wholesale (CAN)</option>
                                <option value="Retail"<% if(string.Compare("Retail", Model.ImageLibraryGroupId, true, System.Globalization.CultureInfo.CurrentUICulture) == 0) { %>selected = "selected"<% } %>>Retail</option>
                                <option value="Corporate"<% if(string.Compare("Corporate", Model.ImageLibraryGroupId, true, System.Globalization.CultureInfo.CurrentUICulture) == 0) { %>selected = "selected"<% } %>>Corporate</option>
                                <option value="Retail - Home Depot"<% if(string.Compare("Retail - Home Depot", Model.ImageLibraryGroupId, true, System.Globalization.CultureInfo.CurrentUICulture) == 0) { %>selected = "selected"<% } %>>Retail - Home Depot</option>
                                <option value="Retail - Lowes"<% if(string.Compare("Retail - Lowes", Model.ImageLibraryGroupId, true, System.Globalization.CultureInfo.CurrentUICulture) == 0) { %>selected = "selected"<% } %>>Retail - Lowes</option>
                            </select>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
                
    <p class="required">* Required field</p>
    <p>
        <input id="SubmitButton" type="submit" value="Save" class="ui-button ui-widget ui-state-default ui-corner-all" onclick="$(this).hide();$('#LoadingMerchantContact').show();return Validate();" /><img id="LoadingMerchantContact" alt="Loading" title="Loading" style="display: none;" src="<%=Url.Content("~/Content/Images/Loading.gif")%>" />
    </p>
<% } %>