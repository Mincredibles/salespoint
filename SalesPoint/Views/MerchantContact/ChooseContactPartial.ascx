﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<Masonite.MTier.SalesPoint.MerchantContact>>" %>

<table id="result" class="no-border">
<% if(ViewData.Model.Count<Masonite.MTier.SalesPoint.MerchantContact>() == 0) { %>
        <tr><td class="no-border">No items match the current search criteria</td></tr>
<% } else { %>
    <% foreach (Masonite.MTier.SalesPoint.MerchantContact item in Model) { %>
        <tr>
            <td class="no-border"><a href="javascript:void(0);" onclick="addMerchantMerchantContact(<%= item.Id %>,this);">Add</a></td>
            <td class="no-border"><%: item.FirstName %> <%: item.LastName %> [<%: item.EmailAddress %>]</td>
        </tr>
    <% } %>
<% } %>
</table>