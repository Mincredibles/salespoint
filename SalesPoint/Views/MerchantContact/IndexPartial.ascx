﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<Masonite.MTier.SalesPoint.MerchantContact>>" %>

<% string imagePathCheckmark = Url.Content("~/Content/Images/Check.png"); %>

<style type="text/css">
    .rn td
    {
        border-style:solid;
        border-width:1px;
        border-color:#BDCEDF;
        vertical-align:middle;
        text-align:center;
    }
</style>

<script type="text/javascript">
    var controllerPath = '<%= VirtualPathUtility.ToAbsolute("~/" + ViewContext.RouteData.Values["Controller"]) %>';
    var isPageReloadNeeded = false;
    var lastElement;
    var lastMerchantContactId;
    setMerchantName('<%= ViewData["MerchantName"] %>');

    $(function () {
        $("#dialog-modal-EditMerchantContact").dialog({
            height: 450,
            width: 900,
            modal: true,
            autoOpen: false,
            resizable: false,
            close: function() {
                dialogClose("#dialog-content-MerchantContact");
            }
        });

        $("#dialog-modal-EditMerchantContactConfigureOne").dialog({
            height: 450,
            width: 900,
            modal: true,
            autoOpen: false,
            resizable: false,
            close: function() {
                dialogClose("#dialog-content-MerchantContactConfigureOne");
            }
        });

        $("#dialog-modal-EditMerchantMerchantContact").dialog({
            height: 350,
            width: 650,
            modal: true,
            autoOpen: false,
            resizable: false,
            close: function() {
                dialogClose("#dialog-content-MerchantMerchantContact");
            }
        });
    });
    function dialogClose(dialogId) {
        if (isPageReloadNeeded == true) {
            $('#SearchButton').click();
        }
        else {
            $(dialogId).empty();
        }
    }
    function editMerchantContactDialog(id) {
        $('#dialog-content-MerchantContact').load('<%= VirtualPathUtility.ToAbsolute("~/MerchantContact/EditPartial")%>/' + id + '?merchantId=<%= ViewData["MerchantId"]%>');
        $('#dialog-modal-EditMerchantContact').dialog('open');
    }
    function editMerchantContactConfigureOneDialog(id) {
        $('#dialog-content-MerchantContactConfigureOne').load('<%= VirtualPathUtility.ToAbsolute("~/MerchantContactConfigureOne/IndexPartial") %>?merchantContactId=' + id);
        $('#dialog-modal-EditMerchantContactConfigureOne').dialog('open');
    }
    function editMerchantMerchantContactDialog(id) {
        $('#dialog-content-MerchantMerchantContact').load('<%= VirtualPathUtility.ToAbsolute("~/MerchantContact/ChooseContactPartial") %>');
        $('#dialog-modal-EditMerchantMerchantContact').dialog('open');
    }
    function deleteCompleted() {
        if (lastElement) {
            $(lastElement).parent().parent().hide();
        }
    }
    function saveMerchantContactConfigureOneCompleted(response) {
        $('#dialog-modal-EditMerchantContactConfigureOne').dialog('close');
    }
    function validateCanAccessConfigurator(isChecked) {
        var merchantHasConfigureOneParent = <%= ViewData["MerchantHasConfigureOneParent"] %>;
            
        if (!isChecked) {
            $('#RowMaxAdmin').hide();
            $('#IsMaxAdministrator').prop('checked', false);
            return false;
        }
        if (merchantHasConfigureOneParent == false) {
            window.alert('In order for a contact to have access to Max, the Location that it belongs to must be associated with at least one distributor.\n\nTo associate distributors, switch to the Distributors tab.');
            $('#IsAllowedAccessToConfigurator').prop('checked', false);
            $('#RowMaxAdmin').hide();
            $('#IsMaxAdministrator').prop('checked', false);
            return false;
        }
        else {
            $('#RowMaxAdmin').show();
            return true;
        }
    }
    function postalCodeLookup(postalCode) {
        if (postalCode.length == 0) { return false; }

        $("#ImageWaitPostalCodeLookup").show();

        $.ajax({
            type: "POST",
            url: '<%= VirtualPathUtility.ToAbsolute("~/Utility/PostalCodeLookup") %>?postalCode=' + postalCode
        }).done(function (data) {
            var useZip = "defaultCityStateZip('','','" + data.City + "', '" + data.StateName + "', '" + data.PostalCode + "', '" + data.County + "');";
            $('#Zip').html("<a href='javascript:void(0);' onclick=\"" + useZip + "\">Use " + data.City + ", " + data.StateName + " " + data.PostalCode + "</a>");

            $("#ImageWaitPostalCodeLookup").hide();
        });
    }
    function locationAddressLookup() {
        $("#ImageWaitLocationLookup").show();

        $.ajax({
            type: "POST",
            url: '<%= VirtualPathUtility.ToAbsolute("~/Utility/LocationAddressLookup") %>?merchantId=<%= ViewData["id"] %>'
        }).done(function (data) {
            defaultCityStateZip(data.Address, data.Address2, data.City, data.StateName, data.PostalCode, data.County);

            $("#ImageWaitLocationLookup").hide();
        });
    }
    function defaultCityStateZip(address, address2, city, state, postalCode, county) {
        if (address.length > 0) { $('#Address').val(address); }
        if (address2.length > 0) { $('#Address2').val(address2); }
        $('#City').val(city);
        $('#State').val(state);
        $('#PostalCode').val(postalCode);
        if (county.length > 0) { $('#County').val(county); }
        $('#MailingAddress').select();
    }
    function generateRandomPassword() {
        $("#ImageWaitGeneratePassword").show();

        $.ajax({
            type: "POST",
            url: '<%= VirtualPathUtility.ToAbsolute("~/Utility/GenerateRandomPassword") %>'
        }).done(function (data) {
            document.getElementById('MasoniteDotComPassword').value = data;

            $("#ImageWaitGeneratePassword").hide();
        });
    }
    function resetPassword(merchantContactId) {
        lastMerchantContactId = merchantContactId;

        $.ajax({
            type: "POST",
            url: '<%= VirtualPathUtility.ToAbsolute("~/MerchantContact/ResetPassword") %>?merchantContactId=' + merchantContactId
        }).done(function () {
            var id = '#Img' + lastMerchantContactId;
            var action = "$('" + id + "').hide();"
            $(id).show();
            setTimeout(action, 5000);
        });
    }
    function removeMerchantMerchantContact(merchantId, merchantContactId, tableRow) {
        lastElement = tableRow;

        $.ajax({
            type: "DELETE",
            url: '<%= VirtualPathUtility.ToAbsolute("~/MerchantMerchantContact/Delete") %>?merchantId=' + merchantId + '&merchantContactId=' + merchantContactId
        }).done(function () {
            $(lastElement).parent().hide();
        });
    }
    function setPrimaryMerchantMerchantContact(merchantId, merchantContactId, div) {
        $.ajax({
            type: "POST",
            url: '<%= VirtualPathUtility.ToAbsolute("~/MerchantMerchantContact/SetPrimary") %>?merchantId=' + merchantId + '&merchantContactId=' + merchantContactId
        }).done(function () {
            $("#LinkedTo").load("<%= VirtualPathUtility.ToAbsolute("~/MerchantMerchantContact/IndexMerchantsPartial") %>?currentMerchantId=" + merchantId + "&merchantContactId=" + merchantContactId);
        });
    }
    function addMerchantMerchantContact(merchantContactId, span) {
        lastElement = span;

        $.ajax({
            type: "POST",
            url: '<%= VirtualPathUtility.ToAbsolute("~/MerchantMerchantContact/Edit") %>?merchantId=<%= ViewData["id"] %>&merchantContactId=' + merchantContactId
        }).done(function () {
            $(lastElement).hide();
            $('#ImageWaitAddMerchantMerchantContact').hide();
            isPageReloadNeeded = true;
        });
    }
    function saveCompleted(data) {
        if (data.success == false) {
            $('#SubmitButton').show();
            $('#LoadingMerchantContact').hide();
            window.alert(data.message);
                
            return;
        }
    
        isPageReloadNeeded = true;
        $('#dialog-modal-EditMerchantContact').dialog('close');
    }
    function Validate() {
        // Manual validation in place because the MVC validation doesn't work on AJAX-loaded forms
        var isDataValid = true;
        var control;

        control = document.getElementById('LastName');            
        if (control.value.length == 0) {
            control.className = 'input-validation-error';
            $('#LastName_validationMessage').show();
            isDataValid = false;
        }
        else {
            control.className = '';
            $('#LastName_validationMessage').hide();
        }

        control = document.getElementById('RoleId');            
        if (control.value.length == 0) {
            control.className = 'input-validation-error';
            $('#RoleId_validationMessage').show();
            isDataValid = false;
        }
        else {
            control.className = '';
            $('#RoleId_validationMessage').hide();
        }

        control = document.getElementById('EmailAddress');            
        if (control.value.length == 0) {
            control.className = 'input-validation-error';
            $('#EmailAddress_validationMessage').show();
            isDataValid = false;
        }
        else {
            control.className = '';
            $('#EmailAddress_validationMessage').hide();
        }

        if (isDataValid == false) {
            $('#SubmitButton').hide();
            $('#LoadingMerchantContact').hide()
        }
        else {
            $('#SubmitButton').show();
        }

        return isDataValid;
    }
</script>
<div id="ContentElementContacts">
    <div style="margin-bottom: 5px;">
        <div class="float-left">
            <input type="button" value="Add New Contact" onclick="editMerchantContactDialog(0);" class="ui-button ui-widget ui-state-default ui-corner-all" />

            <% if (Page.User.IsInRole("SalesPoint Administrators") || Page.User.IsInRole("Max Team")) { %>
            <input type="button" value="Link Existing Contact" onclick="editMerchantMerchantContactDialog();" class="ui-button ui-widget ui-state-default ui-corner-all" />
            <% } %>
        </div>

        <div class="float-right">
            <% using (Ajax.BeginForm("IndexPartial", "MerchantContact", new { merchantId = ViewData["id"], id = "" }, new AjaxOptions { HttpMethod = "POST", UpdateTargetId = "ActiveTabContent", OnComplete = "" })) { %>
                <input type="text" placeholder="Type here to search" name="searchName" id="SearchTextBox" style="height:22px" value="<%: ViewData["searchName"] %>" />
                <input type="submit" value="Search" id="SearchButton" class="ui-button ui-widget ui-state-default ui-corner-all" />
                <input type="button" value="View All" onclick="$('#SearchTextBox').val('');$('#SearchButton').click();" class="ui-button ui-widget ui-state-default ui-corner-all" />
            <% } %>
        </div>

        <div class="break"></div>

        <div class="recordcount">Viewing the top <%= ViewData.Model.Count<Masonite.MTier.SalesPoint.MerchantContact>().ToString()%> result(s).</div>
    </div>

    <table id="ContactAttributes" class="full-width">
        <thead class="ui-widget-header">
            <tr>
                <th></th>
                <th style="width:50px;"></th>
                <th>MConnect?</th>
                <th>Order Tracking?</th>
                <th>Masonite Store?</th>
                <th>Treasure Chest?</th>
                <th>Sales Rebate Contact?</th>
                <th>Password</th>
                <th>Max Options</th>
                <th></th>
            </tr>
        </thead>
                
        <% foreach (Masonite.MTier.SalesPoint.MerchantContact item in Model) { %>
                
        <tr onclick="selectTableRow(this);" class="rn" id="Row<%=item.Id %>">
            <td style="text-align:left;border-right-style:none;">
                <a onclick="editMerchantContactDialog(<%=item.Id %>);" href='javascript:void(0);'><%= item.LastName + (string.IsNullOrEmpty(item.FirstName) ? "" : ", " + item.FirstName) %></a>
                <%= string.IsNullOrEmpty(item.RoleName) ? "" : "<br />" + Server.HtmlEncode(item.RoleName) %>
                <%= string.IsNullOrEmpty(item.PhoneNumber) ? "" : "<br />" + Server.HtmlEncode(Masonite.MTier.Utility.PhoneNumberHelper.Format(item.PhoneNumber, ".")) + " (Phone)" %>
                <%= string.IsNullOrEmpty(item.MobileNumber) ? "" : "<br />" + Server.HtmlEncode(Masonite.MTier.Utility.PhoneNumberHelper.Format(item.MobileNumber, ".")) + " (Cell)" %>
                <% if (!string.IsNullOrEmpty(item.EmailAddress)){%><br />Email <a href="mailto:<%: item.EmailAddress %>"><%: item.EmailAddress %></a><% } %>
            </td>
            <td style="border-left-style:none;border-right-style:none;"><% if(item.IsAllowedAccessToConfigurator) { %><img src="<%=Url.Content("~/Content/Images/MaxSmall.png")%>" alt="This contact has access to Max" title="This contact has access to Max" style="margin: -1px;" /><% } %></td>
            <td>
                <% if(item.IsAllowedAccessToDealerSite) { %><img src="<%= imagePathCheckmark %>" alt="" title="" class="gridicon" /><% } %>
                <% if(item.IsMasoniteDotComLogonLocked) { %><br /><span class="TextRed" title="This account has been locked because the contact has not logged in within the past 90 days.">(Locked)</span><% } %>
            </td>
            <td><% if(item.IsAllowedAccessToOrderTracking) { %><img src="<%= imagePathCheckmark %>" alt="" title="" class="gridicon" /><% } %></td>
            <td><% if(item.IsAllowedAccessToMasoniteStore) { %><img src="<%= imagePathCheckmark %>" alt="" title="" class="gridicon" /><% } %></td>
            <td><% if(item.IsAllowedAccessToTreasureChest) { %><img src="<%= imagePathCheckmark %>" alt="" title="" class="gridicon" /><% } %></td>
            <td><% if(item.IsSalesRebateContact) { %><img src="<%= imagePathCheckmark %>" alt="" title="" class="gridicon" /><% } %></td>
            <td><a href="javascript:void(0);" onclick="resetPassword(<%=item.Id%>);" title="Reset this person's MConnect password">Reset</a> <img id="Img<%=item.Id%>" style="display:none;" class="gridicon" alt="Sent" title="Sent" src="<%=Url.Content("~/Content/Images/Check.png")%>" /></td>
            <td>
                <% if (item.IsAllowedAccessToConfigurator && (Page.User.IsInRole("SalesPoint Administrators") || Page.User.IsInRole("Max Team"))) { %>
                    <a onclick="editMerchantContactConfigureOneDialog(<%=item.Id%>);" href='javascript:void(0);'>Set</a>
                <%} %>
            </td>
            <td class="cellalignmiddle">
                <% if(!item.IsAllowedAccessToConfigurator) { %>
                    <img alt="Delete" title="Delete" src="<%=Url.Content("~/Content/Images/Delete.png")%>" onclick="lastElement = this;deleteItem('<%= item.Id %>', controllerPath)" class="gridicon" />
                <% } %>
            </td>
        </tr>
        <% } %>

    </table>
</div>

<div id="dialog-modal-EditMerchantContact" title="Edit Contact">
    <div id="dialog-content-MerchantContact"></div>
</div>

<div id="dialog-modal-EditMerchantContactConfigureOne" title="Contact: Edit Max Options">
    <div id="dialog-content-MerchantContactConfigureOne"></div>
</div>

<div id="dialog-modal-EditMerchantMerchantContact" title="Link Contact From Another Location">
    <% using (Ajax.BeginForm("ChooseContactPartial", "MerchantContact", new AjaxOptions { UpdateTargetId = "dialog-content-MerchantMerchantContact", HttpMethod = "Post", LoadingElementId = "ImageWaitAddMerchantMerchantContact" }))
       { %>
        <div>    
            <input id="searchName" name="searchName" type="text" value="<%= Html.Encode(ViewData["searchName"])%>" />
            <input type="submit" value="Search" class="ui-button ui-widget ui-state-default ui-corner-all" />
        </div>
        <div class="min-height-15"><img src="<%= Url.Content("~/Content/Images/AjaxWait.gif") %>" alt="Working..." title="Working..." id="ImageWaitAddMerchantMerchantContact" /></div>
        <div id="dialog-content"></div>
    <% } %>
    <div id="dialog-content-MerchantMerchantContact"></div>
</div>

<script type="text/javascript">
    $("#ImageWaitAddMerchantMerchantContact").hide();
</script>