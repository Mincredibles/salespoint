﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<Masonite.MTier.SalesPoint.PostalCodeLocation>>" %>

<% foreach (var item in Model) { %>
        <span>Use <%: string.Concat(item.City, ", ", item.StateCode, " ", item.PostalCode) %></span>
<% } %>