﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<% if (Request.IsAuthenticated) { %>
        Logged on as <strong><%: Page.User.Identity.Name %></strong>
<% } %>
