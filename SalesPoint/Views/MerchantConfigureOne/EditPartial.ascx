﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Masonite.MTier.SalesPoint.MerchantConfigureOne>" %>
    <script type="text/javascript">
        setMerchantName('<%= ViewData["MerchantName"] %>');
    </script>
    
     <% using (Ajax.BeginForm("Edit", "MerchantConfigureOne", new { merchantId = ViewData["MerchantId"] }, new AjaxOptions { HttpMethod = "POST", UpdateTargetId = "ActiveTabContent", OnComplete = "" }))
        { %>
        <%= Html.ValidationSummary(true) %>
        
        <div class="ui-widget-header padding-five">Details</div>
        <div class="editor-label-left extra-top-buffer">Active in Max?</div>
        <div class="editor-field extra-top-buffer">
            <% if((bool)ViewData["IsActiveInMax"] == true) { %>Yes<% } else { %>No [No Distributor(s) are defined, or no contacts have access to Max]<% }%>
        </div>
                                        
        <div class="editor-label-left">External Dealer #</div>
        <div class="editor-field">
            <%= Html.TextBoxFor(model => model.ExternalDealerNumber)%>
        </div>
        <div><%= Html.ValidationMessageFor(model => model.ExternalDealerNumber)%></div>
                
        <div class="editor-label-left">Quote Duration</div>
        <div class="editor-field">
            <%= Html.TextBoxFor(model => model.QuoteDuration)%>
        </div>
        <div><%= Html.ValidationMessageFor(model => model.QuoteDuration)%></div>
        
        <div class="editor-label-left">Standard Disclaimer</div>
        <div class="editor-field">
            <%= Html.TextAreaFor(model => model.StandardDisclaimer, new { @cols = "125", @rows = "5" })%>
        </div>
        <div><%= Html.ValidationMessageFor(model => model.StandardDisclaimer)%></div>

        <div class="break"></div>

        <div class="editor-label-left">Pricing Method</div>
        <div class="editor-field">
            <%= Html.DropDownListFor(model => model.PricingMethod, new SelectList(
                    new List<Object>{
                        new { value = "Catalog Adjustment" , text = "Catalog Adjustment" },
                        new { value = "Cost Multiplier" , text = "Cost Multiplier" },
                        new { value = "Gross Margin" , text = "Gross Margin"}
                    },
                    "value",
                    "text")) %>
        </div>
        <div><%= Html.ValidationMessageFor(model => model.PricingMethod)%></div>

        <div class="editor-label-left">Pricing Default</div>
        <div class="editor-field">
            <%= Html.TextBoxFor(model => model.PricingDefault, new { @class = "Text75", @maxlength = "8" })%> 
            <%= Html.DropDownListFor(model => model.PricingDefaultCategory, new SelectList(
                    new List<Object>{
                        new { value = "P" , text = "%" },
                        new { value = "A" , text = "$"}
                    },
                    "value",
                    "text")) %>
        </div>
        <div><%= Html.ValidationMessageFor(model => model.PricingDefault)%></div>
        <div><%= Html.ValidationMessageFor(model => model.PricingDefaultCategory)%></div>
                
        <div class="ui-widget-header padding-five">Tax Rates</div>
        <div class="editor-label-left extra-top-buffer">Tax Rate 1</div>
        <div class="editor-field">
            <%= Html.TextBoxFor(model => model.TaxRate1)%> <%= Html.TextBoxFor(model => model.TaxRate1Extension, new { @class = "Text75" })%>%
        </div>
        <div><%= Html.ValidationMessageFor(model => model.TaxRate1)%></div>
        
        <div class="editor-label-left">Tax Rate 2</div>
        <div class="editor-field">
            <%= Html.TextBoxFor(model => model.TaxRate2)%> <%= Html.TextBoxFor(model => model.TaxRate2Extension, new { @class = "Text75" })%>%
        </div>
        <div><%= Html.ValidationMessageFor(model => model.TaxRate2)%></div>
        
        <div class="editor-label-left">Tax Rate 3</div>
        <div class="editor-field">
            <%= Html.TextBoxFor(model => model.TaxRate3)%> <%= Html.TextBoxFor(model => model.TaxRate3Extension, new { @class = "Text75" })%>%
        </div>
        <div><%= Html.ValidationMessageFor(model => model.TaxRate3)%></div>
                
        <div class="ui-widget-header padding-five">Permissions</div>
        <table class="merchant-options" cellpadding="1" border="0">
            <tr>
                <td>Edit other members' data?</td>
                <td><%= Html.CheckBoxFor(model => model.IsPermissionCanEditOtherMembersData)%></td>
            </tr>
            <tr>
                <td>Read other members' data?</td>
                <td><%= Html.CheckBoxFor(model => model.IsPermissionCanReadOtherMembersData)%></td>
            </tr>
            <tr>
                <td>Edit descendants' data?</td>
                <td><%= Html.CheckBoxFor(model => model.IsPermissionCanEditDescendantData)%></td>
            </tr>
            <tr>
                <td>Read descendants' data?</td>
                <td><%= Html.CheckBoxFor(model => model.IsPermissionCanReadDescendantData)%></td>
            </tr>
        </table>
         
        <p class="required">* Required field</p>
        <p>
            <input type="submit" value="Save" class="ui-button ui-widget ui-state-default ui-corner-all" />
        </p>
    <% } %>

    <script type="text/javascript">
        $("#PricingMethod").change(function () {
            if ($('#PricingMethod').val() == 'Cost Multiplier' || $('#PricingMethod').val() == 'Gross Margin') {
                $("#PricingDefaultCategory > option[value='P']").prop('selected', true);
                $('#PricingDefaultCategory').hide();
            }
            else {
                $('#PricingDefaultCategory').show();
            }
        });
        $("#PricingMethod").change();
    </script>