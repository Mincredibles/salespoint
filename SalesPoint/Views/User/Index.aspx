﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<Masonite.MTier.SalesPoint.User>>" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">
        var controllerPath = '<%= VirtualPathUtility.ToAbsolute("~/" + ViewContext.RouteData.Values["Controller"]) %>';
        function deleteCompleted() {
            var searchName = $('#searchName').val();
            if (searchName.length > 0) {
                window.location = '<%= VirtualPathUtility.ToAbsolute("~/User") %>?searchName=' + searchName;
            }
            else {
                window.location.reload();
            }
        }
    </script>

    <div id="TwoColumnWrapper">
        <% if(User.IsInRole("SalesPoint Administrators")) { %>
        <div id="column-left" class="float-left"><% Html.RenderAction("AdministrationLeft", "Navigation", new { selectedMenuItem = SalesPoint.Core.SelectedNavigationAdministrationLeft.Users }); %></div>
        <% } %>

        <div id="column-right">
            <h2>Users</h2><%= Html.ActionLink("Clear Search Results", "Index")%> | <%= Html.ActionLink("Create New", "Edit")%>
            <div class="break"></div>

            <div>
                <% using(Html.BeginForm()) { %> 
                    <input type="text" class="editor-field" name="searchName" id="searchName" value="<%: ViewData["searchName"] %>" /> 
                    <input type="submit" value="Search" class="ui-button ui-widget ui-state-default ui-corner-all" />
                    <div class="recordcount">Viewing the top <%= ViewData.Model.Count<Masonite.MTier.SalesPoint.User>().ToString()%> result(s).</div>
                <% } %>
            </div>

            <table style="width:100%;">
                <thead class="ui-widget-header">
                    <tr>
                        <th>Logon Name</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Job Title</th>
                        <th>Phone Number</th>
                        <th>Cell Phone Number</th>
                        <th>Email Address</th>
                        <th></th>
                    </tr>
                </thead>

            <% foreach (var item in Model) { %>
                <tr onclick="selectTableRow(this);">
                    <td><%= Html.ActionLink(item.ActiveDirectoryLogonName, "Edit", new { id = item.Id })%></td>
                    <td><%: item.FirstName %></td>
                    <td><%: item.LastName %></td>
                    <td><%: item.JobTitle %></td>
                    <td><%: Masonite.MTier.Utility.PhoneNumberHelper.Format(item.PhoneNumber, ".") %></td>
                    <td><%: Masonite.MTier.Utility.PhoneNumberHelper.Format(item.CellPhoneNumber, ".") %></td>
                    <td><% if (!string.IsNullOrEmpty(item.EmailAddress)){%><a href="mailto:<%: item.EmailAddress %>"><%: item.EmailAddress %></a><% } %></td>
                    <td class="cellalignmiddle"><img alt="Delete" title="Delete" src="<%=Url.Content("~/Content/Images/Delete.png")%>" onclick="deleteItem('<%= item.Id %>', controllerPath)" class="gridicon" /></td>
                </tr>
            <% } %>

            </table>
        </div>
    </div>
</asp:Content>