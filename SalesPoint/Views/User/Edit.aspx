﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Masonite.MTier.SalesPoint.User>" %>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">
        var lastUserGroupSpan;
        function removeItem(userGroupId, span) {
            lastUserGroupSpan = span;

            $.ajax({
                type: "DELETE",
                url: '<%= VirtualPathUtility.ToAbsolute("~/UserGroupUser/Delete/") %>' + userGroupId + '?userId=<%= Model.Id %>'
            }).done(function () {
                if (lastUserGroupSpan) {
                    lastUserGroupSpan.parentNode.removeChild(lastUserGroupSpan);
                }
            });
        }
        function checkLogonNameForDuplicates(logonName) {
            if (logonName.length == 0 || logonName.toLowerCase() == '<%= Model.ActiveDirectoryLogonName %>'.toLowerCase()) { return false; } // Exit, if this is an existing user and logonName hasn't changed, or if logonName is empty.
            
            $('#ImageWaitDupeCheck').show();
            $('#Dupe').html('');
            $('#SaveButton').hide();
            
            $.ajax({
                type: "POST",
                url: '<%= VirtualPathUtility.ToAbsolute("~/User/LogonNameDuplicates") %>?logonName=' + escape(logonName)
            }).done(function (data) {
                $('#ImageWaitDupeCheck').hide();
                if (data.Id > 0) {
                    $('#Dupe').html('This logon name is already in use, <a href=\'<%= VirtualPathUtility.ToAbsolute("~/User/Edit/") %>' + data.Id + '\'>here</a>');
                }
                $('#SaveButton').show();
            });
        }
        function postalCodeLookup(postalCode) {
            if (postalCode.length == 0) { return false; } // Exit, if postalCode is empty.

            $('#ImageWaitPostalCodeLookup').show();

            $.ajax({
                type: "POST",
                url: '<%= VirtualPathUtility.ToAbsolute("~/Utility/PostalCodeLookup") %>?postalCode=' + postalCode
            }).done(function (data) {
                if (data.IsValid == true) {
                    var useZip = "defaultCityStateZip('" + data.City + "', '" + data.StateName + "', '" + data.PostalCode + "', '" + data.Country + "');";
                    $('#Zip').html("<a href='javascript:void(0);' onclick=\"" + useZip + "\">Use " + data.City + ", " + data.StateName + " " + data.PostalCode + " (" + data.Country + ")</a>");
                }
                $('#ImageWaitPostalCodeLookup').hide();
            });
        }
        function defaultCityStateZip(city, stateName, postalCode, country) {
            $('#City').val(city);
            $('#State').val(stateName);
            $('#PostalCode').val(postalCode);
            $('#Country').val(country);
            $('#PhoneNumber').select();
        }
    </script>

    <div id="TwoColumnWrapper">
        <% if(User.IsInRole("SalesPoint Administrators")) { %>
        <div id="column-left" class="float-left"><% Html.RenderAction("AdministrationLeft", "Navigation", new { selectedMenuItem = SalesPoint.Core.SelectedNavigationAdministrationLeft.Users }); %></div>
        <% } %>

        <div id="column-right">
            <h2>Edit User<%: ViewData["NewRecordText"] %></h2><%= Html.ActionLink("Back to List", "Index") %>
            <div class="break"></div>

            <% using (Html.BeginForm()) {%>
                <%= Html.ValidationSummary(true) %>
                    <% if (!Model.IsActive) { %>
                        <script type="text/javascript">alert('This user account is inactive.  To reactivate it, place a check next to \'Active?\' before saving.');</script>
                    <% ;} %>
        
                    <div class="ui-widget-header padding-five">Logon Account</div>
                    <div class="editor-label-left extra-top-buffer">Active Directory Name<span class="required"> *</span></div>
                    <div class="editor-field">
                        <%= Html.TextBoxFor(model => model.ActiveDirectoryLogonName)%>
                        <%= Html.ValidationMessageFor(model => model.ActiveDirectoryLogonName) %>
                    </div>
            
                    <div class="editor-label-left">First Name<span class="required"> *</span></div>
                    <div class="editor-field">
                        <%= Html.TextBoxFor(model => model.FirstName) %>
                        <%= Html.ValidationMessageFor(model => model.FirstName) %>
                    </div>
            
                    <div class="editor-label-left">Last Name<span class="required"> *</span></div>
                    <div class="editor-field">
                        <%= Html.TextBoxFor(model => model.LastName) %>
                        <%= Html.ValidationMessageFor(model => model.LastName) %>
                    </div>
            
                    <div class="editor-label-left">Job Title</div>
                    <div class="editor-field">
                        <%= Html.TextBoxFor(model => model.JobTitle) %>
                        <%= Html.ValidationMessageFor(model => model.JobTitle) %>
                    </div>
            
                    <div class="editor-label-left">MasterPack Number</div>
                    <div class="editor-field">
                        <%= Html.TextBoxFor(model => model.MasterPackSalesPersonNumber, new { @class = "Text75" })%>
                        <%= Html.ValidationMessageFor(model => model.MasterPackSalesPersonNumber) %>
                    </div>
                                          
                    <div class="ui-widget-header padding-five">Address</div>
                    <div class="editor-label-left extra-top-buffer">Address</div>
                    <div class="editor-field">
                        <%= Html.TextBoxFor(model => model.Address1, new { @class = "Text300" })%>
                        <%= Html.ValidationMessageFor(model => model.Address1) %>
                    </div>
            
                    <div class="editor-label-left"></div>
                    <div class="editor-field">
                        <%= Html.TextBoxFor(model => model.Address2, new { @class = "Text300" })%>
                        <%= Html.ValidationMessageFor(model => model.Address2) %>
                    </div>
            
                    <div class="editor-label-left">Postal Code</div>
                    <div class="editor-field">
                        <%= Html.TextBoxFor(model => model.PostalCode, new { @class = "Text75", @onblur = "postalCodeLookup(this.value);" })%> <img id="ImageWaitPostalCodeLookup" alt="Checking postal code..." title="Checking postal code..." src="<%=Url.Content("~/Content/Images/Loading.gif")%>" style="display: none;" /> <span id="Zip"></span>
                        <%= Html.ValidationMessageFor(model => model.PostalCode) %>
                    </div>
            
                    <div class="editor-label-left">City</div>
                    <div class="editor-field">
                        <%= Html.TextBoxFor(model => model.City) %>
                        <%= Html.ValidationMessageFor(model => model.City) %>
                    </div>
            
                    <div class="editor-label-left">State</div>
                    <div class="editor-field">
                        <%= Html.TextBoxFor(model => model.State, new { @class = "Text75" })%>
                        <%= Html.ValidationMessageFor(model => model.State) %>
                    </div>
                        
                    <div class="editor-label-left">Country</div>
                    <div class="editor-field">
                        <%= Html.TextBoxFor(model => model.Country) %>
                        <%= Html.ValidationMessageFor(model => model.Country) %>
                    </div>
            
                    <div class="ui-widget-header padding-five">Contact Info</div>
                    <div class="editor-label-left extra-top-buffer">Phone Number</div>
                    <div class="editor-field">
                        <%= Html.TextBoxFor(model => model.PhoneNumber) %>
                        <%= Html.ValidationMessageFor(model => model.PhoneNumber) %>
                    </div>
            
                    <div class="editor-label-left">Fax Number</div>
                    <div class="editor-field">
                        <%= Html.TextBoxFor(model => model.FaxNumber) %>
                        <%= Html.ValidationMessageFor(model => model.FaxNumber) %>
                    </div>
            
                    <div class="editor-label-left">Cell Phone Number</div>
                    <div class="editor-field">
                        <%= Html.TextBoxFor(model => model.CellPhoneNumber) %>
                        <%= Html.ValidationMessageFor(model => model.CellPhoneNumber) %>
                    </div>
            
                    <div class="editor-label-left">Email Address<span class="required"> *</span></div>
                    <div class="editor-field">
                        <%= Html.TextBoxFor(model => model.EmailAddress, new { @class = "Text200" })%>
                        <%= Html.ValidationMessageFor(model => model.EmailAddress) %>
                    </div>
            
                    <div class="ui-widget-header padding-five">Options</div>
                    <div class="editor-label-left">Active?</div>
                    <div class="editor-field">
                        <%= Html.CheckBoxFor(model => model.IsActive) %>
                        <%= Html.ValidationMessageFor(model => model.IsActive) %>
                    </div>

                    <div class="clear"></div>
            
                    <div class="ui-widget-header padding-five">Group Membership</div>
                    <div>
                        <% if (Model.Id > 0) { %>
                            <% foreach (Masonite.MTier.SalesPoint.UserGroup item in Model.UserGroups)
                               { %>
                               <span style="margin-right: 20px;"><%= item.Name %> | <a href="javascript:void(0);" onclick="removeItem(<%= item.Id %>, this.parentNode);">Remove</a></span> 
                            <% ; } %>
                        <% ;} %>
                    </div>
            
                    <p class="required">* Required field</p>
                    <p>
                        <input type="button" value="Cancel" id="CancelButton" onclick="window.location.href = '<%= Url.Action("Index") %>';" class="ui-button ui-widget ui-state-default ui-corner-all" />
                        <input type="submit" value="Save" class="ui-button ui-widget ui-state-default ui-corner-all" />
                    </p>

            <% } %>
        </div>
    </div>
</asp:Content>