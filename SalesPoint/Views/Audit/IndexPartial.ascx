﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<Masonite.MTier.SalesPoint.Audit>>" %>
<script type="text/javascript">
    setMerchantName('<%= ViewData["MerchantName"] %>');
</script>
    
<div class="recordcount">Viewing the top <%= ViewData.Model.Count<Masonite.MTier.SalesPoint.Audit>().ToString()%> result(s).</div>
    
<table style="width:100%;">
    <thead class="ui-widget-header">
        <tr>
            <th>Timestamp</th>
            <th>By</th>
            <th>Object</th>
            <th>Operation</th>
            <th>Old Value</th>
            <th>New Value</th>
        </tr>
    </thead>
    <tbody>
    <% foreach (Masonite.MTier.SalesPoint.Audit item in Model) { %>
        <tr onclick="selectTableRow(this);">
            <td><%: item.TimeStamp %></td>
            <td><%: item.User %></td>
            <td><%: item.ObjectName %>.<%: item.PropertyName %></td>
            <td><%: item.Operation %></td>
            <td><%: item.OldValue %></td>
            <td><%: item.NewValue %></td>
        </tr>
    <% } %>
    </tbody>
</table>