﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<Masonite.MTier.SalesPoint.Registration>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div id="TwoColumnWrapper">
        <div id="column-right">
            <h2>Registrations</h2><%= Html.ActionLink("View All", "Index")%>
            <div class="break"></div>

            <div>
                <% using(Html.BeginForm()) { %>
                    <% Html.RenderAction("IndexPartial", "RegistrationCategory", new { selectedValue = ViewData["RegistrationCategoryId"] }); %>
                    <input type="text" class="editor-field" name="searchName" id="searchName" value="<%: ViewData["searchName"] %>" /> 
                    <input type="submit" value="Search" class="ui-button ui-widget ui-state-default ui-corner-all" />
                    <div class="recordcount">Viewing the top <%= ViewData.Model.Count<Masonite.MTier.SalesPoint.Registration>().ToString()%> result(s).</div>
                <% } %>
            </div>

            <table style="width:100%;">
                <thead class="ui-widget-header">
                    <tr>
                        <th>Registration Type</th>
                        <th>Name</th>
                        <th>Address</th>
                        <th>Created</th>
                    </tr>
                </thead>

                <tbody>
                <% foreach (Masonite.MTier.SalesPoint.Registration item in Model) { %>
                    <tr onclick="selectTableRow(this);">
                        <td><%: item.RegistrationCategoryName %></td>
                        <td><%= Html.ActionLink(item.LastName + (string.IsNullOrEmpty(item.FirstName) ? "" : ", " + item.FirstName), "Edit", new { id = item.Id })%></td>
                        <td>
                            <%: item.Address1 %>
                            <% if (!string.IsNullOrEmpty(item.Address2)) { %><br /><%: item.Address2 %><% } %>
                            <br />
                            <%: item.City %>, <% if (!string.IsNullOrEmpty(item.CountrySubdivision)) { %><%: item.CountrySubdivision %><% } %>&nbsp;<%: item.PostalCode %>
                        </td>
                        <td><%: String.Format(System.Globalization.CultureInfo.CurrentUICulture, "{0:g}", item.DateCreated) %> by <%: item.CreatedBy %></td>
                    </tr>
                <% } %>
                </tbody>
            </table>
        </div>
    </div>
</asp:Content>