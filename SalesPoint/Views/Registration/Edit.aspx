﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<SalesPoint.ViewModels.RegistrationModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div id="TwoColumnWrapper">
        <div id="column-right">
            <h2>Registration</h2><%= Html.ActionLink("Back to List", "Index") %>
            <div class="break"></div>

            <table style="width:100%;">
                <thead class="ui-widget-header">
                    <tr>
                        <th>Field</th>
                        <th>Value</th>
                    </tr>
                </thead>

                <tbody>
                    <tr>
                        <td>Registration Type</td>
                        <td><%: Model.Registration.RegistrationCategoryName %></td>
                    </tr>
                    <tr>
                        <td>First Name</td>
                        <td><%: Model.Registration.FirstName %></td>
                    </tr>
                    <tr>
                        <td>Last Name</td>
                        <td><%: Model.Registration.LastName %></td>
                    </tr>
                    <tr>
                        <td>Email Address</td>
                        <td><%: Model.Registration.EmailAddress %></td>
                    </tr>
                    <tr>
                        <td>Phone Number</td>
                        <td><%: Model.Registration.PhoneNumber %></td>
                    </tr>
                    <tr>
                        <td>Fax Number</td>
                        <td><%: Model.Registration.FaxNumber %></td>
                    </tr>
                    <tr>
                        <td>Address 1</td>
                        <td><%: Model.Registration.Address1 %></td>
                    </tr>
                    <tr>
                        <td>Address 2</td>
                        <td><%: Model.Registration.Address2 %></td>
                    </tr>
                    <tr>
                        <td>City</td>
                        <td><%: Model.Registration.City %></td>
                    </tr>
                    <tr>
                        <td>State/Province</td>
                        <td><%: Model.Registration.CountrySubdivision %></td>
                    </tr>
                    <tr>
                        <td>Postal Code</td>
                        <td><%: Model.Registration.PostalCode %></td>
                    </tr>
                    <tr>
                        <td>Country</td>
                        <td><%: Model.Registration.Country %></td>
                    </tr>

                    <% if(Model.RegistrationAttributes.Count() > 0) { %>
                    <tr>
                        <td>&nbsp;</td>
                        <td></td>
                    </tr>
                    <% } %>

                    <% foreach (Masonite.MTier.SalesPoint.RegistrationAttribute item in Model.RegistrationAttributes) { %>
                        <tr onclick="selectTableRow(this);">
                            <td><%: item.Key %></td>
                            <td><%: item.Value %></td>
                        </tr>
                    <% } %>
                </tbody>
            </table>
        </div>
    </div>
</asp:Content>