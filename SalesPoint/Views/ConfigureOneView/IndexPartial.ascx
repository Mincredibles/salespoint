﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>

<select name="DefaultMaxView" id="DefaultMaxView">
    <option value=""></option>
    <option <%if (ViewData["DefaultMaxView"].ToString() == "List"){%>selected="selected"<%}%> value="List">List</option>
    <option <%if (ViewData["DefaultMaxView"].ToString() == "Graphical"){%>selected="selected"<%}%> value="Graphical">Graphical</option>
</select>

