﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<Masonite.MTier.SalesPoint.User>>" %>

<asp:content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">
        var isPageReloadNeeded = false;
        var lastElement;
        var userGroupId = '<%= ViewData["UserGroupId"] %>';
        $(function () {
            $("#dialog-modal").dialog({
                height: 390,
                width: 584,
                modal: true,
                autoOpen: false,
                resizable: false,
                close: dialogClose
            });
        });
        function dialogClose() {
            if (isPageReloadNeeded == true) {
                window.location.reload();
            }
            else {
                $('#result').empty()
            }
        }
        function userGroupUserDialog() {
            $('#dialog-content').load('<%= VirtualPathUtility.ToAbsolute("~/UserGroupUser/ChooseUsersPartial")%>');
            $('#dialog-modal').dialog('open');
            $('#searchName').select();
        }
        function addUser(userId, span) {
            $('#Wait').show();
            
            lastElement = span;
            isPageReloadNeeded = true;
            
            $.ajax({
                type: "POST",
                url: '<%= VirtualPathUtility.ToAbsolute("~/UserGroupUser/EditFromPartial/") %>' + userGroupId + '?userId=' + userId
            }).done(function () {
                $('#Wait').hide();
                if (lastElement) { $(lastElement).hide(); }
                isPageReloadNeeded = true;
            });
        }
        function removeUser(userId, span) {
            lastElement = span;

            $.ajax({
                type: "DELETE",
                url: '<%= VirtualPathUtility.ToAbsolute("~/UserGroupUser/Delete/") %>' + userGroupId + '?userId=' + userId
            }).done(function () {
                if (lastElement) {
                    $(lastElement).parent().parent().hide();
                }
            });
        }
    </script>

    <div id="TwoColumnWrapper">
        <% if(User.IsInRole("SalesPoint Administrators")) { %>
        <div id="column-left" class="float-left"><% Html.RenderAction("AdministrationLeft", "Navigation", new { selectedMenuItem = SalesPoint.Core.SelectedNavigationAdministrationLeft.UserGroups }); %></div>
        <% } %>

        <div id="column-right">
            <h2>Members of '<%: Masonite.MTier.SalesPoint.ObjectFactory.RetrieveUserGroup(Masonite.MTier.Utility.ConversionHelper.ToInt32(ViewContext.RouteData.Values["id"].ToString()), User.Identity.Name).Name %>'</h2><%= Html.ActionLink("Back to User Groups", "Index", "UserGroup")%> | <a href="javascript:void(0);" onclick="userGroupUserDialog();">Add Users</a>
            <div class="break"></div>

            <div class="recordcount">Viewing the top <%= ViewData.Model.Count<Masonite.MTier.SalesPoint.User>().ToString()%> result(s).</div>
    
            <table style="width:100%;">
                <thead class="ui-widget-header">
                    <tr>
                        <th>Location</th>
                        <th></th>
                    </tr>
                </thead>

                <% foreach (Masonite.MTier.SalesPoint.User item in Model) { %>
                    <tr onclick="selectTableRow(this);">
                        <td><%: item.ActiveDirectoryLogonName %></td>
                        <td class="cellalignmiddle"><img alt="Remove" title="Remove" src="<%=Url.Content("~/Content/Images/Delete.png")%>" onclick="removeUser(<%= item.Id %>, this);" class="gridicon" /></td>
                    </tr>
                <% } %>

            </table>
        </div>
    </div>
    
    <div id="dialog-modal" title="Add Users">
    <% using (Ajax.BeginForm("ChooseUsersPartial", "UserGroupUser", new AjaxOptions { UpdateTargetId = "result", HttpMethod = "Post", LoadingElementId = "Wait" })) { %>
        <div>
            <input id="searchName" name="searchName" type="text" value="<%: ViewData["searchName"] %>" />
            <input type="submit" value="Search" class="ui-button ui-widget ui-state-default ui-corner-all" />
        </div>
        <div class="min-height-15"><img src="<%= Url.Content("~/Content/Images/AjaxWait.gif") %>" alt="Working..." title="Working..." id="Wait" /></div>
        <div id="dialog-content"></div>
    <% } %>
    </div>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#Wait').hide();
        });
    </script>
</asp:content>