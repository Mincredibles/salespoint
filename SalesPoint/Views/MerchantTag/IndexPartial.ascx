﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<Masonite.MTier.SalesPoint.MerchantTag>>" %>

<% foreach(Masonite.MTier.SalesPoint.MerchantTag tag in Model) { %>
    <span id="Tag<%= tag.Id %>" class="TagWrapper"><span class="TagLeft">&nbsp;</span><span class="TagCenter"><%= tag.Text %></span><span class="TagRight">&nbsp;</span><span onmouseover="this.className='TagDeleteHover';" onmouseout="this.className='TagDelete';" onclick="deleteTag(<%= tag.Id %>);" class="TagDelete">&nbsp;</span></span>
<% ;} %>
<script type="text/javascript">img=new Image();img.src='<%=Url.Content("~/Content/Images/TagDeleteHover.png")%>';</script>
