﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<Masonite.MTier.SalesPoint.MerchantContactConfigureOne>>" %>

<% using (Ajax.BeginForm("IndexEdit", "MerchantContactConfigureOne", new { merchantContactId = ViewData["merchantContactId"] }, new AjaxOptions { HttpMethod = "Post", OnSuccess = "saveMerchantContactConfigureOneCompleted" })) { %>
    <%= Html.ValidationSummary(true) %>

    <% if(ViewData.Model.Count<Masonite.MTier.SalesPoint.MerchantContactConfigureOne>() > 0) {%>
        <table id="result" class="no-border">
            <thead class="ui-widget-header">
                <tr>
                    <th>Location</th>
                    <th>Receive<br /> Cart<br /> Emails?</th>
                    <th>Permissions</th>
                </tr>
            </thead>
                
            <% foreach (Masonite.MTier.SalesPoint.MerchantContactConfigureOne item in Model) { %>
            <tr">
                <td><%: item.MerchantDealerName %> (<%: item.MerchantDistributorName %>)</td>
                <td class="cellalignmiddle"><input type="checkbox" name="IsReceivingCartEmail" value="<%=item.Id %>" <%= item.IsReceivingCartEmail ? "checked='checked'" : "" %> /></td>
                <td>
                    <%
                        if (!string.IsNullOrEmpty(item.ConfigureOneUserId))
                            Html.RenderAction("IndexPartial", "MerchantContactConfigureOnePermission", new { userId = item.ConfigureOneUserId });
                    %>
                </td>
            </tr>
            <% } %>

        </table>

        <p>
            <input type="submit" value="Save" onclick="$('#ImageWaitMerchantContactConfigureOnePartial').show();" class="ui-button ui-widget ui-state-default ui-corner-all" /><img id="ImageWaitMerchantContactConfigureOnePartial" alt="Saving..." title="Saving..." src="<%=Url.Content("~/Content/Images/AjaxWait.gif")%>" style="display: none;" />
        </p>
    <%} else { %>
        <span>The current contact does not have any associations with Max.  This issue can normally be resolved by opening the contact record for edit, and then simply clicking the Save button.  This will trigger creation of the necessary Max associations, and then allow you to set the Contact's Max Options.</span>
    <% } %>
        
<% } %>