﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<Masonite.MTier.SalesPoint.RegistrationCategory>>" %>

<%@ Import Namespace="Masonite.MTier.Utility" %>

<% int selectedValue = ViewData["SelectedValue"].ToInt32(); %>

<select name="RegistrationCategoryId" id="RegistrationCategoryId">
    <option value=""> </option>

<% foreach (Masonite.MTier.SalesPoint.RegistrationCategory item in Model) { %>
    <option value="<%= item.Id %>" <% if(selectedValue == item.Id){ %>selected="selected"<% } %>><%: item.Name %></option>
<% } %>
</select>