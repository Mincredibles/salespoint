﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<SalesPoint.ViewModels.CorrespondenceIndexModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div id="TwoColumnWrapper">
        <div id="column-right">
            <h2>eBlasts &amp; Letters</h2><%= Html.ActionLink("View All", "Index")%> <% if (Page.User.IsInRole("SalesPoint Administrators") || Page.User.IsInRole("Marketing Team")) { %>| <%= Html.ActionLink("Create New", "Edit")%><% } %>
            <div class="break"></div>

            <div>
                <% using(Html.BeginForm()) { %>
                    <input type="text" class="editor-field" name="searchName" id="searchName" value="<%: ViewData["searchName"] %>" /> 
                    <input type="submit" value="Search" class="ui-button ui-widget ui-state-default ui-corner-all" />
                    <div class="recordcount">Viewing the top <%= ViewData.Model.Correspondences.Count<Masonite.MTier.SalesPoint.Correspondence>().ToString()%> result(s).</div>
                <% } %>
            </div>

            <table style="width:100%;">
                <thead class="ui-widget-header">
                    <tr>
                        <th>Correspondence</th>
                        <th>Extended Description</th>
                        <th>Scheduled Delivery</th>
                        <th>Created</th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>

                <tbody>
                <% foreach (Masonite.MTier.SalesPoint.Correspondence item in Model.Correspondences) { %>
                    <tr onclick="selectTableRow(this);">
                        <% if (Page.User.IsInRole("SalesPoint Administrators") || Page.User.IsInRole("Marketing Team")) { %>
                        <td><%= Html.ActionLink(item.Name, "Edit", new { id = item.Id })%></td>
                        <% } else { %>
                        <td><%: item.Name %></td>
                        <% } %>

                        <td><%: item.Description %></td>
                        <td><%: String.Format(System.Globalization.CultureInfo.CurrentUICulture, "{0:d}", item.ScheduledDeliveryDate) %></td>
                        <td><%: String.Format(System.Globalization.CultureInfo.CurrentUICulture, "{0:g}", item.DateCreated) %> by <%: item.CreatedBy %></td>
                        <td><%= Html.ActionLink("View Recipients", "IndexToExcel", "CorrespondenceRecipient", new { id = item.Id }, new { })%></td>

                        <% if (Page.User.IsInRole("Correspondence Approvers")) { %>
                            <% Masonite.MTier.SalesPoint.CorrespondenceApproval approval = Model.CorrespondenceApprovals.Find(m => m.CorrespondenceId == item.Id); %>
                            <% if(approval != null) { %>
                                <td><img src="<%= Url.Content("~/Content/Images/Check.png") %>" class="gridicon" alt="Approved" title="Approved" /> I approved on <%: String.Format(System.Globalization.CultureInfo.CurrentUICulture, "{0:g}", approval.DateCreated) %></td>
                            <% } else { %>
                                <td><input type="button" value="Approve" onclick="approve(<%= item.Id %>);" class="ui-button ui-widget ui-state-default ui-corner-all" /></td>
                            <% } %>
                        <% } else { %>
                            <td></td>
                        <% } %>
                    </tr>
                <% } %>
                </tbody>
            </table>
        </div>
    </div>
    <script type="text/javascript">
        function approve(correspondenceId) {
            $.ajax({
                type: "POST",
                url: '<%= VirtualPathUtility.ToAbsolute("~/CorrespondenceApproval/Approve") %>?id=' + correspondenceId
            }).done(function () {
                document.location.href = document.location.href;
            });
        }
    </script>
</asp:Content>