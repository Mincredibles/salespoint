﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Masonite.MTier.SalesPoint.Correspondence>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div id="TwoColumnWrapper">
        <div id="column-right">
            <h2>Edit Correspondence<%: ViewData["NewRecordText"] %></h2><%= Html.ActionLink("Back to List", "Index") %>
            <div class="break"></div>

            <% using (Html.BeginForm()) {%>
                <%= Html.ValidationSummary(true) %>
        
                <div class="ui-widget-header padding-five">Details</div>
                <div class="editor-label-left extra-top-buffer">Name<span class="required"> *</span></div>
                <div class="editor-field">
                    <%= Html.TextBoxFor(model => model.Name, new { @class = "Text300" }) %>
                    <%= Html.ValidationMessageFor(model => model.Name) %>
                </div>

                <div class="clear"></div>
        
                <div class="editor-label-left">Extended Description</div>
                <div class="editor-field">
                    <%= Html.TextBoxFor(model => model.Description, new { @class = "Text300" })%>
                    <%= Html.ValidationMessageFor(model => model.Description) %>
                </div>
                
                <div class="clear"></div>

                <div class="editor-label-left">Scheduled Delivery</div>
                <div class="editor-field">
                    <% if(Masonite.MTier.Utility.DateHelper.IsValidSqlDate(Model.ScheduledDeliveryDate)) { %>
                        <%: Html.TextBoxFor(model => model.ScheduledDeliveryDate, "{0:d}") %>
                    <% } else { %>
                        <%: Html.TextBoxFor(model => model.ScheduledDeliveryDate, new { Value = "" }) %>
                    <% } %>
                    <%= Html.ValidationMessageFor(model => model.ScheduledDeliveryDate) %>
                </div>
                
                <div class="clear"></div>

                <div class="editor-label-left">SQL Statement</div>
                <div class="editor-field">
                    <%= Html.TextAreaFor(model => model.RecipientListSqlStatement, new { @class = "Text300", @rows = "8" }) %>
                    <%= Html.ValidationMessageFor(model => model.RecipientListSqlStatement) %>
                </div>
        
                <p class="required">* Required field</p>
                <p>
                    <input type="button" value="Cancel" id="CancelButton" onclick="window.location.href = '<%= Url.Action("Index") %>';" class="ui-button ui-widget ui-state-default ui-corner-all" />
                    <input type="submit" value="Save" class="ui-button ui-widget ui-state-default ui-corner-all" />
                </p>

            <% } %>

            <div class="break"></div>

            <% Html.RenderAction("IndexPartial", "CorrespondenceApproval", new { id = Model.Id }); %>
        </div>
    </div>
</asp:Content>