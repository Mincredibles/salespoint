﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<Masonite.MTier.SalesPoint.MaxVersion>>" %>

<select name="MaxVersionId" id="MaxVersionId">
    <option value="0">Default</option>
<% foreach (var item in Model) { %>
    <option <%if (ViewData["SelectedValue"].ToString() == item.Id.ToString()){%>selected="selected"<%}%> value="<%= item.Id %>"><%: item.Version %></option>
<% } %>
</select>