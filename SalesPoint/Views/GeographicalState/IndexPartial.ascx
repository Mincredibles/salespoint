﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<Masonite.MTier.SalesPoint.GeographicalState>>" %>
<select id='<%= ViewData["controlName"] %>' name="<%= ViewData["controlName"] %>"<% if((bool)ViewData["disableControl"] == true){ %> disabled="disabled"<% ;} %>>
    <% if((bool)ViewData["includeBlankItem"] == true) { %><option value=""> </option><% ;} %>
    <% foreach (Masonite.MTier.SalesPoint.GeographicalState item in Model) { %>
        <option value="<%= item.Id %>"<% if(ViewData["selectedItemId"] != null && item.Id.ToString(System.Globalization.CultureInfo.CurrentUICulture) == ViewData["selectedItemId"].ToString()) { %> selected='selected'<% ;} %>><%: item.Name %></option><% } %>
</select>