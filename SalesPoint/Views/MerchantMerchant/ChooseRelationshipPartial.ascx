﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<Masonite.MTier.SalesPoint.Merchant>>" %>

<table id="result" class="no-border">
<% if(ViewData.Model.Count<Masonite.MTier.SalesPoint.Merchant>() == 0) { %>
        <tr><td class="no-border">No items match the current search criteria</td></tr>
<% } else { %>
    <% foreach (Masonite.MTier.SalesPoint.Merchant item in Model) { %>
        <tr>
            <td class="no-border"><a href="javascript:void(0);" onclick="addMerchant(<%= item.Id %>,this);">Add</a></td>
            <td class="no-border">
                <%: item.Name %>
                <br />
                <%= item.City %>, <%= item.StateName %>
            </td>
        </tr>
    <% } %>
<% } %>
</table>