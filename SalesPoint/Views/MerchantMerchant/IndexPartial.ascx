﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Masonite.MTier.SalesPoint.Merchant>" %>
    
<% Masonite.MTier.SalesPoint.Merchant merchant = Masonite.MTier.SalesPoint.ObjectFactory.RetrieveMerchant(Masonite.MTier.Utility.ConversionHelper.ToInt32(ViewData["MerchantId"].ToString()), Page.User.Identity.Name); %>
    
<script type="text/javascript">
    var isPageReloadNeeded = false;
    var lastElement;
    var merchantId = '<%= ViewData["MerchantId"] %>';
    var editing; // p = parent, c = child
    $(function () {
        $("#dialog-modal").dialog({
            height: 450,
            width: 600,
            modal: true,
            autoOpen: false,
            resizable: false,
            close: dialogClose
        });
    });
    function dialogClose() {
        if (isPageReloadNeeded == true) {
            window.location.reload();
        }
        else {
            $('#result').empty()
        }
    }
    function merchantDialog() {
        $('#dialog-content').load('<%= VirtualPathUtility.ToAbsolute("~/MerchantMerchant/ChooseRelationshipPartial")%>');
        $('#dialog-modal').dialog('open');
        $('#searchName').select();
    }
    function addMerchant(associatedMerchantId, span) {
        $('#Wait').show();

        lastElement = span;
        isPageReloadNeeded = true;

        var action;
        if (editing == 'p') {
            action = '<%= VirtualPathUtility.ToAbsolute("~/MerchantMerchant/Edit") %>?parentMerchantId=' + associatedMerchantId + '&childMerchantId=' + merchantId;
        }
        else {
            action = '<%= VirtualPathUtility.ToAbsolute("~/MerchantMerchant/Edit") %>?parentMerchantId=' + merchantId + '&childMerchantId=' + associatedMerchantId;
        }

        $.ajax({
            type: "POST",
            url: action
        }).done(function () {
            $('#Wait').hide();
            if (lastElement) { $(lastElement).hide(); }
            isPageReloadNeeded = true;
        });
    }
    function removeMerchant(associatedMerchantId, span) {
        lastElement = span;

        var action;
        if (editing == 'p') {
            action = '<%= VirtualPathUtility.ToAbsolute("~/MerchantMerchant/Delete") %>?parentMerchantId=' + associatedMerchantId + '&childMerchantId=' + merchantId;
        }
        else {
            action = '<%= VirtualPathUtility.ToAbsolute("~/MerchantMerchant/Delete") %>?parentMerchantId=' + merchantId + "&childMerchantId=" + associatedMerchantId;
        }

        $.ajax({
            type: "DELETE",
            url: action
        }).done(function () {
            $(lastElement).parent().parent().hide();
            if (editing == 'p') {
                $('#LinkEditParent').show();
            }
        });
    }
    setMerchantName('<%= ViewData["MerchantName"] %>');
</script>
<div id="ContentElementRelationships">
    <div style="margin-bottom: 5px;">
        <%if (!string.IsNullOrEmpty(merchant.MasterPackId)) { %>
        <input type="button" value="Add Parent" id="LinkEditParent" onclick="editing='p';merchantDialog();" class="ui-button ui-widget ui-state-default ui-corner-all" />
        <%} else {%>
        <span>&nbsp;</span>
        <%}%>
        <div class="recordcount">Viewing the top <%= merchant.Parents.Count.ToString()%> result(s).</div>
    </div>
        
    <table style="width:100%; margin-bottom:15px">
        <thead class="ui-widget-header">
            <tr>
                <th style="width:50%">Who can see my orders (My Parent)</th>
                <th style="width:10%"></th>
                <th></th>
                <th></th>
            </tr>
        </thead>

    <% foreach (Masonite.MTier.SalesPoint.MerchantMerchant item in merchant.Parents) { %>
        <tr onclick="selectTableRow(this);">
            <td><%: item.ParentMerchantName %></td>
            <td>MP# <%: item.ParentMerchantMasterPackNumber %></td>
            <td><%: item.ParentMerchantCity %>, <%: item.ParentMerchantStateName %></td>
            <td class="cellalignmiddle"><img alt="Remove" title="Remove" src="<%=Url.Content("~/Content/Images/Delete.png")%>" onclick="editing='p';removeMerchant('<%= item.ParentMerchantId %>',this);" class="gridicon" /></td>
        </tr>
    <% } %>
    </table>
        
    <div style="margin-bottom: 5px;">
        <%if (!string.IsNullOrEmpty(merchant.MasterPackId)) { %>
        <input type="button" value="Add Child" onclick="editing='c';merchantDialog();" class="ui-button ui-widget ui-state-default ui-corner-all" />
        <%} else {%>
        <span>&nbsp;</span>
        <%}%>
        <div class="recordcount">Viewing the top <%= merchant.Children.Count.ToString()%> result(s).</div>
    </div>
        
    <table style="width:100%;">
        <thead class="ui-widget-header">
            <tr>
                <th style="width:50%">Whose orders I can see (My Children)</th>
                <th style="width:10%"></th>
                <th></th>
                <th></th>
            </tr>
        </thead>
        
    <% foreach (Masonite.MTier.SalesPoint.MerchantMerchant item in merchant.Children) { %>
        <tr onclick="selectTableRow(this);">
            <td><%: item.ChildMerchantName %></td>
            <td>MP# <%: item.ChildMerchantMasterPackNumber %></td>
            <td><%: item.ChildMerchantCity %>, <%: item.ChildMerchantStateName %></td>
            <td class="cellalignmiddle"><img alt="Remove" title="Remove" src="<%=Url.Content("~/Content/Images/Delete.png")%>" onclick="editing='c';removeMerchant('<%= item.ChildMerchantId %>',this);" class="gridicon" /></td>
        </tr>
    <% } %>
    </table>
</div>

<div id="dialog-modal" title="Add Relationship">
<% using (Ajax.BeginForm("ChooseRelationshipPartial", "MerchantMerchant", new AjaxOptions { UpdateTargetId = "result", HttpMethod = "Post", LoadingElementId = "Wait" })) { %>
    <div>    
        <input id="searchName" name="searchName" type="text" value="<%= Html.Encode(ViewData["searchName"])%>" />
        <input type="submit" value="Search" class="ui-button ui-widget ui-state-default ui-corner-all" />
    </div>
    <div class="min-height-15"><img src="<%= Url.Content("~/Content/Images/AjaxWait.gif") %>" alt="Working..." title="Working..." id="Wait" /></div>
    <div id="dialog-content"></div>
<% } %>
</div>

<script type="text/javascript">
    $("#Wait").hide();
    <% if(merchant.Parents.Count > 0) { %> $("#LinkEditParent").hide(); <% } %>
</script>