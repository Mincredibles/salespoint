﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<Masonite.MTier.SalesPoint.MerchantMerchantContact>>" %>

<% int merchantId = Masonite.MTier.Utility.ConversionHelper.ToInt32(ViewData["MerchantId"].ToString()); %>
<table class="no-border full-width">
    <% foreach (Masonite.MTier.SalesPoint.MerchantMerchantContact item in Model) { %>
        <tr id="<%=item.MerchantId %>">
            <td>
                <% if (merchantId != item.MerchantId) { %>
                    <%= Html.ActionLink(item.MerchantName, "Edit", "Merchant", new { id = item.MerchantId, tab = (int)SalesPoint.MerchantEditTab.Contacts }, new { })%>
                <% } else { %>
                    <%: item.MerchantName %>
                <% } %>
            </td>
            <td>
                <% if(item.IsPrimaryMerchantForContact) { %>
                    <div class="label label-success" title="This is the contact's primary location.">Primary</div>
                <% } else { %>
                    <div class="label label-info pointer" title="Set this as the contact's primary location." onclick="setPrimaryMerchantMerchantContact(<%= item.MerchantId %>,<%= item.MerchantContactId %>,this);">Set</div>
                <% } %>
            </td>
            <td>
                <% if (merchantId != item.MerchantId && Model.Count() > 1) { %>
                    <img alt="Remove Link" title="Remove Link" src="<%=Url.Content("~/Content/Images/Delete.png")%>" onclick="removeMerchantMerchantContact(<%=item.MerchantId%>,<%=item.MerchantContactId%>,this.parentElement);" class="gridicon" />
                <% } %>
            </td>
        </tr>
    <% } %>
</table>