﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<Masonite.MTier.SalesPoint.MerchantDistributorDealer>>" %>

<% var deletableItems = Model.Where(d => !d.IsDistributorSupplyingProductEntry && !d.IsDealerReceivingCartsEntry && !d.IsDistributorSupplyingProductInterior && !d.IsDealerReceivingCartsInterior).ToList(); %>

<style type="text/css">
    .rb
    {
        border-right-style: solid;
        border-right-width: 1px;
        border-right-color: #BDCEDF
    }
</style>
    
<script type="text/javascript">
    var lastElement;
    var merchantId = '<%= ViewData["MerchantId"] %>';
    var isPageReloadNeeded = false;
    function addDistributor(distributorId, span) {
        $('#Wait').show();

        lastElement = span;
        isPageReloadNeeded = true;

        $.ajax({
            type: "POST",
            url: '<%= VirtualPathUtility.ToAbsolute("~/MerchantDistributorDealer/EditDistributorPartial") %>?distributorId=' + distributorId + '&dealerId=' + merchantId
        }).done(function () {
            addCompleted();
        });
    }
    function addCompleted() {
        $('#Wait').hide();
        if (lastElement) { $(lastElement).hide(); }
        isPageReloadNeeded = true;
    }
    setMerchantName('<%= ViewData["MerchantName"] %>');
    $(function () {
        $("#dialog-modal").dialog({
            height: 300,
            width: 600,
            modal: true,
            autoOpen: false,
            resizable: false,
            close: dialogClose
        });
        $("#dialog-modal-remove").dialog({
            height: 200,
            width: 600,
            modal: true,
            autoOpen: false,
            resizable: false
        });
    });
    function dialogClose() {
        if (isPageReloadNeeded == true) {
            window.location.reload();
        }
        else {
            $('#result').empty()
        }
    }
    function merchantDistributorDealerDialog() {
        $('#dialog-modal').dialog('open');
        $('#searchName').select();
    }
    function removeMerchantDistributorDealerDialog() {
        $('#dialog-modal-remove').dialog('open');
    }
    function markForRemoval(id) {
        $('#remove-mark-' + id).val(1);
        $('#remove-item-' + id).hide();
    }
    function cancelRemoval() {
        $('[id^=remove-mark-').val(0);
        $('[id^=remove-item-').show();
        $('#dialog-modal-remove').dialog('close');
    }
</script>

<div id="ContentElementDistributorDealer">
    <div style="margin-bottom: 5px;">
        <input type="button" value="Add Distributor" onclick="merchantDistributorDealerDialog();" class="ui-button ui-widget ui-state-default ui-corner-all" />
        <% if (false && deletableItems.Any()) { // temporarily disabled pending further data model analysis... %>
            <input type="button" value="Remove Distributor" onclick="removeMerchantDistributorDealerDialog();" class="ui-button ui-widget ui-state-default ui-corner-all" />
        <% } %>
        <div class="recordcount">Viewing the top <%= ViewData.Model.Count<Masonite.MTier.SalesPoint.MerchantDistributorDealer>().ToString()%> result(s).</div>
    </div>
        
    <% using (Html.BeginForm("IndexEdit", "MerchantDistributorDealer", new { merchantId = ViewData["id"] })) { %>
        <%= Html.ValidationSummary(true) %>
        <%
            bool hasEntryCartSelected = false;
            bool hasInteriorCartSelected = false;
        %>
        <table style="width:100%; margin-bottom:15px">
            <thead class="ui-widget-header">
                <tr>
                    <th>Distributor</th>
                    <th>Entry Doors?</th>
                    <th>Receive<br />Carts?</th>
                    <th>Interior Doors?</th>
                    <th>Receive<br />Carts?</th>
                    <th>Contacts</th>
                    <th>Max Options</th>
                </tr>
            </thead>
                
        <% foreach (Masonite.MTier.SalesPoint.MerchantDistributorDealer item in Model) { %>
        <%
                if (item.IsDealerReceivingCartsEntry) { hasEntryCartSelected = true; }
                if (item.IsDealerReceivingCartsInterior) { hasInteriorCartSelected = true; }
        %>
            <tr onclick="selectTableRow(this);">
                <td class="rb align-top">
                    <div class="label label-default"><%: item.MerchantDistributorName %></div>
                    <div class="break"></div>
                    <div>Max ID: <%: item.ConfigureOneWorkgroupId %></div>
                    <div>MyMax Name: <%: item.MyMaxName %></div>
                    <div>MyMax Display Name: <%: item.MyMaxDisplayName %></div>
                    <input type="hidden" value="<%=item.Id %>" name="MerchantDistributorDealerId" /></td>
                <td class="cellalignmiddle"><input type="checkbox" name="IsDistributorSupplyingProductEntry" value="<%=item.Id %>" <%=item.IsDistributorSupplyingProductEntry ? "checked='checked'" : "" %> /></td>
                <td class="cellalignmiddle rb"><input type="radio" name="IsDealerReceivingCartsEntry" value="<%=item.Id %>" <%=item.IsDealerReceivingCartsEntry ? "checked='checked'" : "" %> /></td>
                <td class="cellalignmiddle"><input type="checkbox" name="IsDistributorSupplyingProductInterior" value="<%=item.Id %>" <%=item.IsDistributorSupplyingProductInterior ? "checked='checked'" : "" %> /></td>
                <td class="cellalignmiddle rb"><input type="radio" name="IsDealerReceivingCartsInterior" value="<%=item.Id %>" <%=item.IsDealerReceivingCartsInterior ? "checked='checked'" : "" %> /></td>
                <td class="align-top">
                    <div class="padding-five">
                        <div>P/O Email Address</div>
                        <div><input type="text" value="<%=item.PurchaseOrderSupportEmailAddress %>" name="PurchaseOrderSupportEmailAddress" /></div>
                    </div>

                    <div class="break"></div>

                    <div class="padding-five">
                        <div>Distributor Sales Rep Name</div>
                        <div><input type="text" value="<%=item.DistributorSalesRepName %>" name="DistributorSalesRepName" /></div>
                    </div>

                    <div class="break"></div>

                    <div class="padding-five">
                        <div>Distributor Sales Rep Email</div>
                        <div><input type="text" value="<%=item.DistributorSalesRepEmailAddress %>" name="DistributorSalesRepEmailAddress" /></div>
                    </div>
                </td>
                <td class="align-top">
                    <div class="padding-five">
                        <div class="float-left Text100">Default Product</div>
                        <% Html.RenderAction("IndexPartial", "ConfigureOneProductLine", new { configureOneProductLineId = item.ConfigureOneProductLineId }); %>
                    </div>

                    <div class="break"></div>

                    <div class="padding-five">
                        <div class="float-left Text100">Default View</div>
                        <% Html.RenderAction("IndexPartial", "ConfigureOneView", new { defaultMaxView = item.DefaultMaxView }); %>
                        <input type="checkbox" name="IsDealerAllowedToSwitchMaxView" value="<%=item.Id %>" <%=item.IsDealerAllowedToSwitchMaxView ? "checked='checked'" : "" %> /> Can Switch?
                    </div>

                    <div class="break"></div>

                    <div class="padding-five">
                        <div class="float-left Text100">Price List</div>
                        <input type="text" value="<%=item.PriceList %>" name="PriceList" />
                    </div>

                    <div class="break"></div>

                    <div class="padding-five">
                        <div class="float-left Text100">Secondary Discount Type</div>
                        <input type="text" value="<%=item.SecondaryDiscountType %>" name="SecondaryDiscountType" />
                    </div>

                    <div class="break"></div>

                    <div class="padding-five">
                        <div class="float-left Text100">Max Version</div>
                        <% Html.RenderAction("IndexPartial", "MaxVersion", new { maxVersionId = item.MaxVersionId }); %>
                    </div>

                    <div class="break"></div>

                    <div class="ui-widget-header padding-five">Permissions</div>

                    <% Html.RenderAction("IndexPartial", "MerchantDistributorDealerPermission", new { workgroupId = item.ConfigureOneWorkgroupId }); %>
                </td>
            </tr>
        <% } %>

        <% if(ViewData.Model.Count<Masonite.MTier.SalesPoint.MerchantDistributorDealer>() > 0) { %>
            <tr>
                <td class="rb"></td>
                <td style="text-align:right; white-space:nowrap;">Receive No Carts</td>
                <td class="cellalignmiddle rb"><input type="radio" name="IsDealerReceivingCartsEntry" value="0" <%= hasEntryCartSelected == false ? "checked='checked'" : "" %> /></td>
                <td style="text-align:right; white-space:nowrap;">Receive No Carts</td>
                <td class="cellalignmiddle rb"><input type="radio" name="IsDealerReceivingCartsInterior" value="0" <%= hasInteriorCartSelected == false ? "checked='checked'" : "" %> /></td>
                <td></td>
                <td></td>
            </tr>
        <%} %>

        </table>

        <p>
            <input type="submit" value="Save" class="ui-button ui-widget ui-state-default ui-corner-all" />
        </p>
    <% } %>
</div>

<div id="dialog-modal" title="Add Distributor">
<% using (Ajax.BeginForm("ChooseDistributorPartial", "MerchantDistributorDealer", new AjaxOptions { UpdateTargetId = "result", HttpMethod = "Post", LoadingElementId = "Wait" })) { %>
    <input type="hidden" name="merchantId" id="merchantId" value="<%= ViewData["MerchantId"] %>" />
    <input type="text" class="editor-field" name="searchName" id="searchName" />
    <input type="submit" value="Search" class="ui-button ui-widget ui-state-default ui-corner-all" />
    <input type="submit" value="Show All" onclick="$('#searchName').val('');" class="ui-button ui-widget ui-state-default ui-corner-all" /> <img id="Wait" alt="Loading" title="Loading" src="<%=Url.Content("~/Content/Images/Loading.gif")%>" />
    <div id="result"></div>
<% } %>
</div>

<div id="dialog-modal-remove" title="Edit Distributors">
<% using (Html.BeginForm("RemoveDistributorDealers", "MerchantDistributorDealer", new { merchantId = ViewData["id"] })) { %>
    <table class="no-border full-width merchant-edit">
        <% foreach (Masonite.MTier.SalesPoint.MerchantDistributorDealer item in deletableItems) { %>
            <tr id="remove-item-<%=item.Id %>">
                <td>
                    <%: item.MerchantDistributorName %>
                    <input id="remove-mark-<%=item.Id %>" name="remove-mark-<%=item.Id %>" type="hidden" value="0" />
                </td>
                <td>
                    <img alt="Remove Link" title="Remove Link" src="<%=Url.Content("~/Content/Images/Delete.png")%>" onclick="markForRemoval(<%=item.Id%>);" class="gridicon" />
                </td>
            </tr>
        <% } %>
    </table>

    <input type="submit" value="Save" class="ui-button ui-widget ui-state-default ui-corner-all" />
    <input type="button" value="Cancel" onclick="cancelRemoval();" class="ui-button ui-widget ui-state-default ui-corner-all" />
<% } %>
</div>

<script type="text/javascript">
    $("#Wait").hide();
</script>