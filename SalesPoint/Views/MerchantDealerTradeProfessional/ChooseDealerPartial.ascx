﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<Masonite.MTier.SalesPoint.Merchant>>" %>
    <p><span class="recordcount" style="float: none;">Viewing the top <%= ViewData.Model.Count<Masonite.MTier.SalesPoint.Merchant>().ToString()%> result(s).</span></p>

    <% foreach (Masonite.MTier.SalesPoint.Merchant item in Model) { %>
        <span style="margin-right: 20px;"><strong><%= item.Name %></strong> [<%= item.City %>, <%= item.StateName %>] | <a href="javascript:void(0);" onclick="addItem(<%= item.Id %>, this);">Add</a></span><div class="break"></div>
    <% ; } %>