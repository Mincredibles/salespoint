﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<Masonite.MTier.SalesPoint.MerchantDealerTradeProfessional>>" %>
    
<script type="text/javascript">
    var lastTableRow;
    var lastSpan;
    var merchantId = '<%= ViewData["MerchantId"] %>';
    function removeItem(id, tableRow) {
        if (!window.confirm('This action will permanently disassociate the selected dealer from the current trade professional.  Continue?')) { return false; }
        lastTableRow = tableRow;

        $.ajax({
            type: "DELETE",
            url: '<%= VirtualPathUtility.ToAbsolute("~/MerchantDealerTradeProfessional/Delete") %>/' + id
        }).done(function (data) {
            lastTableRow.parentNode.removeChild(lastTableRow);
        });
    }
    function addItem(dealerId, span) {
        lastSpan = span;

        $('#ImageWaitChooseDealerPartial').show();

        $.ajax({
            type: "POST",
            url: '<%= VirtualPathUtility.ToAbsolute("~/MerchantDealerTradeProfessional/EditDealerPartial") %>?dealerId=' + dealerId + '&tradeProfessionalId=' + merchantId
        }).done(function (data) {
            isDirty = true;

            if (lastSpan) {
                lastSpan.parentNode.removeChild(lastSpan);
            }

            $('#ImageWaitChooseDealerPartial').hide();
        });
    }
    setMerchantName('<%= ViewData["MerchantName"] %>');
</script>

<div id="ContentElementDealerTradeProfessional">
    <div style="margin-bottom: 5px;">
        <input type="button" value="Add Dealer" onclick="" class="ui-button ui-widget ui-state-default ui-corner-all" />
        <span class="recordcount">Viewing the top <%= ViewData.Model.Count<Masonite.MTier.SalesPoint.MerchantDealerTradeProfessional>().ToString()%> result(s).</span>
    </div>
        
    <% using (Html.BeginForm("IndexEdit", "MerchantDealerTradeProfessional", new { merchantId = ViewData["id"] })) { %>
        <%= Html.ValidationSummary(true) %>
        <table style="width:100%; margin-bottom:15px">
            <tr>
                <th>Dealer</th>
                <th></th>
            </tr>
                
        <% foreach (Masonite.MTier.SalesPoint.MerchantDealerTradeProfessional item in Model) { %>
            <tr onclick="selectTableRow(this);">
                <td><%: item.MerchantDealerName %><input type="hidden" value="<%=item.Id %>" name="MerchantDealerTradeProfessionalId" /></td>
                <td class="cellalignmiddle"><img alt="Remove" title="Remove" src="<%=Url.Content("~/Content/Images/Delete.png")%>" onclick="removeItem(<%= item.Id %>, this.parentNode.parentNode)" class="gridicon" /></td>
            </tr>
        <% } %>

        </table>

        <p>
            <input type="submit" value="Save" class="ui-button ui-widget ui-state-default ui-corner-all" />
        </p>
    <% } %>
</div>