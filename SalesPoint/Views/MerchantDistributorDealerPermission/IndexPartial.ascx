﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<Masonite.MTier.SalesPoint.MerchantDistributorDealerPermission>>" %>

<% foreach (var item in Model) { %>
    <div class="padding-five">
        <div class="float-left Text100"><%: item.PermissionName %></div>
        <input type="checkbox" name='MaxPermission-<%= ViewData["WorkgroupId"] %>' value="<%= item.PermissionId %>" <% if (item.WorkgroupId != 0) { %> checked='checked' <% } %> />
    </div>
<% } %>
