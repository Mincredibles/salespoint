﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">
        var defSearchText = "Location search...";
    </script>

    <div id="TwoColumnWrapper">
        <% if(User.IsInRole("SalesPoint Administrators")) { %>
        <div id="column-left" class="float-left"><% Html.RenderAction("AdministrationLeft", "Navigation", new { selectedMenuItem = SalesPoint.Core.SelectedNavigationAdministrationLeft.None }); %></div>
        <% } %>

        <div id="column-right">
            <fieldset style="width:250px;display:inline;">
                <legend>Location Administration</legend>
                <p><input type="text" value="Location search..." onfocus="if(this.value == defSearchText){this.value = '';}" onblur="if (this.value.length == 0){this.value = 'Location search...';}" onkeydown="if(trapEnterPress(event)){window.location = '<%= Url.Action("Index", "Merchant")%>?searchName=' + escape(this.value);}" class="Text200" /></p>
                <p><%= Html.ActionLink("Locations", "Index", "Merchant") %></p>
                <p><%= Html.ActionLink("Create New Location", "Edit", "Merchant") %></p>
            </fieldset>
        </div>
    </div>
</asp:Content>