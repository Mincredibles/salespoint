﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Masonite.MTier.SalesPoint.GeographicalDistrict>" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div id="TwoColumnWrapper">
        <% if(User.IsInRole("SalesPoint Administrators")) { %>
        <div id="column-left" class="float-left"><% Html.RenderAction("AdministrationLeft", "Navigation", new { selectedMenuItem = SalesPoint.Core.SelectedNavigationAdministrationLeft.Districts }); %></div>
        <% } %>

        <div id="column-right">
            <h2>Edit District<%: ViewData["NewRecordText"] %></h2><%= Html.ActionLink("Back to List", "Index") %>
            <div class="break"></div>

            <% using (Html.BeginForm()) {%>
                <%= Html.ValidationSummary(true) %>
        
                <div class="ui-widget-header padding-five">Details</div>
                <div class="editor-label-left extra-top-buffer">Name<span class="required"> *</span></div>
                <div class="editor-field">
                    <%= Html.TextBoxFor(model => model.Name) %>
                    <%= Html.ValidationMessageFor(model => model.Name) %>
                </div>
        
                <div class="editor-label-left">Number<span class="required"> *</span></div>
                <div class="editor-field">
                    <%= Html.TextBox("Number", Model.Number == 0 ? "" : Model.Number.ToString(System.Globalization.CultureInfo.CurrentUICulture), new { @class = "Text75" })%>
                    <%= Html.ValidationMessageFor(model => model.Number) %>
                </div>
        
                <div class="editor-label-left">Extended Description</div>
                <div class="editor-field">
                    <%= Html.TextBoxFor(model => model.Description, new { @class = "Text300" })%>
                    <%= Html.ValidationMessageFor(model => model.Description) %>
                </div>
        
                <div class="editor-label-left">Type<span class="required"> *</span></div>
                <div class="editor-field">
                    <%= Html.DropDownListFor(model => model.GeographicalDistrictTypeId, new SelectList(Masonite.MTier.SalesPoint.ObjectFactory.RetrieveMerchantChains(), "Id", "Name", Model.GeographicalDistrictTypeId), " ")%>
                    <%= Html.ValidationMessageFor(model => model.GeographicalDistrictTypeId)%>
                </div>
        
                <div class="editor-label-left">Active?</div>
                <div class="editor-field">
                    <%= Html.CheckBoxFor(model => model.IsActive) %>
                    <%= Html.ValidationMessageFor(model => model.IsActive)%>
                </div>
        
                <p class="required">* Required field</p>
                <p>
                    <input type="button" value="Cancel" id="CancelButton" onclick="window.location.href = '<%= Url.Action("Index") %>';" class="ui-button ui-widget ui-state-default ui-corner-all" />
                    <input type="submit" value="Save" class="ui-button ui-widget ui-state-default ui-corner-all" />
                </p>

            <% } %>
        </div>
    </div>
</asp:Content>