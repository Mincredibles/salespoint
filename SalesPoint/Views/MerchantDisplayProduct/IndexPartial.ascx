﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<Masonite.MTier.SalesPoint.DisplayProduct>>" %>
    <script type="text/javascript">
        var lastDisplayProductId;
        function itemChecked(checkbox, merchantId, displayProductId) {
            var action;
            var verb;

            if (checkbox.checked == true) {
                verb = "POST";
                action = '<%= VirtualPathUtility.ToAbsolute("~/MerchantDisplayProduct/Edit") %>?merchantId=' + merchantId + '&displayProductId=' + displayProductId;
            }
            else {
                verb = "DELETE";
                action = '<%= VirtualPathUtility.ToAbsolute("~/MerchantDisplayProduct/Delete") %>?merchantId=' + merchantId + '&displayProductId=' + displayProductId;
            }

            $('#ImageWait' + displayProductId).show();

            $.ajax({
                type: verb,
                url: action
            }).done(function () {
                $('#ImageWait' + displayProductId).hide();
            });
        }
        setMerchantName('<%= ViewData["MerchantName"] %>');
    </script>
    
    <div class="recordcount">Viewing the top <%= ViewData.Model.Count<Masonite.MTier.SalesPoint.DisplayProduct>().ToString()%> result(s).</div>
    
    <table style="width:100%;">
        <thead class="ui-widget-header">
            <tr>
                <th colspan="2">Display Products</th>
            </tr>
        </thead>

    <% List<Masonite.MTier.SalesPoint.DisplayProduct> selectedItems = Masonite.MTier.SalesPoint.ObjectFactory.RetrieveDisplayProducts(Masonite.MTier.Utility.ConversionHelper.ToInt32(ViewData["MerchantId"].ToString())); %>
    <% foreach (var item in Model) { %>
        <tr onclick="selectTableRow(this);">
            <td><input type="checkbox" onclick="itemChecked(this,<%= ViewData["MerchantId"] %>,<%= item.Id %>);" <%if (selectedItems.Exists(delegate(Masonite.MTier.SalesPoint.DisplayProduct product) { return product.Id == item.Id; })) { %>checked="checked"<% } %> /></td>
            <td style="width:99%;"><%: item.Name %> <span id="ImageWait<%=item.Id %>" style="display:none;"><img src="<%=Url.Content("~/Content/Images/Loading.gif")%>" alt="Saving..." title="Saving..." /></span></td>
        </tr>
    <% } %>

    </table>