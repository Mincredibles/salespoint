﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<Masonite.MTier.SalesPoint.MerchantDistributorDealerUsersPermissions>>" %>

<% foreach (var item in Model) { %>
    <div>
        <div class="float-left Text100"><%: item.PermissionName %></div>
        <input type="checkbox" name='MaxPermission-<%= ViewData["UserId"] %>' value="<%= item.PermissionId %>" <% if (item.UserId != 0) { %> checked='checked' <% } %> />
    </div>
<% } %>
