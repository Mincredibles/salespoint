﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<Masonite.MTier.SalesPoint.CorrespondenceApproval>>" %>

<table style="width:100%;">
    <thead class="ui-widget-header">
        <tr>
            <th>Approval</th>
            <th>Date</th>
        </tr>
    </thead>

    <tbody>
    <% foreach (Masonite.MTier.SalesPoint.CorrespondenceApproval item in Model) { %>
        <tr onclick="selectTableRow(this);">
            <td><%: item.CreatedBy %></td>
            <td><%: String.Format(System.Globalization.CultureInfo.CurrentUICulture, "{0:g}", item.DateCreated) %></td>
        </tr>
    <% } %>
    </tbody>
</table>