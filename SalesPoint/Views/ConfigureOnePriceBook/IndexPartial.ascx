﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<Masonite.MTier.SalesPoint.ConfigureOnePriceBook>>" %>

<select name="ConfigureOnePriceBookId" id="ConfigureOnePriceBookId">
    <option value=""></option>
<% foreach (var item in Model) { %>
    <option <%if (ViewData["ConfigureOnePriceBookId"].ToString() == item.Id.ToString()){%>selected="selected"<%}%> value="<%= item.Id %>"><%: item.Name %></option>
<% } %>
</select>