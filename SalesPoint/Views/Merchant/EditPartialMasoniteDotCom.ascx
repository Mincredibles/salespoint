﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Masonite.MTier.SalesPoint.Merchant>" %>
    <script type="text/javascript" src="<%=Url.Content("~/Scripts/MerchantEditPartial.min.js")%>"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=<%= System.Configuration.ConfigurationManager.AppSettings["GoogleMapsApiKey"] %>&sensor=false" type="text/javascript"></script>
    <script type="text/javascript">
        setMerchantName('<%= ViewData["MerchantName"] %>');
        var pathUtilityGetGeoLocation = '<%= VirtualPathUtility.ToAbsolute("~/Utility/GetGeoLocation") %>';
        $(function () {
            $("#dialog-modal").dialog({
                height: 450,
                width: 650,
                modal: true,
                autoOpen: false,
                resizable: false,
                close: dialogClose
            });
        });
        function dialogClose() {
            //
        }
        function mapDialog(latitude, longitude) {
            var googleLatLong = new google.maps.LatLng(latitude, longitude);
            var mapOptions = {
                center: googleLatLong,
                zoom: 15,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            var map = new google.maps.Map($("#map")[0], mapOptions);
            var marker = new google.maps.Marker({
                position: googleLatLong,
                map: map,
                animation: google.maps.Animation.DROP
            });
            $('#dialog-modal').dialog('open');

            // Force resize and re-center for strange anomaly, presumably related to jQuery dialog.  Without these, if
            // you close and re-open the dialog, the map reappears shifted most of the way off the container.
            google.maps.event.trigger(map, 'resize');
            map.panTo(googleLatLong);

            $('#searchName').select();
        }
    </script>
    
    <% using (Html.BeginForm("EditMasoniteDotCom", "Merchant", new { merchantId = ViewData["MerchantId"] })) { %>
        <%= Html.ValidationSummary(true) %>
        
        <table class="merchant-edit">
            <tr>
                <td valign="top">
                    <div style="width:400px;">
                        <div class="full-width ui-widget-header padding-five">Product Lines</div>
                        <% if (!Model.IsActiveInMax) { %>
                        <table class="merchant-options">
                            <tr>
                                <td>Entry Doors</td>
                                <td><input type="checkbox" id="IsDistributorSupplyingProductEntry" name="IsDistributorSupplyingProductEntry" <% if((int)ViewData["IsDistributorSupplyingProductEntry"] == 1) { %>checked="checked"<% } %> /></td>
                            </tr>
                            <tr>
                                <td>Interior Doors</td>
                                <td><input type="checkbox" id="IsDistributorSupplyingProductInterior" name="IsDistributorSupplyingProductInterior" <% if((int)ViewData["IsDistributorSupplyingProductInterior"] == 1) { %>checked="checked"<% } %> /></td>
                            </tr>
                            <tr>
                                <td>Patio Doors</td>
                                <td><input type="checkbox" id="IsDistributorSupplyingProductPatio" name="IsDistributorSupplyingProductPatio" <% if((int)ViewData["IsDistributorSupplyingProductPatio"] == 1) { %>checked="checked"<% } %> /></td>
                            </tr>
                        </table>
                        <% } else { %>
                        <table class="merchant-options">
                            <tr>
                                <td>Product lines for this location are managed by the <a href="mailto:max@masonite.com">Max Team</a>.</td>
                            </tr>
                        </table>
                        <% } %>
                        
                        <div class="full-width ui-widget-header padding-five">Where to Buy <div id="WebsiteStatus" style="float:right;">&nbsp;</div></div>
                        <div class="editor-label-left-sm extra-top-buffer">On Locator?</div>
                        <div class="editor-field">
                            <%= Html.CheckBoxFor(model => model.IsSearchableOnMasoniteWebsite, new { @onclick = "validateSearchableOnWebsite()" })%>
                        </div>

                        <div class="editor-label-left-sm">Premier?</div>
                        <div class="editor-field">
                            <% if (!Page.User.IsInRole("SalesPoint Administrators") && !Page.User.IsInRole("Max Team")) { %>
                                <% if (Model.IsPremierStatus) { %>Yes<% } else { %>No<%}%>
                                <%= Html.CheckBoxFor(model => model.IsPremierStatus, new { @style = "visibility:hidden;" })%>
                            <% } else { %>
                                <%= Html.CheckBoxFor(model => model.IsPremierStatus)%>
                            <% } %>
                        </div>
                        
                        <div class="editor-label-left-sm">Latitude</div>
                        <div class="editor-field">
                            <%= Html.TextBox("Latitude", Model.Latitude == 0 ? "" : Model.Latitude.ToString(System.Globalization.CultureInfo.CurrentUICulture), new { @onblur = "updateWebsiteStatus()", @class = "Text75" })%> 
                            <%= Html.ValidationMessageFor(model => model.Latitude)%>
                        </div>
                        
                        <div class="editor-label-left-sm">Longitude</div>
                        <div class="editor-field">
                            <%= Html.TextBox("Longitude", Model.Longitude == 0 ? "" : Model.Longitude.ToString(System.Globalization.CultureInfo.CurrentUICulture), new { @onblur = "updateWebsiteStatus()", @class = "Text75" })%>
                             <br />
                             <a id="CoordinatesLink" href="javascript:void(0);" onclick="geoCode('<%= Model.Address.Replace("'", "\\'") %>','<%= Model.City.Replace("'", "\\'") %>','<%= Model.StateName.Replace("'", "\\'") %>','<%= Model.PostalCode %>')">Get Lat. and Long.</a><span id="linkSep">&nbsp;|&nbsp;</span><a id="googleLink" href="javascript:void(0);" onclick="mapDialog($('#Latitude').val(),$('#Longitude').val());">Show on map</a>
                            <%= Html.ValidationMessageFor(model => model.Longitude)%>
                        </div>
                    </div>
                </td>
                <td valign="top">
                    <div style="width:280px;padding-left:20px;">
                        <div class="full-width ui-widget-header padding-five">Options</div>
                    </div>
                    <table id="dealeroptions" class="merchant-options" style="margin-left:20px;" cellpadding="1" border="0">
                        <tr class="moreoptions">
                            <td>Carrier of Specialty Glass?</td>
                            <td><%= Html.CheckBoxFor(model => model.IsCarrierOfSpecialtyGlass)%></td>
                        </tr>
                        <tr class="homedepotglass">
                            <td>Carrier of Home Depot Glass?</td>
                            <td><%= Html.CheckBoxFor(model => model.IsCarrierOfHomeDepotGlass)%></td>
                        </tr>
                        <tr class="lowesglass">
                            <td>Carrier of Lowes Glass?</td>
                            <td><%= Html.CheckBoxFor(model => model.IsCarrierOfLowesGlass)%></td>
                        </tr>
                        <tr class="moreoptions">
                            <td>Retail Inquiries Desired?</td>
                            <td><%= Html.CheckBoxFor(model => model.IsRetailInquiriesDesired)%></td>
                        </tr>
                        <tr class="moreoptions">
                            <td>Professional Trade Inquiries Desired?</td>
                            <td><%= Html.CheckBoxFor(model => model.IsProfessionalTradeInquiriesDesired)%></td>
                        </tr>
                        <tr class="moreoptions">
                            <td>Dealer Inquiries Desired?</td>
                            <td><%= Html.CheckBoxFor(model => model.IsDealerInquiriesDesired)%></td>
                        </tr>
                        <tr class="moreoptions">
                            <td>Installation Services Offered?</td>
                            <td><%= Html.CheckBoxFor(model => model.IsInstallationServicesOffered)%></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>

        <p class="required">* Required field</p>
        <p>
            <input type="submit" value="Save" class="ui-button ui-widget ui-state-default ui-corner-all" />
        </p>
    <% } %>

    <div id="dialog-modal" title="Google Maps">
        <div id="map" class="full-width full-height"></div>
    </div>
    
    <script type="text/javascript">
        updateWebsiteStatus();
        merchantChainChanged(<%= Model.MerchantChainId %>);
    </script>