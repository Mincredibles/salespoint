﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Masonite.MTier.SalesPoint.Merchant>" %>
    <script type="text/javascript" src="<%=Url.Content("~/Scripts/MerchantEditPartial.min.js")%>"></script>
    <script type="text/javascript">
        <% if(ViewData["DeniedAccessToMerchant"] != null && string.Compare("1", ViewData["DeniedAccessToMerchant"].ToString(), false, System.Globalization.CultureInfo.CurrentUICulture) == 0) { %>
        window.alert('The requested location cannot be shown.  The location may not exist, or your user account may not have the necessary permissions to view it.');
        window.location.href = '<%= VirtualPathUtility.ToAbsolute("~/Merchant") %>';
        <% } %>
        var pathUtilityPostalCodeLookup = '<%= VirtualPathUtility.ToAbsolute("~/Utility/PostalCodeLookup") %>';
        var pathUtilityGetGeoLocation = '<%= VirtualPathUtility.ToAbsolute("~/Utility/GetGeoLocation") %>';
        var pathUtilityGenerateRandomPassword = '<%= VirtualPathUtility.ToAbsolute("~/Utility/GenerateRandomPassword") %>';
        var pathMerchantMerchantNameDuplicates = '<%= VirtualPathUtility.ToAbsolute("~/Merchant/MerchantNameDuplicates") %>';
        var pathMerchantEdit = '<%= VirtualPathUtility.ToAbsolute("~/Merchant/Edit/") %>';
        var merchantInitialName = '<%= ("" + Model.Name).Replace("'", "\\'") %>';
        var tagIdToDelete;
        
        function addTag(text) {
            if (text.length == 0) { return false; }

            $.ajax({
                type: "POST",
                url: '<%= VirtualPathUtility.ToAbsolute("~/MerchantTag/Edit") %>/0?merchantId=<%= Model.Id %>&text=' + escape(text)
            }).done(function (data) {
                $('#Tags').load('<%= VirtualPathUtility.ToAbsolute("~/MerchantTag/IndexPartial") %>/0?merchantId=<%= Model.Id %>');
                document.getElementById('TagText').value = '';
                $('#TagText').select();
            });

            return false;
        }
        function deleteTag(id) {
            tagIdToDelete = id;
            deleteItem(id, '<%= VirtualPathUtility.ToAbsolute("~/MerchantTag") %>')
        }
        function deleteCompleted() {
            elementToRemove = document.getElementById('Tag' + tagIdToDelete);
            if (elementToRemove) {
                elementToRemove.parentNode.removeChild(elementToRemove);
            }
        }
    </script>
     <% using (Html.BeginForm("Edit", "Merchant", new { id = Model.Id }, FormMethod.Post, new { enctype = "multipart/form-data" })) { %>
        <%= Html.ValidationSummary(true) %>
        
        <table class="merchant-edit alignment-fix">
            <tr>
                <td valign="top">
                    <div style="float:left;width:435px;">
                        <div class="full-width ui-widget-header padding-five">Details</div>
                        <div class="editor-label-left extra-top-buffer">Type<span class="required"> *</span></div>
                        <div class="editor-field">
                            <% if (Model.IsDirectCustomer && !Page.User.IsInRole("SalesPoint Administrators")) { %>
                                <%= Html.DropDownListFor(model => model.CategoryId, new SelectList(Masonite.MTier.SalesPoint.ObjectFactory.RetrieveMerchantCategories(), "Id", "Name", Model.CategoryId), new { @disabled = "disabled" })%>
                                <%= Html.HiddenFor(model => model.CategoryId) %>
                            <% } %>
                            <% else { %>
                                <%= Html.DropDownListFor(model => model.CategoryId, new SelectList(Masonite.MTier.SalesPoint.ObjectFactory.RetrieveMerchantCategories(), "Id", "Name", Model.CategoryId), " ", new { @onchange = "merchantTypeChanged(this.options[this.selectedIndex].value);" })%>
                            <% } %>
                        </div>
                        <div><%= Html.ValidationMessageFor(model => model.CategoryId)%></div>
                        
                        <div id="ChainLabel" style="width:385px;display:none;">
                            <div class="editor-label-left">Chain<span class="required"> *</span></div>
                            <div class="editor-field">
                                <%= Html.DropDownListFor(model => model.MerchantChainId, new SelectList(Masonite.MTier.SalesPoint.ObjectFactory.RetrieveMerchantChains(), "Id", "Name", Model.MerchantChainId))%>
                            </div>
                            <div><%= Html.ValidationMessageFor(model => model.MerchantChainId)%></div>
                        </div>
                        
                        <div id="ToBeDeleted" style="display:none;">
                        <div class="editor-label-left">Number<span class="required"> *</span></div>
                        <div class="editor-field">
                            <% if (Model.IsDirectCustomer) { %>
                                <%= Html.TextBoxFor(model => model.Number, new { @class = "Text75", @disabled = "disabled" })%>
                                <%= Html.HiddenFor(model => model.Number)%>
                            <% } %>
                            <% else { %>
                                <%= Html.TextBoxFor(model => model.Number, new { @class = "Text75", @onblur = "defaultNumberTextbox(this);" })%>
                            <% } %>
                        </div>
                        <div><%= Html.ValidationMessageFor(model => model.Number)%></div>
                        </div>
                                                                        
                        <div class="editor-label-left">Name<span class="required"> *</span></div>
                        <div class="editor-field">
                            <% if (Model.IsDirectCustomer && !Page.User.IsInRole("SalesPoint Administrators")) { %>
                                <%= Html.TextBoxFor(model => model.Name, new { @class = "Text250", @disabled = "disabled" })%>
                                <%= Html.HiddenFor(model => model.Name)%>
                            <% } %>
                            <% else { %>
                                <%= Html.TextBoxFor(model => model.Name, new { @class = "Text250", @onkeyup = "setMerchantName(this.value);", @onblur = "checkNameForDuplicates(this.value);" })%> <img id="ImageWaitDupeCheck" alt="Checking logon name..." title="Checking name..." src="<%=Url.Content("~/Content/Images/Loading.gif")%>" style="display: none;" /> <span id="Dupe"></span>
                            <% } %>
                        </div>
                        <div><%= Html.ValidationMessageFor(model => model.Name)%></div>
                        
                        <div class="editor-label-left">Active?</div>
                        <div class="editor-field">
                            <% if (Model.IsActiveInMax) { %>
                                <% if (Model.IsActive) { %>Yes<% } else { %>No<%}%>
                                <%= Html.CheckBoxFor(model => model.IsActive, new { @style = "visibility:hidden;" })%>
                            <% } else { %>
                                <%= Html.CheckBoxFor(model => model.IsActive)%>
                            <% } %>
                        </div>
                        
                        <div class="editor-label-left">Direct Customer?</div>
                        <div class="editor-field extra-top-buffer">
                            <% if (Model.IsDirectCustomer) {%> Yes (some fields read-only) <%} else {%> No <%}%>
                        </div>

                        <div class="editor-label-left">ConfigureOne Parent?</div>
                        <div class="editor-field extra-top-buffer"> 
                            <% if (Model.IsConfigureOneParentMerchant) { %>Yes<% } else { %>No<%}%>
                        </div>

                        <div class="editor-label-left">Excluded From Max?</div>
                        <div class="editor-field">
                            <% if (!Page.User.IsInRole("SalesPoint Administrators")) { %>
                                <% if (Model.IsExcludedFromMax) { %>Yes<% } else { %>No<%}%>
                                <%= Html.CheckBoxFor(model => model.IsExcludedFromMax, new { @style = "visibility:hidden;" })%>
                            <% } else { %>
                                <%= Html.CheckBoxFor(model => model.IsExcludedFromMax)%>
                            <% } %>
                        </div>

                        <div class="editor-label-left">Show in Max Reports?</div>
                        <div class="editor-field">
                            <% if (!Page.User.IsInRole("SalesPoint Administrators") && !Page.User.IsInRole("Max Team")) { %>
                                <% if (Model.IsShownInMaxMetricsReports) { %>Yes<% } else { %>No<%}%>
                                <%= Html.CheckBoxFor(model => model.IsShownInMaxMetricsReports, new { @style = "visibility:hidden;" })%>
                            <% } else { %>
                                <%= Html.CheckBoxFor(model => model.IsShownInMaxMetricsReports)%>
                            <% } %>
                        </div>
                        
                        <div>&nbsp;</div>
                                                        
                        <div class="editor-label-left">Region</div>
                        <div class="editor-field">
                            <%= Html.DropDownListFor(model => model.RegionId, new SelectList(Masonite.MTier.SalesPoint.ObjectFactory.RetrieveGeographicalRegions(), "Id", "Name", Model.RegionId), " ")%>
                            <%= Html.ValidationMessageFor(model => model.RegionId)%>
                        </div>
                                                                    
                        <div class="editor-label-left">Salesperson</div>
                        <div class="editor-field">
                            <%= Html.DropDownListFor(model => model.MasterPackSalesPersonNumber, new SelectList(Masonite.MTier.SalesPoint.ObjectFactory.RetrieveTerritorySalesManagers(), "MasterPackSalesPersonNumber", "MasterPackSalesPersonNumberDescription", Model.MasterPackSalesPersonNumber), " ")%>
                            <%= Html.ValidationMessageFor(model => model.MasterPackSalesPersonNumber)%>
                        </div>
                        
                        <div class="editor-label-left">UK Customer Acct#</div>
                        <div class="editor-field">
                            <%= Html.TextBoxFor(model => model.MasoniteUnitedKingdomCustomerAccountNumber)%>
                            <%= Html.ValidationMessageFor(model => model.MasoniteUnitedKingdomCustomerAccountNumber)%>
                        </div>

                        <div class="editor-label-left">France Customer Acct#</div>
                        <div class="editor-field">
                            <%= Html.TextBoxFor(model => model.MasoniteFranceCustomerAccountNumber)%>
                            <%= Html.ValidationMessageFor(model => model.MasoniteFranceCustomerAccountNumber)%>
                        </div>
                        
                        <div class="editor-label-left">MasterPack ID</div>
                        <div class="editor-field extra-top-buffer">
                            <%= string.IsNullOrEmpty(Model.MasterPackId) ? "[None]" : Model.MasterPackId%>
                        </div>

                        <div class="editor-label-left">MasterPack A/R#</div>
                        <div class="editor-field">
                            <% if (!Page.User.IsInRole("Accounting Team") && !Page.User.IsInRole("SalesPoint Administrators")) { %>
                                <%= Html.TextBoxFor(model => model.MasterPackAccountsReceiveableNumber, new { @disabled = "disabled" })%>
                                <%= Html.HiddenFor(model => model.MasterPackAccountsReceiveableNumber)%>
                            <% } %>
                            <% else { %>
                                <%= Html.TextBoxFor(model => model.MasterPackAccountsReceiveableNumber)%>
                            <% } %>
                        </div>
                        <div><%= Html.ValidationMessageFor(model => model.MasterPackAccountsReceiveableNumber)%></div>

                        <div class="editor-label-left">MasterPack A/P#</div>
                        <div class="editor-field">
                            <% if (!Page.User.IsInRole("Accounting Team") && !Page.User.IsInRole("SalesPoint Administrators")) { %>
                                <%= Html.TextBoxFor(model => model.MasterPackAccountsPayableNumber, new { @disabled = "disabled" })%>
                                <%= Html.HiddenFor(model => model.MasterPackAccountsPayableNumber)%>
                            <% } %>
                            <% else { %>
                                <%= Html.TextBoxFor(model => model.MasterPackAccountsPayableNumber)%>
                            <% } %>
                        </div>
                        <div><%= Html.ValidationMessageFor(model => model.MasterPackAccountsPayableNumber)%></div>
                             
                        <div class="full-width ui-widget-header padding-five">Contact</div>
                        <div class="editor-label-left extra-top-buffer">Phone Number</div>
                        <div class="editor-field">
                            <%= Html.TextBoxFor(model => model.PhoneNumber)%>
                            <%= Html.ValidationMessageFor(model => model.PhoneNumber)%>
                        </div>
                        
                        <div class="editor-label-left">Fax Number</div>
                        <div class="editor-field">
                            <%= Html.TextBoxFor(model => model.FaxNumber)%>
                            <%= Html.ValidationMessageFor(model => model.FaxNumber)%>
                        </div>
                        
                        <div class="editor-label-left">Website Url</div>
                        <div class="editor-field">
                            <%= Html.TextBoxFor(model => model.WebsiteUrl, new { @class = "Text200" })%> <a href="javascript:void(0);" onclick="window.open($('#WebsiteUrl').val());">Go</a>
                            <%= Html.ValidationMessageFor(model => model.WebsiteUrl)%>
                        </div>
                    </div>
                </td>
                <td valign="top">
                    <div style="float:left;width:365px;padding-left:20px;">
                        <div class="full-width ui-widget-header padding-five">Physical Address</div>
                        <div class="editor-label-left extra-top-buffer">Address<span class="required"> *</span></div>
                        <div class="editor-field">
                            <% if (Model.IsDirectCustomer && !Page.User.IsInRole("SalesPoint Administrators")) { %>
                                <%= Html.TextBoxFor(model => model.Address, new { @class = "Text200", @disabled = "disabled" })%>
                                <%= Html.HiddenFor(model => model.Address)%>
                            <% } %>
                            <% else { %>
                                <%= Html.TextBoxFor(model => model.Address, new { @class = "Text200" })%>
                            <% } %>
                        </div>
                        <div><%= Html.ValidationMessageFor(model => model.Address)%></div>
                    
                        <div class="editor-label-left"></div>
                        <div class="editor-field">
                            <% if (Model.IsDirectCustomer && !Page.User.IsInRole("SalesPoint Administrators")) { %>
                                <%= Html.TextBoxFor(model => model.Address2, new { @class = "Text200", @disabled = "disabled" })%>
                                <%= Html.HiddenFor(model => model.Address2)%>
                            <% } %>
                            <% else { %>
                                <%= Html.TextBoxFor(model => model.Address2, new { @class = "Text200" })%>
                                <%= Html.ValidationMessageFor(model => model.Address2)%>
                            <% } %>
                        </div>
                    
                        <div class="editor-label-left">Postal Code<span class="required"> *</span></div>
                        <div class="editor-field">
                            <% if (Model.IsDirectCustomer && !Page.User.IsInRole("SalesPoint Administrators")) { %>
                                <%= Html.TextBoxFor(model => model.PostalCode, new { @class = "Text75", @disabled = "disabled" })%>
                                <%= Html.HiddenFor(model => model.PostalCode)%>
                            <% } %>
                            <% else { %>
                                <%= Html.TextBoxFor(model => model.PostalCode, new { @class = "Text75", @onblur = "postalCodeLookup(this.value, null);" })%> <img id="ImageWaitPostalCodeLookup" alt="Checking postal code..." title="Checking postal code..." src="<%=Url.Content("~/Content/Images/Loading.gif")%>" style="display: none;" /> <span id="Zip"></span>
                            <% } %>
                        </div>
                        <div><%= Html.ValidationMessageFor(model => model.PostalCode)%></div>
                    
                        <div class="editor-label-left">City<span class="required"> *</span></div>
                        <div class="editor-field">
                            <% if (Model.IsDirectCustomer && !Page.User.IsInRole("SalesPoint Administrators")) { %>
                                <%= Html.TextBoxFor(model => model.City, new { @disabled = "disabled" })%>
                                <%= Html.HiddenFor(model => model.City)%>
                            <% } %>
                            <% else { %>
                                <%= Html.TextBoxFor(model => model.City)%>
                            <% } %>
                        </div>
                        <div><%= Html.ValidationMessageFor(model => model.City)%></div>
                    
                        <div class="editor-label-left">State<span class="required"> *</span></div>
                        <div class="editor-field">
                            <% if (Model.IsDirectCustomer && !Page.User.IsInRole("SalesPoint Administrators")) { %>
                                <% Html.RenderAction("IndexPartial", "GeographicalState", new { controlName = "StateId", includeBlankItem = true, selectedItemId = Model.StateId, disableControl = true }); %>
                                <%= Html.HiddenFor(model => model.StateId)%>
                            <% } %>
                            <% else { %>
                                <% Html.RenderAction("IndexPartial", "GeographicalState", new { controlName = "StateId", includeBlankItem = true, selectedItemId = Model.StateId, disableControl = false }); %>
                            <% } %>
                        </div>
                        <div><%= Html.ValidationMessageFor(model => model.StateId)%></div>
                                        
                        <div class="editor-label-left">Country</div>
                        <div class="editor-field">
                            <% if (Model.IsDirectCustomer && !Page.User.IsInRole("SalesPoint Administrators")) { %>
                                <%= Html.TextBoxFor(model => model.Country, new { @disabled = "disabled" })%>
                                <%= Html.HiddenFor(model => model.Country)%>
                            <% } %>
                            <% else { %>
                                <%= Html.TextBoxFor(model => model.Country)%>
                                <%= Html.ValidationMessageFor(model => model.Country)%>
                            <% } %>
                        </div>
                                        
                        <div class="full-width ui-widget-header padding-five">Mailing Address (if different)</div>
                        <div class="editor-label-left extra-top-buffer">Address</div>
                        <div class="editor-field">
                            <%= Html.TextBoxFor(model => model.MailingAddress, new { @class = "Text200" })%>
                            <%= Html.ValidationMessageFor(model => model.MailingAddress)%>
                        </div>
                    
                        <div class="editor-label-left"></div>
                        <div class="editor-field">
                            <%= Html.TextBoxFor(model => model.MailingAddress2, new { @class = "Text200" })%>
                            <%= Html.ValidationMessageFor(model => model.MailingAddress2)%>
                        </div>
                    
                        <div class="editor-label-left">Postal Code</div>
                        <div class="editor-field">
                            <%= Html.TextBoxFor(model => model.MailingPostalCode, new { @class = "Text75", @onblur = "postalCodeLookup(this.value, 'M');" })%> <img id="ImageWaitPostalCodeLookupMailing" alt="Checking postal code..." title="Checking postal code..." src="<%=Url.Content("~/Content/Images/Loading.gif")%>" style="display: none;" /> <span id="ZipMailing"></span>
                            <%= Html.ValidationMessageFor(model => model.MailingPostalCode)%>
                        </div>
                    
                        <div class="editor-label-left">City</div>
                        <div class="editor-field">
                            <%= Html.TextBoxFor(model => model.MailingCity)%>
                            <%= Html.ValidationMessageFor(model => model.MailingCity)%>
                        </div>
                    
                        <div class="editor-label-left">State</div>
                        <div class="editor-field">
                            <%= Html.TextBoxFor(model => model.MailingState, new { @class = "Text75" })%>
                            <%= Html.ValidationMessageFor(model => model.MailingState)%>
                        </div>
                                        
                        <div class="editor-label-left">Country</div>
                        <div class="editor-field">
                            <%= Html.TextBoxFor(model => model.MailingCountry)%>
                            <%= Html.ValidationMessageFor(model => model.MailingCountry)%>
                        </div>
                    
    <% if (Model.Id > 0) { %>
                        <div class="full-width ui-widget-header padding-five">Tags<div style="float:right;"><a href="javascript:void(0);" onclick="window.alert('A tag is any user-generated word or phrase that helps categorize content on this site.  These tags can be grouped and easily searched upon to help you find Locations at a later time.\n\nFor instance, you might tag several Locations that regularily attend the International Builders Show with the phrase \'IBS\'.  At a later time, you can then easily poll for a list of all Locations that attend the show by searching for the tag \'IBS\'.');">Why?</a></div></div>
                        <div class="editor-field">
                            <input type="text" id="TagText" name="TagText" class="Text200" onkeydown="if(trapEnterPress(event)){document.getElementById('ButtonAddTag').click();return false;}else{return true;}" />&nbsp;<input id="ButtonAddTag" type="button" value="Add Tag" onclick="addTag(document.getElementById('TagText').value);" class="ui-button ui-widget ui-state-default ui-corner-all" />
                        </div>
                        <div id="Tags"><% Html.RenderAction("IndexPartial", "MerchantTag", new { merchantId = Model.Id }); %></div>
    <% ; } %>
                    </div>
                </td>
                <td valign="top">
                    <div style="float:left;width:365px;padding-left:20px;">
                        <div class="full-width ui-widget-header padding-five">Logo</div>
                        <% if (!string.IsNullOrWhiteSpace(Model.Image)) { %>
                            <img style="background-color: #3F3F40;" src="<%: ConfigurationManager.AppSettings["LogoPath"] %><%: Model.Image %>" alt="">
                            <div class="break"></div>
                            <span style="font-weight: bold;"><%: Model.Image %></span>
                        <% } else { %>
                            <span style="font-weight: bold;">No logo uploaded</span>
                        <% } %>
                        <div class="break"></div>
                        <% if (!Model.IsDirectCustomer || Page.User.IsInRole("SalesPoint Administrators")) { %>
                            <div class="editor-label-left extra-top-buffer">Upload Logo</div>
                            <div class="editor-field">
                                <%= Html.TextBoxFor(model => model.Image, new { @class = "Text200", type = "file" })%>
                            </div>
                            <% if (!string.IsNullOrWhiteSpace(Model.Image)) { %>
                                <div class="editor-label-left extra-top-buffer">Delete Logo</div>
                                <div class="editor-field">
                                    <%= Html.CheckBox("DeleteLogo") %>
                                </div>
                            <% } %>
                        <% } %>
                    </div>
                </td>
            </tr>
        </table>
        
        <p class="required">* Required field</p>
        <p>
            <input type="submit" value="Save" id="SaveButton2" class="ui-button ui-widget ui-state-default ui-corner-all" />
        </p>
    <% } %>
    
    <script type="text/javascript">
        setMerchantName('<%: ViewData["MerchantName"] %>');
        updateChain();
        $('#Name').select(); // Pre-select the Name field.
    </script>