﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<Masonite.MTier.SalesPoint.Merchant>>" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">
        var controllerPath = '<%= VirtualPathUtility.ToAbsolute("~/" + ViewContext.RouteData.Values["Controller"]) %>';
        function deleteCompleted() {
            var searchName = $('#searchName').val();
            if (searchName.length > 0) {
                window.location = '<%= VirtualPathUtility.ToAbsolute("~/Merchant") %>?searchName=' + escape(searchName);
            }
            else {
                window.location.reload();
            }
        }
    </script>

    <h2>Locations</h2>
    <input type="button" value="Show Recent" onclick="document.location.href = '<%= Url.Action("Index") %>';" class="ui-button ui-widget ui-state-default ui-corner-all" />
    <input type="button" value="Add New" onclick="document.location.href = '<%= Url.Action("Edit") %>';" class="ui-button ui-widget ui-state-default ui-corner-all" />
    <div class="break"></div>

    <div>
        <% using(Html.BeginForm()) %>
        <% { %>
            <table class="merchant-edit" style="border:1px solid #C0C0C0;width:auto;" cellpadding="3">
                <tr>
                    <td>
                        <table class="merchant-edit" style="width:auto;" cellpadding="3">
                            <tr>
                                <td>MP Number</td>
                                <td><input type="text" name="searchMasterPackId" id="searchMasterPackId" value="<%: ViewData["searchMasterPackId"] %>" /></td>
                            </tr>
                            <tr>
                                <td>Name</td>
                                <td><input type="text" name="searchMerchantName" id="searchMerchantName" value="<%: ViewData["searchMerchantName"] %>" /></td>
                            </tr>
                            <tr>
                                <td>Tag</td>
                                <td><input type="text" name="searchTag" id="searchTag" value="<%: ViewData["searchTag"] %>" /></td>
                            </tr>
                            <tr>
                                <td>City</td>
                                <td><input type="text" name="searchCity" id="searchCity" value="<%: ViewData["searchCity"] %>" /></td>
                            </tr>
                            <tr>
                                <td>State</td>
                                <td>
                                    <% Html.RenderAction("IndexPartial", "GeographicalState", new { controlName = "searchStateId", includeBlankItem = true, selectedItemId = ViewData["searchStateId"], disableControl = false }); %>
                                </td>
                            </tr>
                            <tr>
                                <td>Postal Code</td>
                                <td><input type="text" name="searchPostalCode" id="searchPostalCode" value="<%: ViewData["searchPostalCode"] %>" /></td>
                            </tr>
                        </table>
                    </td>
                    <td>&nbsp;</td>
                    <td style="vertical-align:top;">
                        <table class="merchant-edit" style="width:auto;float:right;vertical-align:top;" cellpadding="3">
                            <tr>
                                <td>Contact Name</td>
                                <td style="white-space:nowrap"><input type="text" name="searchContactFirstName" id="searchContactFirstName" value="<%: ViewData["searchContactFirstName"] %>" /> <input type="text" name="searchContactLastName" id="searchContactLastName" value="<%: ViewData["searchContactLastName"] %>" /></td>
                            </tr>
                            <tr>
                                <td>Contact Email</td>
                                <td><input type="text" name="searchContactEmailAddress" id="searchContactEmailAddress" value="<%: ViewData["searchContactEmailAddress"]%>" /></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <input type="checkbox" name="retrieveOnlyDisabled" id="retrieveOnlyDisabled" <%= ViewData["retrieveOnlyDisabled"]%> /> Search disabled locations <input type="submit" value="Search" style="margin-left:100px" class="ui-button ui-widget ui-state-default ui-corner-all" />
                    </td>
                </tr>
            </table>
            <br />
            <div class="recordcount">Viewing the top <%= ViewData.Model.Count<Masonite.MTier.SalesPoint.Merchant>().ToString()%> result(s).</div>
        <% } %>
    </div>

    <table style="width:100%;">
        <thead class="ui-widget-header">
            <tr>
                <th>Name</th>
                <th></th>
                <th>Phone Number</th>
                <th>Fax Number</th>
                <th>Website</th>
                <th>Category</th>
                <th>Masterpack Number</th>
                <th></th>
            </tr>
        </thead>

    <% foreach (Masonite.MTier.SalesPoint.Merchant item in Model)
       { %>
        <tr onclick="selectTableRow(this);">
            <td>
                <%= Html.ActionLink(item.Name, "Edit", new { id = item.Id })%>
                <br />
                <%: item.Address %>
                <%= string.IsNullOrEmpty(item.Address2) ? "" : "<br />" + Html.Encode(item.Address2) %>
                <%= string.IsNullOrEmpty(item.City) ? "" : "<br />" + Html.Encode(item.City) %><%= string.IsNullOrEmpty(item.StateName) ? "" : ", " + Html.Encode(item.StateName) %><%= string.IsNullOrEmpty(item.PostalCode) ? "" : " " + Html.Encode(item.PostalCode) %>
            </td>
            <td style="white-space:nowrap;">
                <% if(item.IsSearchableOnMasoniteWebsite && item.Latitude != 0 && item.Longitude != 0) { %><img src="<%=Url.Content("~/Content/Images/MasoniteSmall.png")%>" alt="This location appears on masonite.com" title="This location appears on masonite.com" style="margin: -1px 5px -1px -1px;" /><% } %>
                <% if(item.IsActiveInMax) { %><img src="<%=Url.Content("~/Content/Images/MaxSmall.png")%>" alt="This location is active in Max" title="This location is active in Max" style="margin: -1px;" /><% } %>
                <% if(item.IsPremierStatus) { %><img src="<%=Url.Content("~/Content/Images/PremierStatus.png")%>" alt="This is a premier location" title="This is a premier location" style="margin: -1px;" /><% } %>
            </td>
            <td><%: Masonite.MTier.Utility.PhoneNumberHelper.Format(item.PhoneNumber, ".") %></td>
            <td><%: Masonite.MTier.Utility.PhoneNumberHelper.Format(item.FaxNumber, ".") %></td>
            <td><% if(!string.IsNullOrEmpty(item.WebsiteUrl)) { %><a href="<%= item.WebsiteUrl %>" target="_blank"><%= item.WebsiteUrl %></a><% } %></td>
            <td style="white-space:nowrap;"><%: item.CategoryName %></td>
            <td><%: item.MasterPackId %></td>
            <td class="cellalignmiddle"><% if(!item.IsActiveInMax && item.CategoryId != 2 && item.CategoryId != 3) { %><img alt="Delete" title="Delete" src="<%=Url.Content("~/Content/Images/Delete.png")%>" onclick="deleteItem('<%= item.Id %>', controllerPath)" class="gridicon" /><% } %></td>
        </tr>
    <% } %>

    </table>
</asp:Content>