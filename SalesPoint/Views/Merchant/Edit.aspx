﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Masonite.MTier.SalesPoint.Merchant>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">
        var merchantId = '<%= ViewData["MerchantId"] %>';
        var isDirty = false;
        function setMerchantName(name) {
            document.getElementById('MerchantName').innerHTML = name;
        }
    </script>
       
    <h2>Edit Location</h2><%= Html.ActionLink("Back to List", "Index") %>
    <div class="break"></div>

    <span id="MerchantName"></span>
    <div class="break"></div>

    <%= Html.Action("IndexMerchant", "TabStrip", new { merchantId = ViewData["MerchantId"], tab = ViewData["TabIndex"] })%>
</asp:Content>