﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Masonite.MTier.SalesPoint.Configuration>" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">
        function prePublish() {
            $('#PrePublishResult').empty();
            $("#PrePublishResultDiv").show();
            $("#ImageWaitPrePublish").show();

            $.ajax({
                type: "POST",
                url: '<%= VirtualPathUtility.ToAbsolute("~/Configurator/ExecutePriceBookPublishPreStep") %>'
            }).done(function (data) {
                data = JSON.parse(data);
                $.each(data, function (index, val) {
                    $('#PrePublishResult').append('<tr><td>' + val + '</td></tr>');
                });

                $("#ImageWaitPrePublish").hide();
            });
        }
        function postPublish() {
            $('#PostPublishResult').empty();
            $("#PostPublishResultDiv").show();
            $("#ImageWaitPostPublish").show();

            $.ajax({
                type: "POST",
                url: '<%= VirtualPathUtility.ToAbsolute("~/Configurator/ExecutePriceBookPublishPostStep") %>'
            }).done(function (data) {
                data = JSON.parse(data);
                $.each(data, function (index, val) {
                    $('#PostPublishResult').append('<tr><td>' + val + '</td></tr>');
                });

                $("#ImageWaitPostPublish").hide();
            });
        }
    </script>

    <div id="TwoColumnWrapper">
        <% if(User.IsInRole("SalesPoint Administrators")) { %>
        <div id="column-left" class="float-left"><% Html.RenderAction("AdministrationLeft", "Navigation", new { selectedMenuItem = SalesPoint.Core.SelectedNavigationAdministrationLeft.SettingsAndStatus }); %></div>
        <% } %>

        <div id="column-right">
            <h2>Configuration</h2>
            <div class="break"></div>

            <% using (Html.BeginForm()) {%>
                <%= Html.ValidationSummary(true) %>


                <% if(Page.User.IsInRole("SalesPoint Administrators") || Page.User.IsInRole("Max Price Book Publishers")) { %>
                <div class="ui-widget-header padding-five">Application Status</div>
        
                <div class="editor-label-left-lg">Max Web Application Active?</div>
                <div class="editor-field">
                    <%= Html.CheckBoxFor(model => model.IsMaxEnabled) %>
                    <%= Html.ValidationMessageFor(model => model.IsMaxEnabled)%>

                    <% if(Model.IsMaxEnabled) { %><span class="TextGreen">Active</span><% } %>
                    <% else { %><span class="TextRed">Inactive</span><% } %>
                </div>
        
                <p>
                    <input type="submit" value="Save" class="ui-button ui-widget ui-state-default ui-corner-all" />
                </p>
                <% } %>

                
                <div class="ui-widget-header padding-five">Configurator</div>
                <div class="break"></div>
                
                <div class="editor-field"><input type="button" id="ConfiguratorPrePublish" value="Execute PriceBook Pre-Publish" onclick="prePublish();" style="width:239px" class="ui-button ui-widget ui-state-default ui-corner-all" /></div>

                <div id="PrePublishResultDiv">
                    <img src="<%= Url.Content("~/Content/Images/AjaxWait.gif") %>" alt="Working..." title="Working..." id="ImageWaitPrePublish" />
                    <table style="margin-left: 10px">
                        <thead class="ui-widget-header">
                            <tr>
                                <th>Pre-Publish Results</th>
                            </tr>
                        </thead>
                        <tbody id="PrePublishResult"></tbody>
                    </table>
                </div>

                <div class="break"></div>

                <div class="editor-field"><input type="button" id="ConfiguratorPostPublish" value="Execute PriceBook Post-Publish" onclick="postPublish();" style="width:239px" class="ui-button ui-widget ui-state-default ui-corner-all" /></div>

                <div id="PostPublishResultDiv">
                    <img src="<%= Url.Content("~/Content/Images/AjaxWait.gif") %>" alt="Working..." title="Working..." id="ImageWaitPostPublish" />
                    <table style="margin-left: 10px">
                        <thead class="ui-widget-header">
                            <tr>
                                <th>Post-Publish Results</th>
                            </tr>
                        </thead>
                        <tbody id="PostPublishResult"></tbody>
                    </table>
                </div>
            <% } %>
        </div>
    </div>

    <script type="text/javascript">
        $('#PrePublishResultDiv').hide();
        $('#PostPublishResultDiv').hide();
        <% if(Model.PriceBookPublishState != Masonite.MTier.PublishState.Inactive) { %>$('#ConfiguratorPrePublish').prop("disabled", true);<% } %>
        <% if(Model.PriceBookPublishState != Masonite.MTier.PublishState.PrePublishComplete) { %>$('#ConfiguratorPostPublish').prop("disabled", true);<% } %>
    </script>
</asp:Content>