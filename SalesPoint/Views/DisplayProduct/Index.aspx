﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<Masonite.MTier.SalesPoint.DisplayProduct>>" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">
        var controllerPath = '<%= VirtualPathUtility.ToAbsolute("~/" + ViewContext.RouteData.Values["Controller"]) %>';
        function deleteCompleted() {
            var searchName = $('#searchName').val();
            if (searchName.length > 0) {
                window.location = '<%= VirtualPathUtility.ToAbsolute("~/DisplayProduct") %>?searchName=' + escape(searchName);
            }
            else {
                window.location.reload();
            }
        }
    </script>

    <div id="TwoColumnWrapper">
        <% if(User.IsInRole("SalesPoint Administrators")) { %>
        <div id="column-left" class="float-left"><% Html.RenderAction("AdministrationLeft", "Navigation", new { selectedMenuItem = SalesPoint.Core.SelectedNavigationAdministrationLeft.DisplayProducts }); %></div>
        <% } %>

        <div id="column-right">
            <h2>Display Products</h2><%= Html.ActionLink("View All", "Index")%> | <%= Html.ActionLink("Create New", "Edit")%>
            <div class="break"></div>

            <div>
                <% using(Html.BeginForm()) { %> 
                    <input type="text" class="editor-field" name="searchName" id="searchName" value="<%: ViewData["searchName"] %>" /> 
                    <input type="submit" value="Search" class="ui-button ui-widget ui-state-default ui-corner-all" />
                    <div class="recordcount">Viewing the top <%= ViewData.Model.Count<Masonite.MTier.SalesPoint.DisplayProduct>().ToString()%> result(s).</div>
                <% } %>
            </div>

            <table style="width:100%;">
                <thead class="ui-widget-header">
                    <tr>
                        <th>Name</th>
                        <th>Code</th>
                        <th>Extended Description</th>
                        <th></th>
                    </tr>
                </thead>
        
            <% foreach (Masonite.MTier.SalesPoint.DisplayProduct item in Model) { %>
                <tr onclick="selectTableRow(this);">
                    <td><%= Html.ActionLink(item.Name, "Edit", new { id = item.Id })%></td>
                    <td><%: item.Code %></td>
                    <td><%: item.Description %></td>
                    <td class="cellalignmiddle"><img alt="Delete" title="Delete" src="<%=Url.Content("~/Content/Images/Delete.png")%>" onclick="deleteItem('<%= item.Id %>', controllerPath)" class="gridicon" /></td>
                </tr>
            <% } %>

            </table>
        </div>
    </div>
</asp:Content>