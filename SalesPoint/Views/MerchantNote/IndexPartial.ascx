﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<Masonite.MTier.SalesPoint.MerchantNote>>" %>

<script type="text/javascript">
    var lastFlagIsOn;
    var lastMerchantNoteId;

    function setFlag(merchantNoteId, isFlagged) {
        lastMerchantNoteId = merchantNoteId;
        lastFlagIsOn = isFlagged;

        $.ajax({
            type: "POST",
            url: '<%= VirtualPathUtility.ToAbsolute("~/MerchantNote/SetFlag/") %>' + merchantNoteId + "?isFlagged=" + isFlagged
        }).done(function () {
            if (lastFlagIsOn) {
                $('#' + lastMerchantNoteId + 'Off').hide();
                $('#' + lastMerchantNoteId + 'On').show();
            }
            else {
                $('#' + lastMerchantNoteId + 'On').hide();
                $('#' + lastMerchantNoteId + 'Off').show();
            }
        });
    }
    setMerchantName('<%= ViewData["MerchantName"] %>');
</script>

<div id="ContentElement">
    <% using (Ajax.BeginForm("Edit", "MerchantNote", new { merchantId = ViewData["MerchantId"] }, new AjaxOptions { HttpMethod = "Post", LoadingElementId = "Loading", OnSuccess = "function() { window.location.reload(); }" })) { %>
        <br />
        <div style="border-style: solid; border-width: 1px; padding-top: 10px; padding-left: 15px;">
            <div class="editor-label-left">Note</div>
            <div class="editor-field"><textarea id="Text" name="Text" cols="100" rows="4"></textarea></div>
                
            <!--
            <div class="editor-label-left">Tags</div>
            <div class="editor-field"><input type="text" id="Tags" name="Tags" class="Text200" />&nbsp;* Separate individual tags with commas</div>
            -->
                
            <div class="editor-label-left">Flag this note</div>
            <div class="editor-field"><input type="checkbox" id="IsFlagged" name="IsFlagged" /></div>
                
            <p>
                <input type="submit" value="Save" onclick="this.style.display='none';document.getElementById('LoadingMerchantNote').style.display = 'inline';" class="ui-button ui-widget ui-state-default ui-corner-all" /><img id="LoadingMerchantNote" alt="Loading" title="Loading" style="display: none;" src="<%=Url.Content("~/Content/Images/Loading.gif")%>" />
            </p>
        </div>
        <br />
    <% } %>
        
    <div class="recordcount">Viewing the top <%= ViewData.Model.Count<Masonite.MTier.SalesPoint.MerchantNote>().ToString()%> result(s).</div>

    <table style="width:100%;">
        <thead class="ui-widget-header">
            <tr>
                <th></th>
                <th>Note</th>
                <th>By</th>
                <th>Date</th>
            </tr>
        </thead>

    <% foreach (Masonite.MTier.SalesPoint.MerchantNote item in Model) { %>
        
        <tr onclick="selectTableRow(this);">
            <td class="cellalignmiddle">
                <img id="<%= item.Id %>On" style='display: <%= item.IsFlagged ? "inline" : "none" %>' onclick='setFlag(<%= item.Id %>, false);' alt="This note is flagged." title="This note is flagged" src="<%=Url.Content("~/Content/Images/FlagOn.png")%>" class="gridicon" />
                <img id="<%= item.Id %>Off" style='display: <%= !item.IsFlagged ? "inline" : "none" %>' onclick='setFlag(<%= item.Id %>, true);' alt="Flag this note" title="Flag this note" src="<%=Url.Content("~/Content/Images/FlagOff.png")%>" class="gridicon" />
            </td>
            <td><%: item.Text %></td>
            <td><%: item.CreatedBy %></td>
            <td><%: String.Format(System.Globalization.CultureInfo.CurrentUICulture, "{0:g}", item.DateCreated) %></td>
        </tr>
        
    <% } %>

    </table>
</div>