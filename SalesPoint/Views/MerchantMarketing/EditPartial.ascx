﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Masonite.MTier.SalesPoint.MerchantMarketing>" %>
    <style type="text/css">
        .rt
        {
            white-space:nowrap;
            /*--display:block;*/
        }
        .rc
        {
            vertical-align:bottom;
            white-space:nowrap;
            padding-bottom:10px;
        }
        .rh td
        {
            border-style:none;
            text-align:center;
            vertical-align:top;
            font-weight:bold;
        }
        .rn td
        {
            border-style:solid;
            border-width:1px;
            border-color:#BDCEDF;
            vertical-align:middle;
            text-align:center;
        }
    </style>
   
    <script type="text/javascript">
        setMerchantName('<%= ViewData["MerchantName"] %>');
    </script>
    
     <% using (Html.BeginForm("Edit", "MerchantMarketing", new { merchantId = ViewData["MerchantId"] })) { %>
        <%= Html.ValidationSummary(true) %>
        
        <div class="ui-widget-header padding-five">Location Options</div>
        
        <table style="border-style:none; width:400px;" class="merchant-edit">
            <tr>
                <td>Carries Entry Doors?</td>
                <td>
                    <%= Html.CheckBoxFor(model => model.IsCarrierOfEntryDoors)%>
                    <%= Html.ValidationMessageFor(model => model.IsCarrierOfEntryDoors)%>
                </td>
            </tr>
            <tr>
                <td>Carries Interior Molded Doors?</td>
                <td>
                    <%= Html.CheckBoxFor(model => model.IsCarrierOfInteriorMoldedDoors)%>
                    <%= Html.ValidationMessageFor(model => model.IsCarrierOfInteriorMoldedDoors)%>
                </td>
            </tr>
            <tr>
                <td>Carries Interior Stile and Rail Doors?</td>
                <td>
                    <%= Html.CheckBoxFor(model => model.IsCarrierOfInteriorStileAndRailDoors)%>
                    <%= Html.ValidationMessageFor(model => model.IsCarrierOfInteriorStileAndRailDoors)%>
                </td>
            </tr>
            <tr>
                <td>Carries Exterior Decorative Glass?</td>
                <td>
                    <%= Html.CheckBoxFor(model => model.IsCarrierOfSpecialtyGlass)%>
                    <%= Html.ValidationMessageFor(model => model.IsCarrierOfSpecialtyGlass)%>
                </td>
            </tr>
            <tr>
                <td>Allow Reps to Participate in Promotions?</td>
                <td>
                    <%= Html.CheckBoxFor(model => model.IsPromotionParticipationAccessibleToSalesReps)%>
                    <%= Html.ValidationMessageFor(model => model.IsPromotionParticipationAccessibleToSalesReps)%>
                </td>
            </tr>
            <tr>
                <td>National Account?</td>
                <td>
                    <%= Html.CheckBoxFor(model => model.IsNationalAccount)%>
                    <%= Html.ValidationMessageFor(model => model.IsNationalAccount)%>
                </td>
            </tr>
            <tr>
                <td>Region</td>
                <td>
                    <%= Html.DropDownListFor(model => model.Region, new SelectList(SalesPoint.Models.Marketing.GetMarketingRegionCollection(), "Name", "Name", Model.Region), " ")%>
                    <%= Html.ValidationMessageFor(model => model.Region) %>
                </td>
            </tr>
        </table>
        
        <div class="ui-widget-header padding-five">Contact Options</div>
        <table id="ContactAttributes" style="border-style:none;">
            <tr class="rh">
                <td style="width:200px;"></td>
                <td style="width:120px;text-align:center;" colspan="4">Receive Price Increases For:</td>
                <td style="width:30px"></td>
                <td style="width:120px;text-align:center;" colspan="4">Receive Marketing Product Bulletins For:</td>
                <td style="width:30px">&nbsp;</td>
                <td style="width:60px" colspan="2">&nbsp;</td>
                <td style="width:30px">&nbsp;</td>
                <td style="width:30px">&nbsp;</td>
            </tr>
            <tr>
                <td style="vertical-align:bottom;" class="rt">
                    <a href="javascript:check(document.getElementById('ContactAttributes'), true);" title="Check all in this table">Check All</a>&nbsp;|&nbsp;<a href="javascript:check(document.getElementById('ContactAttributes'), false);" title="Uncheck all in this table">Check None</a>
                </td>
                <td class="rc"><div class="rt" title="Entry Doors">Entry Doors</div></td>
                <td class="rc"><div class="rt" title="Interior Molded Doors">Interior Molded Doors</div></td>
                <td class="rc"><div class="rt" title="Stile-and-Rail Doors">Stile-and-Rail Doors</div></td>
                <td class="rc"><div class="rt" title="Exterior Decorative Glass">Exterior Decorative Glass</div></td>
                <td class="rc"></td>
                <td class="rc"><div class="rt" title="Entry Doors">Entry Doors</div></td>
                <td class="rc"><div class="rt" title="Interior Molded Doors">Interior Molded Doors</div></td>
                <td class="rc"><div class="rt" title="Stile-and-Rail Doors">Stile-and-Rail Doors</div></td>
                <td class="rc"><div class="rt" title="Exterior Decorative Glass">Exterior Decorative Glass</div></td>
                <td class="rc"></td>
                <td class="rc"><div class="rt" title="Email Marketing">Email Marketing</div></td>
                <td class="rc"><div class="rt" title="Newsletter">Newsletter</div></td>
                <td class="rc"></td>
                <td class="rc"><div class="rt" title="French Speaking">French Speaking</div></td>
            </tr>
            <% 
                System.Collections.Generic.List<Masonite.MTier.SalesPoint.MerchantContact> contacts = Masonite.MTier.SalesPoint.ObjectFactory.RetrieveMerchantContacts((int)ViewData["MerchantId"]);
                foreach(Masonite.MTier.SalesPoint.MerchantContact contact in contacts) {
            %>
            <tr onclick="selectTableRow(this);" class="rn" id="Row<%=contact.Id %>">
                <td style="text-align:left;"><%= contact.LastName + (string.IsNullOrEmpty(contact.FirstName) ? "" : ", " + contact.FirstName)%><div style="float:right"><input type="checkbox" onclick="check(document.getElementById('Row<%=contact.Id %>'), this.checked);" title="Check or uncheck all in this row" /></div></td>
                <td><input type="checkbox" name="IsReceivingPriceIncreasesEntryDoor" value="<%=contact.Id %>" <%=contact.MarketingAttributes.IsReceivingPriceIncreasesEntryDoor ? "checked='checked'" : "" %> /></td>
                <td><input type="checkbox" name="IsReceivingPriceIncreasesInteriorMoldedDoor" value="<%=contact.Id %>" <%=contact.MarketingAttributes.IsReceivingPriceIncreasesInteriorMoldedDoor ? "checked='checked'" : "" %> /></td>
                <td><input type="checkbox" name="IsReceivingPriceIncreasesStileAndRail" value="<%=contact.Id %>" <%=contact.MarketingAttributes.IsReceivingPriceIncreasesStileAndRail ? "checked='checked'" : "" %> /></td>
                <td><input type="checkbox" name="IsReceivingPriceIncreasesExteriorDecorativeGlass" value="<%=contact.Id %>" <%=contact.MarketingAttributes.IsReceivingPriceIncreasesExteriorDecorativeGlass ? "checked='checked'" : "" %> /></td>
                <td></td>
                <td><input type="checkbox" name="IsReceivingProductBulletinsEntryDoor" value="<%=contact.Id %>" <%=contact.MarketingAttributes.IsReceivingProductBulletinsEntryDoor ? "checked='checked'" : "" %> /></td>
                <td><input type="checkbox" name="IsReceivingProductBulletinsInteriorMoldedDoor" value="<%=contact.Id %>" <%=contact.MarketingAttributes.IsReceivingProductBulletinsInteriorMoldedDoor ? "checked='checked'" : "" %> /></td>
                <td><input type="checkbox" name="IsReceivingProductBulletinsStileAndRail" value="<%=contact.Id %>" <%=contact.MarketingAttributes.IsReceivingProductBulletinsStileAndRail ? "checked='checked'" : "" %> /></td>
                <td><input type="checkbox" name="IsReceivingProductBulletinsExteriorDecorativeGlass" value="<%=contact.Id %>" <%=contact.MarketingAttributes.IsReceivingProductBulletinsExteriorDecorativeGlass ? "checked='checked'" : "" %> /></td>
                <td></td>
                <td><input type="checkbox" name="IsReceivingEmailMarketing" value="<%=contact.Id %>" <%=contact.MarketingAttributes.IsReceivingEmailMarketing ? "checked='checked'" : "" %> /></td>
                <td><input type="checkbox" name="IsReceivingNewsletter" value="<%=contact.Id %>" <%=contact.MarketingAttributes.IsReceivingNewsletter ? "checked='checked'" : "" %> /></td>
                <td></td>
                <td><input type="checkbox" name="IsFrenchSpeaking" value="<%=contact.Id %>" <%=contact.MarketingAttributes.IsFrenchSpeaking ? "checked='checked'" : "" %> /></td>
            </tr>
            <% } %>
        </table>
        
        <p>
            <input type="submit" value="Save" class="ui-button ui-widget ui-state-default ui-corner-all" />
        </p>
    <% } %>