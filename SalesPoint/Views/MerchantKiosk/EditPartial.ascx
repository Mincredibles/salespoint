﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Masonite.MTier.SalesPoint.Merchant>" %>
<%
    string defaultUrl = null;
    if (string.IsNullOrEmpty(Model.KioskUrl))
    {
        var distributorDealer = Model.Distributors.FirstOrDefault();
        if (distributorDealer != null)
            defaultUrl = string.Format(System.Configuration.ConfigurationManager.AppSettings["KioskDefaultUrl"], distributorDealer.MerchantDistributorId, Model.Id);
    }
%>
<% using (Ajax.BeginForm("Edit", "MerchantKiosk", null, new AjaxOptions { HttpMethod = "POST", UpdateTargetId = "ActiveTabContent", OnComplete = "" })) { %>
    <%= Html.ValidationSummary() %>
    <%= Html.HiddenFor(model => model.Id) %>
    <%= Html.HiddenFor(model => model.Name) %>
    <%= Html.HiddenFor(model => model.City) %>
    <%= Html.HiddenFor(model => model.Address) %>
    <%= Html.HiddenFor(model => model.PostalCode) %>
        
    <div class="alignment-fix">
        <div class="full-width ui-widget-header padding-five">Kiosk</div>

        <div class="editor-label-left extra-top-buffer">Kiosk Email Address</div>
        <div class="editor-field">
            <%= Html.TextBoxFor(model => model.KioskEmail, new { @class = "Text250" })%>
        </div>
        <div><%= Html.ValidationMessageFor(model => model.KioskEmail)%></div>
                        
        <div class="editor-label-left">Kiosk Number</div>
        <div class="editor-field">
            <%= Html.TextBoxFor(model => model.KioskNumber, new { id = "kiosk-number", @class = "Text250" })%>
        </div>
                        
        <div class="editor-label-left">Kiosk URL</div>
        <div class="editor-field">
            <%= Html.TextBoxFor(model => model.KioskUrl, new { id = "kiosk-url", style = "width: 700px;" })%>
        </div>

        <div class="editor-label-left">Kiosk Name</div>
        <div class="editor-field">
            <%= Html.TextBoxFor(model => model.KioskName, new { @class = "Text250" })%>
        </div>
    </div>
        
    <%--<p class="required">* Required field</p>--%>
    <p>
        <input type="submit" value="Save" id="SaveButton2" class="ui-button ui-widget ui-state-default ui-corner-all" />
    </p>
<% } %>
    
<script type="text/javascript">
    $('#KioskEmail').select(); // Pre-select the Name field.

    <% if (defaultUrl != null) { %>

    $('#kiosk-number').keyup(function() {
        if ($('#kiosk-number').val() != '0' && $('#kiosk-url').val().length == 0)
            $('#kiosk-url').val('<%= defaultUrl %>');
    })
            
    <% } %>


</script>