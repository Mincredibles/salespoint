﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<Masonite.MTier.SalesPoint.UserGroup>>" %>
    
<script type="text/javascript">
    var lastElement;
    var isPageReloadNeeded = false;
    var merchantId = '<%= ViewData["MerchantId"] %>';

    $(function () {
        $("#dialog-modal").dialog({
            height: 450,
            width: 600,
            modal: true,
            autoOpen: false,
            resizable: false,
            close: dialogClose
        });
    });
    function dialogClose() {
        if (isPageReloadNeeded == true) {
            window.location.reload();
        }
        else {
            $('#result').empty()
        }
    }
    function userGroupMerchantDialog() {
        $('#dialog-content').load('<%= VirtualPathUtility.ToAbsolute("~/UserGroupMerchant/ChooseUserGroupsPartial")%>');
        $('#dialog-modal').dialog('open');
        $('#searchName').select();
    }
    function addUserGroup(userGroupId, span) {
        $('#Wait').show();

        lastElement = span;
        isPageReloadNeeded = true;

        $.ajax({
            type: "POST",
            url: '<%= VirtualPathUtility.ToAbsolute("~/UserGroupMerchant/Edit/") %>' + userGroupId + '?merchantId=' + merchantId
        }).done(function () {
            $('#Wait').hide();
            if (lastElement) { $(lastElement).hide(); }
            isPageReloadNeeded = true;
        });
    }
    function removeUserGroup(userGroupId, tableRow) {
        lastElement = tableRow;

        $.ajax({
            type: "DELETE",
            url: '<%= VirtualPathUtility.ToAbsolute("~/UserGroupMerchant/Delete/") %>' + userGroupId + '?merchantId=' + merchantId
        }).done(function () {
            if (lastElement) {
                $(lastElement).parent().parent().hide();
            }
        });
    }
    setMerchantName('<%= ViewData["MerchantName"] %>');
</script>
<div id="ContentElementGroups">
    <div style="margin-bottom: 5px;">
        <input type="button" value="Add Groups" onclick="userGroupMerchantDialog();" class="ui-button ui-widget ui-state-default ui-corner-all" />
        <div class="recordcount">Viewing the top <%= ViewData.Model.Count<Masonite.MTier.SalesPoint.UserGroup>().ToString()%> result(s).</div>
    </div>

    <table style="width:100%;">
        <thead class="ui-widget-header">
            <tr>
                <th>UserGroup</th>
                <th></th>
            </tr>
        </thead>

        <% foreach (Masonite.MTier.SalesPoint.UserGroup item in Model) { %>
            <tr onclick="selectTableRow(this);">
                <td><%: item.Name %></td>
                <td class="cellalignmiddle"><img alt="Remove" title="Remove" src="<%=Url.Content("~/Content/Images/Delete.png")%>" onclick="removeUserGroup(<%= item.Id %>, this);" class="gridicon" /></td>
            </tr>
        <% } %>

    </table>
</div>

<div id="dialog-modal" title="Add Groups">
<% using (Ajax.BeginForm("ChooseUserGroupsPartial", "UserGroupMerchant", new AjaxOptions { UpdateTargetId = "result", HttpMethod = "Post", LoadingElementId = "Wait" })) { %>
    <div>    
        <input id="searchName" name="searchName" type="text" value="<%= Html.Encode(ViewData["searchName"])%>" />
        <input type="submit" value="Search" class="ui-button ui-widget ui-state-default ui-corner-all" />
    </div>
    <div class="min-height-15"><img src="<%= Url.Content("~/Content/Images/AjaxWait.gif") %>" alt="Working..." title="Working..." id="Wait" /></div>
    <div id="dialog-content"></div>
<% } %>
</div>

<script type="text/javascript">
    $("#Wait").hide();
</script>