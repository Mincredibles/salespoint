﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<Masonite.MTier.SalesPoint.Merchant>>" %>

<asp:content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div id="TwoColumnWrapper">
        <% if(User.IsInRole("SalesPoint Administrators")) { %>
        <div id="column-left" class="float-left"><% Html.RenderAction("AdministrationLeft", "Navigation", new { selectedMenuItem = SalesPoint.Core.SelectedNavigationAdministrationLeft.UserGroups }); %></div>
        <% } %>

        <div id="column-right">
            <h2>Members of '<%: Masonite.MTier.SalesPoint.ObjectFactory.RetrieveUserGroup(Masonite.MTier.Utility.ConversionHelper.ToInt32(Request.QueryString["userGroupId"]), User.Identity.Name).Name %>'</h2><%= Html.ActionLink("Back to User Groups", "Index", "UserGroup")%> | <a href="javascript:void(0);" onclick="userGroupMerchantDialog();">Add Locations</a>
            <div class="break"></div>

            <% Html.RenderPartial("IndexMerchantPartial", Model, ViewData); %>
        </div>
    </div>

    <div id="dialog-modal" title="Add Locations">
    <% using (Ajax.BeginForm("ChooseMerchantsPartial", "UserGroupMerchant", new AjaxOptions { UpdateTargetId = "result", HttpMethod = "Post", LoadingElementId = "Wait" })) { %>
        <div>
            <input id="searchName" name="searchName" type="text" value="<%: ViewData["searchName"] %>" />
            <input type="submit" value="Search" class="ui-button ui-widget ui-state-default ui-corner-all" />
        </div>
        <div class="min-height-15"><img src="<%= Url.Content("~/Content/Images/AjaxWait.gif") %>" alt="Working..." title="Working..." id="Wait" /></div>
        <div id="dialog-content"></div>
    <% } %>
    </div>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#Wait').hide();
        });
    </script>
</asp:content>