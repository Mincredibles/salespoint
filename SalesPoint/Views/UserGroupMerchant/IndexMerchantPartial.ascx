﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<Masonite.MTier.SalesPoint.Merchant>>" %>
    
<script type="text/javascript">
    var isPageReloadNeeded = false;
    var userGroupId = '<%= ViewData["UserGroupId"] %>';
    var lastElement;
    $(function () {
        $("#dialog-modal").dialog({
            height: 390,
            width: 584,
            modal: true,
            autoOpen: false,
            resizable: false,
            close: dialogClose
        });
    });
    function dialogClose() {
        if (isPageReloadNeeded == true) {
            window.location.reload();
        }
        else {
            $('#result').empty()
        }
    }
    function userGroupMerchantDialog() {
        $('#dialog-content').load('<%= VirtualPathUtility.ToAbsolute("~/UserGroupMerchant/ChooseMerchantsPartial")%>');
        $('#dialog-modal').dialog('open');
        $('#searchName').select();
    }
    function addMerchant(merchantId, span) {
        $('#Wait').show();

        lastElement = span;
        isPageReloadNeeded = true;

        $.ajax({
            type: "POST",
            url: '<%= VirtualPathUtility.ToAbsolute("~/UserGroupMerchant/Edit/") %>' + userGroupId + '?merchantId=' + merchantId
        }).done(function () {
            $('#Wait').hide();
            if (lastElement) { $(lastElement).hide(); }
            isPageReloadNeeded = true;
        });
    }
    function removeMerchant(merchantId, span) {
        lastElement = span;

        $.ajax({
            type: "DELETE",
            url: '<%= VirtualPathUtility.ToAbsolute("~/UserGroupMerchant/Delete/") %>' + userGroupId + '?merchantId=' + merchantId
        }).done(function () {
            if (lastElement) {
                $(lastElement).parent().parent().hide();
            }
        });
    }
</script>
    
<div class="recordcount">Viewing the top <%= ViewData.Model.Count<Masonite.MTier.SalesPoint.Merchant>().ToString()%> result(s).</div>

<table style="width:100%;">
    <thead class="ui-widget-header">
        <tr>
            <th>Location</th>
            <th></th>
        </tr>
    </thead>

    <% foreach (Masonite.MTier.SalesPoint.Merchant item in Model)
        { %>
        <tr onclick="selectTableRow(this);">
            <td><%: item.Name %></td>
            <td class="cellalignmiddle"><img alt="Remove" title="Remove" src="<%=Url.Content("~/Content/Images/Delete.png")%>" onclick="removeMerchant(<%= item.Id %>, this);" class="gridicon" /></td>
        </tr>
    <% } %>

</table>