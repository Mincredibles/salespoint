﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<Masonite.MTier.SalesPoint.UserGroup>>" %>

<table id="result" class="no-border">
<% if(ViewData.Model.Count<Masonite.MTier.SalesPoint.UserGroup>() == 0) { %>
        <tr><td class="no-border">No items match the current search criteria</td></tr>
<% } else { %>
    <% foreach (Masonite.MTier.SalesPoint.UserGroup item in Model) { %>
        <tr>
            <td class="no-border"><a href="javascript:void(0);" onclick="addUserGroup(<%= item.Id %>,this);">Add</a></td>
            <td class="no-border"><%: item.Name %></td>
        </tr>
    <% } %>
<% } %>
</table>