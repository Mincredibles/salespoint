﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<Masonite.MTier.SalesPoint.Structure>>" %>

<script type="text/javascript">
    var controllerPath = '<%= VirtualPathUtility.ToAbsolute("~/" + ViewContext.RouteData.Values["Controller"]) %>';
    var isPageReloadNeeded = false;
    var lastElement;
    setMerchantName('<%= ViewData["MerchantName"] %>');

    $(function () {
        $("#dialog-modal-EditStructure").dialog({
            height: 450,
            width: 600,
            modal: true,
            autoOpen: false,
            resizable: false,
            close: function() {
                dialogClose("#dialog-content-Structure");
            }
        });
    });
    function dialogClose(dialogId) {
        if (isPageReloadNeeded == true) {
            window.location.reload();
        }
        else {
            $(dialogId).empty();
        }
    }
    function editStructureDialog(id) {
        $('#dialog-content-Structure').load('<%= VirtualPathUtility.ToAbsolute("~/Structure/EditPartial")%>/' + id + '?merchantId=<%= ViewData["MerchantId"]%>');
        $('#dialog-modal-EditStructure').dialog('open');
    }
    function deleteCompleted() {
        if (lastElement) {
            $(lastElement).parent().parent().hide();
        }
    }
    function saveCompleted(data) {
        if (data.success == false) {
            $('#SubmitButton').show();
            $('#LoadingStructure').hide();
            window.alert(data.message);
                
            return;
        }
    
        isPageReloadNeeded = true;
        $('#dialog-modal-EditStructure').dialog('close');
    }
    function postalCodeLookup(postalCode) {
        if (postalCode.length == 0) { return false; }

        $("#ImageWaitPostalCodeLookup").show();

        $.ajax({
            type: "POST",
            url: '<%= VirtualPathUtility.ToAbsolute("~/Utility/PostalCodeLookup") %>?postalCode=' + postalCode
        }).done(function (data) {
            var useZip = "defaultCityStateZip('','','" + data.City + "', '" + data.StateName + "', '" + data.PostalCode + "', '" + data.County + "');";
            $('#Zip').html("<a href='javascript:void(0);' onclick=\"" + useZip + "\">Use " + data.City + ", " + data.StateName + " " + data.PostalCode + "</a>");

            $("#ImageWaitPostalCodeLookup").hide();
        });
    }
    function defaultCityStateZip(address, address2, city, state, postalCode, county) {
        $('#City').val(city);
        $('#State').val(state);
        $('#PostalCode').val(postalCode);
        $('#InteriorDoors').select();
    }
    function Validate() {
        // Manual validation in place because the MVC validation doesn't work on AJAX-loaded forms
        var isDataValid = true;
        var control;

        control = document.getElementById('Address1');
        if (control.value.length == 0) {
            control.className = 'input-validation-error';
            $('#Address1_validationMessage').show();
            isDataValid = false;
        }
        else {
            control.className = '';
            $('#Address1_validationMessage').hide();
        }

        control = document.getElementById('City');
        if (control.value.length == 0) {
            control.className = 'input-validation-error';
            $('#City_validationMessage').show();
            isDataValid = false;
        }
        else {
            control.className = '';
            $('#City_validationMessage').hide();
        }

        control = document.getElementById('State');
        if (control.value.length == 0) {
            control.className = 'input-validation-error';
            $('#State_validationMessage').show();
            isDataValid = false;
        }
        else {
            control.className = '';
            $('#State_validationMessage').hide();
        }

        control = document.getElementById('Distributor');
        if (control.value.length == 0) {
            control.className = 'input-validation-error';
            $('#Distributor_validationMessage').show();
            isDataValid = false;
        }
        else {
            control.className = '';
            $('#Distributor_validationMessage').hide();
        }

        if (isDataValid == false) {
            $('#SubmitButton').hide();
            $('#LoadingStructure').hide()
        }
        else {
            $('#SubmitButton').show();
        }

        return isDataValid;
    }
</script>
<div>
    <div style="margin-bottom: 5px;">
        <div class="float-left">
            <input type="button" value="Add Model Home" onclick="editStructureDialog(0);" />
        </div>

        <div class="break"></div>

        <div class="recordcount">Viewing the top <%= ViewData.Model.Count<Masonite.MTier.SalesPoint.Structure>().ToString()%> result(s).</div>
    </div>
        
    <% using (Html.BeginForm("IndexEdit", "Structure", new { merchantId = ViewData["id"] })) { %>
        <%= Html.ValidationSummary(true) %>

        <table class="full-width">
            <thead class="ui-widget-header">
                <tr>
                    <th>Address</th>
                    <th>Development</th>
                    <th>Lot Number(s)</th>
                    <th>Distributor</th>
                    <th>Exterior Doors</th>
                    <th>Interior Doors</th>
                    <th></th>
                </tr>
            </thead>
                
            <% foreach (Masonite.MTier.SalesPoint.Structure item in Model) { %>
                
            <tr onclick="selectTableRow(this);">
                <td>
                    <a onclick="editStructureDialog(<%=item.Id %>);" href='javascript:void(0);'><%= item.Address1 %></a><br />
                    <%: item.City %>, <%: item.State %>
                </td>
                <td><%: item.HousingDevelopment %></td>
                <td><%: item.LotNumber %></td>
                <td><%: item.Distributor %></td>
                <td><%: item.ExteriorDoors %> <%: item.ExteriorDoorQuantity > 0 ? " (" + item.ExteriorDoorQuantity.ToString(System.Globalization.CultureInfo.CurrentUICulture) + " Qty)" : "" %></td>
                <td><%: item.InteriorDoors %> <%: item.InteriorDoorQuantity > 0 ? " (" + item.InteriorDoorQuantity.ToString(System.Globalization.CultureInfo.CurrentUICulture) + " Qty)" : "" %></td>
                <td class="cellalignmiddle"><img alt="Delete" title="Delete" src="<%=Url.Content("~/Content/Images/Delete.png")%>" onclick="lastElement = this;deleteItem('<%= item.Id %>', controllerPath)" class="gridicon" /></td>
            </tr>
            <% } %>

        </table>
    <% } %>

</div>

<div id="dialog-modal-EditStructure" title="Edit Model Home">
    <div id="dialog-content-Structure"></div>
</div>