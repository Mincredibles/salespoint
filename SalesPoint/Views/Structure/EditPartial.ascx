﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Masonite.MTier.SalesPoint.Structure>" %>

<% using (Ajax.BeginForm("Edit", "Structure", new { id = ViewData["id"], merchantId = ViewData["MerchantId"] }, new AjaxOptions { HttpMethod = "Post", LoadingElementId = "Loading", OnSuccess = "saveCompleted" })) { %>
    <div>
        <div>
            <div class="editor-label-left">Development Name</div>
            <div class="editor-field">
                <%= Html.TextBoxFor(model => model.HousingDevelopment, new { @class = "Text300" })%>
                <%= Html.ValidationMessageFor(model => model.HousingDevelopment)%>
            </div>
        </div>

        <div class="clear"></div>
        
        <div>
            <div class="editor-label-left">Lot Number(s)</div>
            <div class="editor-field">
                <%= Html.TextBoxFor(model => model.LotNumber, new { @class = "Text100" })%>
                <%= Html.ValidationMessageFor(model => model.LotNumber)%>
            </div>
        </div>

        <div class="clear"></div>

        <div>
            <div class="editor-label-left">Address<span class="required"> *</span></div>
            <div class="editor-field">
                <%= Html.TextBoxFor(model => model.Address1, new { @class = "Text300", @onblur = "Validate();" })%>
                <%= Html.ValidationMessageFor(model => model.Address1)%>
                <div style="display:none;" id="Address1_validationMessage" class="field-validation-error">Address is a required field. Please provide a valid value.</div>
            </div>
        </div>

        <div class="clear"></div>

        <div>
            <div class="editor-label-left"></div>
            <div class="editor-field">
                <%= Html.TextBoxFor(model => model.Address2, new { @class = "Text300" })%>
                <%= Html.ValidationMessageFor(model => model.Address2)%>
            </div>
        </div>

        <div class="clear"></div>

        <div>
            <div class="editor-label-left">Postal Code</div>
            <div class="editor-field">
                <%= Html.TextBoxFor(model => model.PostalCode, new { @class = "Text100", @onblur = "postalCodeLookup(this.value);" })%> <img id="ImageWaitPostalCodeLookup" alt="Checking postal code..." title="Checking postal code..." src="<%=Url.Content("~/Content/Images/Loading.gif")%>" style="display: none;" /> <span id="Zip"></span>
                <%= Html.ValidationMessageFor(model => model.PostalCode)%>
            </div>
        </div>

        <div class="clear"></div>

        <div>
            <div class="editor-label-left">City<span class="required"> *</span></div>
            <div class="editor-field">
                <%= Html.TextBoxFor(model => model.City, new { @onblur = "Validate();" })%>
                <%= Html.ValidationMessageFor(model => model.City)%>
                <div style="display:none;" id="City_validationMessage" class="field-validation-error">City is a required field. Please provide a valid value.</div>
            </div>
        </div>

        <div class="clear"></div>

        <div>
            <div class="editor-label-left">State<span class="required"> *</span></div>
            <div class="editor-field">
                <%= Html.TextBoxFor(model => model.State, new { @class = "Text100", @onblur = "Validate();" })%>
                <%= Html.ValidationMessageFor(model => model.State)%>
                <div style="display:none;" id="State_validationMessage" class="field-validation-error">State is a required field. Please provide a valid value.</div>
            </div>
        </div>
        
        <div class="clear"></div>

        <div>
            <div class="editor-label-left">Interior Door(s)</div>
            <div class="editor-field">
                <%= Html.TextBoxFor(model => model.InteriorDoors, new { @class = "Text300" })%>
                <%= Html.ValidationMessageFor(model => model.InteriorDoors)%>
            </div>
        </div>

        <div class="clear"></div>

        <div>
            <div class="editor-label-left">Interior Door Qty</div>
            <div class="editor-field">
                <%= Html.TextBoxFor(model => model.InteriorDoorQuantity, new { @class = "Text100" })%>
                <%= Html.ValidationMessageFor(model => model.InteriorDoorQuantity)%>
            </div>
        </div>

        <div class="clear"></div>

        <div>
            <div class="editor-label-left">Exterior Door(s)</div>
            <div class="editor-field">
                <%= Html.TextBoxFor(model => model.ExteriorDoors, new { @class = "Text300" })%>
                <%= Html.ValidationMessageFor(model => model.ExteriorDoors)%>
            </div>
        </div>

        <div class="clear"></div>

        <div>
            <div class="editor-label-left">Exterior Door Qty</div>
            <div class="editor-field">
                <%= Html.TextBoxFor(model => model.ExteriorDoorQuantity, new { @class = "Text100" })%>
                <%= Html.ValidationMessageFor(model => model.ExteriorDoorQuantity)%>
            </div>
        </div>

        <div class="clear"></div>

        <div>
            <div class="editor-label-left">Distributor<span class="required"> *</span></div>
            <div class="editor-field">
                <%= Html.TextBoxFor(model => model.Distributor, new { @class = "Text300", @onblur = "Validate();" })%>
                <%= Html.ValidationMessageFor(model => model.Distributor)%>
                <div style="display:none;" id="Distributor_validationMessage" class="field-validation-error">Distributor is a required field. Please provide a valid value.</div>
            </div>
        </div>

        <div class="clear"></div>
    </div>    
                
    <p class="required">* Required field</p>
    <p>
        <input id="SubmitButton" type="submit" value="Save" onclick="$(this).hide(); $('#LoadingStructure').show(); return Validate();" class="ui-button ui-widget ui-state-default ui-corner-all" /><img id="LoadingStructure" alt="Loading" title="Loading" style="display: none;" src="<%=Url.Content("~/Content/Images/Loading.gif")%>" />
    </p>
<% } %>