﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<Masonite.MTier.SalesPoint.ConfigureOneProductLine>>" %>

<select name="ConfigureOneProductLineId" id="ConfigureOneProductLineId">
    <option value=""></option>
<% foreach (var item in Model) { %>
    <option <%if (ViewData["ConfigureOneProductLineId"].ToString() == item.Id.ToString()){%>selected="selected"<%}%> value="<%= item.Id %>"><%: item.Name %></option>
<% } %>
</select>