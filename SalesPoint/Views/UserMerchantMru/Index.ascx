﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<Masonite.MTier.SalesPoint.Merchant>>" %>

<table style="color: #696969;">
    <thead class="ui-widget-header">
        <tr>
            <th>Name</th>
            <th>Number</th>
            <th>City</th>
            <th>State</th>
        </tr>
    </thead>

<% foreach (var item in Model) { %>
    
    <tr onclick="selectTableRow(this);">
        <td><%= Html.ActionLink(item.Name, "Edit", "Merchant", new { id = item.Id }, new {})%></td>
        <td><%: item.Number %></td>
        <td><%: item.City %></td>
        <td><%: item.StateName %></td>
    </tr>
    
<% } %>
</table>