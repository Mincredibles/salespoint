﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Masonite.MTier.SalesPoint;

namespace SalesPoint
{
    public class MvcApplication : System.Web.HttpApplication
    {
        // Deploying MVC apps to IIS6:
        // http://blog.stevensanderson.com/2008/07/04/options-for-deploying-aspnet-mvc-to-iis-6/

        // Overriding IIS6 wildcard maps on individual directories:
        //http://blog.stevensanderson.com/2008/07/07/overriding-iis6-wildcard-maps-on-individual-directories/


        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.IgnoreRoute("{*favicon}", new { favicon = @"(.*/)?favicon.ico(/.*)?" });

            routes.MapRoute(
                "Default", // Route name
                "{controller}/{action}/{id}", // URL with parameters
                new { controller = "Home", action = "Index", id = UrlParameter.Optional } // Parameter defaults
            );
        }

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RegisterRoutes(RouteTable.Routes);

            // Setting AddImplicitRequiredAttributeForValueTypes = false prevents the non-nullable
            // values (for instance, dates and numeric values) from automatically being tagged as "required" by the
            // validation provider.
            DataAnnotationsModelValidatorProvider.AddImplicitRequiredAttributeForValueTypes = false;
        }

        protected void Application_AuthenticateRequest(object sender, EventArgs args)
        {
            if (HttpContext.Current != null)
            {
                string url = HttpContext.Current.Request.Url.OriginalString;

                // Ignore requests for files of type:  js, css, png, jpg, gif
                if 
                    (
                           url.EndsWith(".js") 
                        || url.EndsWith(".css") 
                        || url.EndsWith(".png") 
                        || url.EndsWith(".jpg") 
                        || url.EndsWith(".gif")
                    ) 
                { 
                    return; 
                }

                // Using custom roles with Windows authentication
                // http://stackoverflow.com/questions/6043100/asp-net-mvc-and-windows-authentication-with-custom-roles

                // Hotfix for the error 'The trust relationship between the primary domain and the trusted domain failed.'
                // http://support.microsoft.com/kb/976494
            }
        }

        protected void Application_PostAuthenticateRequest(object sender, EventArgs e)
        {
            HttpContext context = HttpContext.Current;

            if (context.Request.IsAuthenticated)
            {
                // Retrieve roles from the database, and attach to the user (security principal)
                using (User user = Masonite.MTier.SalesPoint.ObjectFactory.RetrieveUser(context.User.Identity.Name))
                {
                    if (user != null)
                    {
                        String[] roles = new string[user.UserGroups.Count];
                        int position = 0;

                        foreach (UserGroup group in user.UserGroups)
                        {
                            roles[position] = group.Name;
                            position += 1;
                        }

                        System.Security.Principal.GenericPrincipal principal = new System.Security.Principal.GenericPrincipal(context.User.Identity, roles);
                        System.Threading.Thread.CurrentPrincipal = context.User = principal;
                    }
                }
            }
        }

        protected void Application_Error(Object sender, EventArgs e)
        {
            Masonite.MTier.Logging.LoggingHelper.Log("Masonite.SalesPoint", Context.Error, Masonite.MTier.Logging.EventCategory.Error);
        } 
    }
}