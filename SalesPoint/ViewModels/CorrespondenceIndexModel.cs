﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Masonite.MTier.SalesPoint;

namespace SalesPoint.ViewModels
{
    public class CorrespondenceIndexModel
    {
        public List<Correspondence> Correspondences { get; set; }
        public List<CorrespondenceApproval> CorrespondenceApprovals { get; set; }
    }
}