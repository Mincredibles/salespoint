﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Masonite.MTier.SalesPoint;

namespace SalesPoint.ViewModels
{
    public class RegistrationModel
    {
        public Registration Registration { get; set; }
        public List<RegistrationAttribute> RegistrationAttributes { get; set; }
    }
}