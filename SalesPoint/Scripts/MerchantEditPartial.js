﻿function merchantTypeChanged(newType) {
    if (newType == 1) {
        $('#ChainLabel').show();
    } 
    else {
        $('#ChainLabel').hide();
    }
}

function updateChain() {
    var categoryDropDownList = document.getElementById("CategoryId");

    if (categoryDropDownList.options[categoryDropDownList.selectedIndex].value == 1) {
        $('#ChainLabel').show();
    }
    else {
        $('#ChainLabel').hide();
    }
}

function validateSearchableOnWebsite() {
    var isSearchableElement = document.getElementById("IsSearchableOnMasoniteWebsite");

    if (isSearchableElement.checked) {
        if (!isNumeric(document.getElementById("Latitude").value) || !isNumeric(document.getElementById("Longitude").value)) {
            alert('Latitude and Longitude must be defined in order for the Location to be visible on masonite.com');
            isSearchableElement.checked = false;
        }
    }
    updateWebsiteStatus();
}

function updateWebsiteStatus() {
    var statusElement = document.getElementById("WebsiteStatus");
    var coordinatesLink = document.getElementById("CoordinatesLink");
    var googleLink = document.getElementById("googleLink");
    var linkSep = document.getElementById("linkSep");
    var status = new Array();
    var messagelist = new Array();
    var messagevalid = true;

    if (document.getElementById("IsSearchableOnMasoniteWebsite").checked) {
      status[0] = '1';
    } 
    else {
      status[0] = '0';
    }

    if (isNumeric(document.getElementById("Latitude").value)) {
      status[1] = '1';
    } 
    else {
      status[1] = '0';
    }

    if (isNumeric(document.getElementById("Longitude").value)) {
      status[2] = '1';
    } 
    else {
      status[2] = '0';
    }
    
    //disable links
    if ((status[1] == '1') && (status[2] == '1')) {
        googleLink.style.display = 'inline';
        linkSep.style.display = 'inline';
    }
    else {
        googleLink.style.display = 'none';
        linkSep.style.display = 'none';
    }
    
    for (var i = 0; i < status.length; i++) {
        if (status[i] == '0') {
            messagevalid = false;
            break;
        }
    }

    if (messagevalid) {
        statusElement.style.color = '#E1FFE1';
        statusElement.innerHTML = "Shown on Masonite.com";
    } 
    else {
        statusElement.style.color = '#FFEAEA';
        statusElement.innerHTML = "Not Shown on Masonite.com";
    }
}

function merchantChainChanged(chainId) {
    var classHomeDepotGlass = 'homedepotglass';
    var classLowesGlass = 'lowesglass';
    var classMoreOptions = 'moreoptions'
    var options = document.getElementById("dealeroptions").getElementsByTagName("tr");
    var canSee;

    if (navigator.appName.indexOf("Microsoft") > -1) {
        canSee = 'block';
    }
    else {
        canSee = 'table-row';
    }

    switch (chainId) {
        case 0:
            for (i = 0; i < options.length; i++) {
                if (options[i].className == classHomeDepotGlass) {
                    options[i].style.display = 'none';
                }
                if (options[i].className == classMoreOptions) {
                    options[i].style.display = canSee;
                }
                if (options[i].className == classLowesGlass) {
                    options[i].style.display = 'none';
                }
            }
            break;
        case 1:
            for (i = 0; i < options.length; i++) {
                if (options[i].className == classHomeDepotGlass) {
                    options[i].style.display = canSee;
                }
                if (options[i].className == classMoreOptions) {
                    options[i].style.display = 'none';
                }
                if (options[i].className == classLowesGlass) {
                    options[i].style.display = 'none';
                }
            }
            break;
        case 2:
            for (i = 0; i < options.length; i++) {
                if (options[i].className == classHomeDepotGlass) {
                    options[i].style.display = 'none';
                }
                if (options[i].className == classMoreOptions) {
                    options[i].style.display = 'none';
                }
                if (options[i].className == classLowesGlass) {
                    options[i].style.display = canSee;
                }
            }
            break;
        case 12:
            for (i = 0; i < options.length; i++) {
                if (options[i].className == classHomeDepotGlass) {
                    options[i].style.display = 'none';
                }
                if (options[i].className == classMoreOptions) {
                    options[i].style.display = 'none';
                }
                if (options[i].className == classLowesGlass) {
                    options[i].style.display = 'none';
                }
            }
            break;
    }
}

function postalCodeLookup(postalCode, addressType) {
    if (postalCode.length == 0) { return false; } // Exit, if this postalCode is empty.

    if (addressType == 'M') {
        $("#ImageWaitPostalCodeLookupMailing").show();
    }
    else {
        $("#ImageWaitPostalCodeLookup").show();
    }

    $.ajax({
        type: "POST",
        url: pathUtilityPostalCodeLookup + '?postalCode=' + postalCode
    }).done(function (data) {
        if (addressType == 'M') {
            if (data.IsValid == true) {
                var useZip = "defaultCityStateZip('" + data.City + "', '" + data.StateCode + "', '" + data.PostalCode + "', '" + data.Country + "', 'M');";
                $('#ZipMailing').html("<a href='javascript:void(0);' onclick=\"" + useZip + "\">Use " + data.City + ", " + data.StateName + " " + data.PostalCode + " (" + data.Country + ")</a>");
            }
            $("#ImageWaitPostalCodeLookupMailing").hide();
        }
        else {
            if (data.IsValid == true) {
                var useZip = "defaultCityStateZip('" + data.City + "', '" + data.StateId + "', '" + data.PostalCode + "', '" + data.Country + "', null);";
                $('#Zip').html("<a href='javascript:void(0);' onclick=\"" + useZip + "\">Use " + data.City + ", " + data.StateName + " " + data.PostalCode + " (" + data.Country + ")</a>");
            }
            $("#ImageWaitPostalCodeLookup").hide();
        }
    });
}

function defaultCityStateZip(city, stateId, postalCode, country, addressType) {
    if (addressType == 'M') {
        $('#MailingCity').val(city);
        $('#MailingState').val(stateId);
        $('#MailingPostalCode').val(postalCode);
        $('#MailingCountry').val(country);
        $('#MailingCountry').select();
    }
    else {
        $('#City').val(city);
        $('#StateId').val(stateId);
        $('#PostalCode').val(postalCode);
        $('#Country').val(country);
        $('#MailingAddress').select();
    }
}

function geoCode(address, city, state, zip) {
    if (address.length == 0) { return false; } // Exit, if address is empty.
    if (city.length == 0) { return false; } // Exit, if city is empty.
    if (state.length == 0) { return false; } // Exit, if state is empty.
    if (zip.length == 0) { return false; } // Exit, if zip is empty.

    $.ajax({
        type: "POST",
        url: pathUtilityGetGeoLocation + '?address=' + escape(address) + "&city=" + escape(city) + "&state=" + escape(state) + "&postalCode=" + escape(zip) + "&callback=?"
    }).done(function (data) {
        $("#Latitude").val(data.Latitude);
        $("#Longitude").val(data.Longitude);
        updateWebsiteStatus();
    });
}

function checkNameForDuplicates(name) {
    if (name.length == 0 || name.toLowerCase() == merchantInitialName.toLowerCase()) { return false; } // Exit, if this is an existing location and name hasn't changed, or if name is empty.

    $('#ImageWaitDupeCheck').show();
    $('#Dupe').html('');

    $.ajax({
        type: "POST",
        url: pathMerchantMerchantNameDuplicates + '?name=' + escape(name)
    }).done(function (data) {
        if (data.Id > 0) {
            $('#Dupe').html('<br/>This name is already in use <a href=\'' + pathMerchantEdit + data.Id + '\'>here</a>');
        }
        $('#ImageWaitDupeCheck').hide();
    });
}

function generateRandomPassword() {
    $('#ImageWaitGeneratePassword').show();

    $.ajax({
        type: "POST",
        url: pathUtilityGenerateRandomPassword
    }).done(function (data) {
        $('#LogonPassword').val(data);
        $('#ImageWaitGeneratePassword').hide();
    });
}

function defaultNumberTextbox(txt) {
    if (txt.value.length == 0) {
        txt.value = '[auto]';
    }
}

function transposeAddress() {
    $('#MailingAddress').val($('Address').val());
    $('#MailingAddress2').val($('Address2').val());
    $('#MailingPostalCode').val($('PostalCode').val());
    $('#MailingCity').val($('City').val());
    $('#MailingCountry').val($('Country').val());
    
    var statesDropDownList = document.getElementById('StateId');
    document.getElementById('MailingState').value = statesDropDownList.options[statesDropDownList.selectedIndex].text;
}