﻿var currentElement;
var currentElementBGColor = '';

// Highlights a table row.
function selectTableRow(me) {
    if (currentElement) {
        // Reset the previously selected row
        currentElement.style.backgroundColor = currentElementBGColor;
    }
    currentElementBGColor = me.style.backgroundColor;
    me.style.backgroundColor = '#EDEDDC';
    currentElement = me;
}

// Traps pressing of the [Enter] key.
function trapEnterPress(e) {
    var keynum;
    
    if (window.event) { 
        // IE
        keynum = e.keyCode;
    }
    else if (e.which) { 
        // Normal browsers
        keynum = e.which;
    }
    
    return keynum == 13;
}

// Prompts (confirms) to delete an item, and submits the request to the server if the user confirms.
function deleteItem(itemId, controller) {
    if (!window.confirm('This action will permanently delete the current item.  Continue?')) { return false; }

    $.ajax({
        type: "DELETE",
        url: controller + "/Delete/" + itemId
    }).done(function () {
        deleteCompleted()
    });
}

// Determines whether a given value is numeric
function isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}

// Places or removes checkmarks in every checkbox that resides within containerElement
function check(containerElement, doCheck) {
    if (containerElement) {
        checkBoxes = containerElement.getElementsByTagName('input');
        for (var i = 0; i < checkBoxes.length; i++) {
            checkBoxes[i].checked = doCheck;
        }
    }
}