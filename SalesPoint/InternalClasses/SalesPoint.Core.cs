﻿using System;

namespace SalesPoint
{
    /// <summary>
    /// Core methods, properties, and constants for the SalesPoint web application.
    /// </summary>
	public class Core
	{
        /// <summary>
        /// Represents the text that will be displayed in the user interface when editing a new object.
        /// </summary>
        public const string NewRecordText = " [New]";

        /// <summary>
        /// Represents the text that will appear at the beginning of any internal validation warning.
        /// </summary>
        public const string ValidationPrefix = "[Validation] ";

        /// <summary>
        /// Provides enumerated values that indicate which AdministrationLeft navigation menu item is currently selected.
        /// </summary>
        public enum SelectedNavigationAdministrationLeft
        {
            None = 0,

            /// <summary>
            /// The Settings & Status menu item.
            /// </summary>
            SettingsAndStatus = 1,

            /// <summary>
            /// The Merchant Chains menu item.
            /// </summary>
            MerchantChains = 2,

            /// <summary>
            /// The Merchant Categories menu item.
            /// </summary>
            MerchantCategories = 3,

            /// <summary>
            /// The Users menu item.
            /// </summary>
            Users = 4,

            /// <summary>
            /// The UserGroups menu item.
            /// </summary>
            UserGroups = 5,

            /// <summary>
            /// The Roles menu item.
            /// </summary>
            Roles = 6,

            /// <summary>
            /// The States menu item.
            /// </summary>
            States = 8,

            /// <summary>
            /// The Regions menu item.
            /// </summary>
            Regions = 9,

            /// <summary>
            /// The Divisions menu item.
            /// </summary>
            Divisions = 10,

            /// <summary>
            /// The Districts menu item.
            /// </summary>
            Districts = 11,

            /// <summary>
            /// The Product Lines menu item.
            /// </summary>
            ProductLines = 12,

            /// <summary>
            /// The Product Categories menu item.
            /// </summary>
            ProductCategories = 13,

            /// <summary>
            /// The Display Categories menu item.
            /// </summary>
            DisplayCategories = 14,

            /// <summary>
            /// The Display products menu item.
            /// </summary>
            DisplayProducts = 15,
            
            /// <summary>
            /// The ConfigureOne Price Books menu item.
            /// </summary>
            ConfigureOnePriceBooks = 16,

            /// <summary>
            /// The ConfigureOne Time Zones menu item.
            /// </summary>
            ConfigureOneTimeZones = 17,

            /// <summary>
            /// The ConfigureOne Product Lines menu item.
            /// </summary>
            ConfigureOneProductLines = 18,

            /// <summary>
            /// The Masonite Store Group menu item.
            /// </summary>
            MasoniteStoreGroups = 19,

            /// <summary>
            /// The StructureCategory menu item.
            /// </summary>
            StructureCategory = 20,

            /// <summary>
            /// The RegistrationCategory menu item.
            /// </summary>
            RegistrationCategory = 21
        }
	}
}
