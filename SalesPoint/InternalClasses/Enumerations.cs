﻿using System;

namespace SalesPoint
{
    /// <summary>
    /// Provides enumerated values that represent individual tabs that are displayed on the Merchant edit screen.
    /// </summary>
    public enum MerchantEditTab
    {
        /// <summary>
        /// The Details tab.
        /// </summary>
        Details = 1,

        /// <summary>
        /// The masonite.com tab.
        /// </summary>
        MasoniteDotCom = 2,
        
        /// <summary>
        /// The Display Products tab.
        /// </summary>
        DisplayProducts = 4,

        /// <summary>
        /// The UserGroup Membership tab.
        /// </summary>
        GroupMembership = 5,

        /// <summary>
        /// The Parent/Child tab.
        /// </summary>
        ParentChild = 6,

        /// <summary>
        /// The Contacts tab.
        /// </summary>
        Contacts = 7,

        /// <summary>
        /// The Marketing tab.
        /// </summary>
        Marketing = 8,

        /// <summary>
        /// The Configurator tab.
        /// </summary>
        Configurator = 9,

        /// <summary>
        /// The Notes tab.
        /// </summary>
        Notes = 11,

        /// <summary>
        /// The Distributors tab.
        /// </summary>
        Distributors = 12,

        /// <summary>
        /// The Dealers tab.
        /// </summary>
        Dealers = 13,

        /// <summary>
        /// The Audit Events tab.
        /// </summary>
        Audit = 14,

        /// <summary>
        /// The Structures tab.  (Actually, this currently only used for Model Homes)
        /// </summary>
        Structures = 15,

        /// <summary>
        /// The Kiosk tab.
        /// </summary>
        Kiosk = 16
    }

    /// <summary>
    /// Provides enumerated values that represent the available Merchant classifications.
    /// </summary>
    public enum MerchantClassification
    {
        /// <summary>
        /// No type provided.
        /// </summary>
        None = 0,

        /// <summary>
        /// The Merchant is a Home Center
        /// </summary>
        HomeCenter = 1,

        /// <summary>
        /// The Merchant is a 1-Step Distributor.
        /// </summary>
        Distributor1Step = 2,

        /// <summary>
        /// The Merchant is a 2-Step Distributor.
        /// </summary>
        Distributor2Step = 3,

        /// <summary>
        /// The Merchant is a Dealer.
        /// </summary>
        Dealer = 4,

        /// <summary>
        /// The Merchant is a Builder.
        /// </summary>
        Builder = 7,

        /// <summary>
        /// The Merchant is an Architect.
        /// </summary>
        Architect = 8,

        /// <summary>
        /// The Merchant is an (Other).
        /// </summary>
        Other = 9,

        /// <summary>
        /// The Merchant is a Remodeler.
        /// </summary>
        Remodeler = 10
    }

    /// <summary>
    /// Provides enumerated values that represent the available Structure classifications.
    /// </summary>
    public enum StructureClassification
    {
        /// <summary>
        /// No type provided.
        /// </summary>
        None = 0,

        /// <summary>
        /// The Structure is a Model Home.
        /// </summary>
        ModelHome = 1
    }

    /// <summary>
    /// Provides enumerated values that represent the types of reports that users can generate within the system.
    /// </summary>
    public enum Report
    {
        /// <summary>
        /// The admin merchant duplicate report.
        /// </summary>
        AdminMerchantDuplicate = 1,

        /// <summary>
        /// The admin merchant not geo-coded report.
        /// </summary>
        AdminMerchantNotGeocoded = 2,

        /// <summary>
        /// The marketing mailing report.
        /// </summary>
        MarketingMailing = 3,

        /// <summary>
        /// The Security Users That Can Access Configurator report.
        /// </summary>
        SecurityUsersThatCanAccessConfigurator = 4,

        /// <summary>
        /// The Security Users That Can Access Dealer Logon report.
        /// </summary>
        SecurityUsersThatCanAccessDealerLogon = 5
    }

    /// <summary>
    /// Provides enumerated values that represent the file formats available for system reports.
    /// </summary>
    public enum ReportFormat
    {
        /// <summary>
        /// The PDF file format.
        /// </summary>
        Pdf = 1,

        /// <summary>
        /// The Excel worksheet format.
        /// </summary>
        Excel = 2
    }
}
