﻿using System;
using System.Web;
using System.Collections.Generic;
using System.Threading;

namespace SalesPoint.Models
{
    /// <summary>
    /// Provides static methods for generating collections of marketing objects.
    /// </summary>
    public static class Marketing
    {
        /// <summary>
        /// Returns a collection of MarketingRegionData objects.
        /// </summary>
        /// <returns>A Generic List of MarketingRegionData objects.</returns>
        public static List<MarketingRegionData> GetMarketingRegionCollection()
        {
            return new List<MarketingRegionData>
            {
                new MarketingRegionData
                {
                    Name = "US"
                },
                new MarketingRegionData
                {
                    Name = "Eastern Canada"
                },
                new MarketingRegionData
                {
                    Name = "Western Canada"
                }
            };
        }
    }

    /// <summary>
    /// Represents an object used to generate a marketing region.
    /// </summary>
    public class MarketingRegionData
    {
        /// <summary>
        /// Gets or sets the region name.
        /// </summary>
        public string Name { get; set; }
    }
}
