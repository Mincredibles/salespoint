﻿using System;
using System.Web;
using System.Collections.Generic;
using System.Threading;

namespace SalesPoint.Models
{
    /// <summary>
    /// Provides static methods for generating collections of objects used to populate a Telerik TabStrip control.
    /// </summary>
    public static class Navigation
    {
        /// <summary>
        /// Returns a collection of NavigationData objects which are used in generating an instance of the Telerik TabStrip control.
        /// </summary>
        /// <param name="tabIndex">The index of the tab that is to be selected.</param>
        /// <param name="merchantId">The ID of the Merchant that is currently being viewed.</param>
        /// <returns>A Generic List of NavigationData objects.</returns>
        public static List<NavigationData> GetMerchantTabCollection(int tabIndex, int merchantId)
        {
            MerchantEditTab tab = (MerchantEditTab)tabIndex;
            MerchantClassification category = MerchantClassification.None;

            using (Masonite.MTier.SalesPoint.Merchant merchant = Masonite.MTier.SalesPoint.ObjectFactory.RetrieveMerchantForAdministration(merchantId))
            {
                if (merchant != null)
                {
                    category = (MerchantClassification)merchant.CategoryId;
                }
            }

            return new List<NavigationData>
            {
                new NavigationData
                {
                    Text = "Details",
                    ActionName = "EditPartial",
                    ControllerName = "Merchant",
                    RouteValues = new { id = merchantId, tab = tab },
                    PageUrl = GenerateMerchantEditUrl(MerchantEditTab.Details, merchantId),
                    IsContentLoadable = tab == MerchantEditTab.Details ? true : false,
                    IsSelected = tab == MerchantEditTab.Details ? true : false,
                    IsVisible = true
                },
                new NavigationData
                {
                    Text = "Masonite.com",
                    ActionName = "EditPartialMasoniteDotCom",
                    ControllerName = "Merchant",
                    RouteValues = new { merchantId = merchantId, tab = tab },
                    PageUrl = GenerateMerchantEditUrl(MerchantEditTab.MasoniteDotCom, merchantId),
                    IsContentLoadable = tab == MerchantEditTab.MasoniteDotCom ? true : false,
                    IsSelected = tab == MerchantEditTab.MasoniteDotCom ? true : false,
                    IsVisible = merchantId > 0
                },
                new NavigationData
                {
                    Text = "Distributors",
                    ActionName = "IndexPartial",
                    ControllerName = "MerchantDistributorDealer",
                    RouteValues = new { merchantId = merchantId, tab = tab },
                    PageUrl = GenerateMerchantEditUrl(MerchantEditTab.Distributors, merchantId),
                    IsContentLoadable = tab == MerchantEditTab.Distributors ? true : false,
                    IsSelected = tab == MerchantEditTab.Distributors ? true : false,
                    IsVisible = 
                                (Thread.CurrentPrincipal.IsInRole("SalesPoint Administrators") || Thread.CurrentPrincipal.IsInRole("Max Team"))
                                && merchantId > 0
                },
                new NavigationData
                {
                    Text = "Dealers",
                    ActionName = "IndexPartial",
                    ControllerName = "MerchantDealerTradeProfessional",
                    RouteValues = new { merchantId = merchantId, tab = tab },
                    PageUrl = GenerateMerchantEditUrl(MerchantEditTab.Dealers, merchantId),
                    IsContentLoadable = tab == MerchantEditTab.Dealers ? true : false,
                    IsSelected = tab == MerchantEditTab.Dealers ? true : false,
                    IsVisible = 
                                (Thread.CurrentPrincipal.IsInRole("SalesPoint Administrators") || Thread.CurrentPrincipal.IsInRole("Max Team"))
                                && merchantId > 0
                                && category != MerchantClassification.Dealer
                                && category != MerchantClassification.Distributor1Step
                                && category != MerchantClassification.Distributor2Step
                                && category != MerchantClassification.HomeCenter
                },
                new NavigationData
                {
                    Text = "Model Homes",
                    ActionName = "IndexPartial",
                    ControllerName = "Structure",
                    RouteValues = new { merchantId = merchantId, tab = tab },
                    PageUrl = GenerateMerchantEditUrl(MerchantEditTab.Structures, merchantId),
                    IsContentLoadable = tab == MerchantEditTab.Structures ? true : false,
                    IsSelected = tab == MerchantEditTab.Structures ? true : false,
                    IsVisible = merchantId > 0 && category == MerchantClassification.Builder
                },
                //new NavigationData
                //{
                //    Text = "Display Products",
                //    ActionName = "IndexPartial",
                //    ControllerName = "MerchantDisplayProduct",
                //    RouteValues = new { merchantId = merchantId, tab = tab },
                //    PageUrl = GenerateMerchantEditUrl(MerchantEditTab.DisplayProducts, merchantId),
                //    IsContentLoadable = tab == MerchantEditTab.DisplayProducts ? true : false,
                //    IsSelected = tab == MerchantEditTab.DisplayProducts ? true : false,
                //    IsVisible = merchantId > 0
                //},
                new NavigationData
                {
                    Text = "Group Membership",
                    ActionName = "IndexUserGroupPartial",
                    ControllerName = "UserGroupMerchant",
                    RouteValues = new { merchantId = merchantId, tab = tab },
                    PageUrl = GenerateMerchantEditUrl(MerchantEditTab.GroupMembership, merchantId),
                    IsContentLoadable = tab == MerchantEditTab.GroupMembership ? true : false,
                    IsSelected = tab == MerchantEditTab.GroupMembership ? true : false,
                    IsVisible = Thread.CurrentPrincipal.IsInRole("SalesPoint Administrators") && merchantId > 0
                },
                new NavigationData
                {
                    Text = "Order Visibility",
                    ActionName = "IndexPartial",
                    ControllerName = "MerchantMerchant",
                    RouteValues = new { merchantId = merchantId, tab = tab },
                    PageUrl = GenerateMerchantEditUrl(MerchantEditTab.ParentChild, merchantId),
                    IsContentLoadable = tab == MerchantEditTab.ParentChild ? true : false,
                    IsSelected = tab == MerchantEditTab.ParentChild ? true : false,
                    IsVisible = merchantId > 0
                },
                new NavigationData
                {
                    Text = "Contacts",
                    ActionName = "IndexPartial",
                    ControllerName = "MerchantContact",
                    RouteValues = new { merchantId = merchantId, tab = tab },
                    PageUrl = GenerateMerchantEditUrl(MerchantEditTab.Contacts, merchantId),
                    IsContentLoadable = tab == MerchantEditTab.Contacts ? true : false,
                    IsSelected = tab == MerchantEditTab.Contacts ? true : false,
                    IsVisible = merchantId > 0
                },
                new NavigationData
                {
                    Text = "Marketing",
                    ActionName = "EditPartial",
                    ControllerName = "MerchantMarketing",
                    RouteValues = new { merchantId = merchantId, tab = tab },
                    PageUrl = GenerateMerchantEditUrl(MerchantEditTab.Marketing, merchantId),
                    IsContentLoadable = tab == MerchantEditTab.Marketing ? true : false,
                    IsSelected = tab == MerchantEditTab.Marketing ? true : false,
                    IsVisible = merchantId > 0
                },
                new NavigationData
                {
                    Text = "Configurator",
                    ActionName = "EditPartial",
                    ControllerName = "MerchantConfigureOne",
                    RouteValues = new { merchantId = merchantId, tab = tab },
                    PageUrl = GenerateMerchantEditUrl(MerchantEditTab.Configurator, merchantId),
                    IsContentLoadable = tab == MerchantEditTab.Configurator ? true : false,
                    IsSelected = tab == MerchantEditTab.Configurator ? true : false,
                    IsVisible = (Thread.CurrentPrincipal.IsInRole("SalesPoint Administrators") || Thread.CurrentPrincipal.IsInRole("Wholesale Administrators")) && merchantId > 0
                },
                new NavigationData
                {
                    Text = "Notes",
                    ActionName = "IndexPartial",
                    ControllerName = "MerchantNote",
                    RouteValues = new { merchantId = merchantId, tab = tab },
                    PageUrl = GenerateMerchantEditUrl(MerchantEditTab.Notes, merchantId),
                    IsContentLoadable = tab == MerchantEditTab.Notes ? true : false,
                    IsSelected = tab == MerchantEditTab.Notes ? true : false,
                    IsVisible = merchantId > 0
                },
                new NavigationData
                {
                    Text = "Audit Events",
                    ActionName = "IndexPartial",
                    ControllerName = "Audit",
                    RouteValues = new { merchantId = merchantId, tab = tab },
                    PageUrl = GenerateMerchantEditUrl(MerchantEditTab.Audit, merchantId),
                    IsContentLoadable = tab == MerchantEditTab.Audit ? true : false,
                    IsSelected = tab == MerchantEditTab.Audit ? true : false,
                    IsVisible = Thread.CurrentPrincipal.IsInRole("SalesPoint Administrators") && merchantId > 0
                },
                new NavigationData
                {
                    Text = "Kiosk",
                    ActionName = "EditPartial",
                    ControllerName = "MerchantKiosk",
                    RouteValues = new { merchantId = merchantId, tab = tab },
                    PageUrl = GenerateMerchantEditUrl(MerchantEditTab.Kiosk, merchantId),
                    IsContentLoadable = tab == MerchantEditTab.Kiosk ? true : false,
                    IsSelected = tab == MerchantEditTab.Kiosk ? true : false,
                    IsVisible = Thread.CurrentPrincipal.IsInRole("SalesPoint Administrators") && merchantId > 0
                }
            };
        }

        /// <summary>
        /// Internal helper method used to generate a URL to a page corresponding to the given tab.
        /// </summary>
        /// <param name="tab">The Merchant tab to link to.</param>
        /// <param name="merchantId">The ID of the Merchant that is currently being viewed.</param>
        /// <returns>A string object.</returns>
        private static string GenerateMerchantEditUrl(MerchantEditTab tab, int merchantId)
        {
            return string.Format(
                                System.Globalization.CultureInfo.CurrentUICulture,
                                VirtualPathUtility.ToAbsolute("~/Merchant/Edit/") + "{0}?tab={1}",
                                merchantId,
                                (int)tab
                                );
        }
    }

    /// <summary>
    /// Represents an object used to generate a single Telerik TabStrip tab.
    /// </summary>
    public class NavigationData
    {
        /// <summary>
        /// Gets or sets the tab text.
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        /// Gets or sets a value that indicates whether the tab should be displayed in the browser window.
        /// </summary>
        public bool IsVisible { get; set; }

        /// <summary>
        /// Gets or sets a value that indicates whether the tab is selected.
        /// </summary>
        public bool IsSelected { get; set; }

        /// <summary>
        /// Gets or sets a value that indicates whether the tab contents should be loaded when the control is rendered in the browser.
        /// </summary>
        public bool IsContentLoadable { get; set; }

        /// <summary>
        /// Gets or sets the name of the action method to be invoked when IsContentLoadable is true.
        /// </summary>
        public string ActionName { get; set; }

        /// <summary>
        /// Gets or sets the name of the controller that contains the action method indicated by ActionName.
        /// </summary>
        public string ControllerName { get; set; }

        /// <summary>
        /// Gets or sets the object that contains the parameters for the route.
        /// </summary>
        public object RouteValues { get; set; }

        /// <summary>
        /// Gets or sets the URL of the page that the tab will link to.  This property is ignored when IsContentLoadable is true.
        /// </summary>
        public string PageUrl { get; set; }
    }
}
