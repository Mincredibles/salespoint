﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Masonite.MTier.Utility;
using Masonite.MTier.SalesPoint;

namespace SalesPoint.Controllers
{
    [Authorize(Roles = "SalesPoint Administrators")]
    public class MerchantKioskController : Controller
    {
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult EditPartial(int merchantId)
        {
            return PartialView("EditPartial", ObjectFactory.RetrieveMerchant(merchantId, User.Identity.Name));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Edit(Merchant model)
        {
            //if (ModelState.IsValid)
            {
                Merchant item = ObjectFactory.RetrieveMerchant(model.Id, User.Identity.Name);

                // Populate
                item.KioskEmail = model.KioskEmail;
                item.KioskName = model.KioskName;
                item.KioskNumber = model.KioskNumber;
                item.KioskUrl = model.KioskUrl;

                // Save
                try
                {
                    item.Save(User.Identity.Name, true);
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(string.Empty, ex.Message);
                }
            }

            return PartialView("EditPartial", ObjectFactory.RetrieveMerchant(model.Id, User.Identity.Name));
        }
    }
}