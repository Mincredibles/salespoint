﻿using System;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Masonite.MTier.Configurator;

namespace SalesPoint.Controllers
{
    [Authorize(Roles = "SalesPoint Administrators, Max Price Book Publishers")]
    public class ConfiguratorController : Controller
    {
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult ExecutePriceBookPublishPreStep()
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            return this.Json(serializer.Serialize(HelperFunctions.PublishPriceBookPreStep()));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult ExecutePriceBookPublishPostStep()
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            return this.Json(serializer.Serialize(HelperFunctions.PublishPriceBookPostStep()));
        }
    }
}