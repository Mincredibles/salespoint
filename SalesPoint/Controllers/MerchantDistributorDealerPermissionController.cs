﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Masonite.MTier.Utility;
using Masonite.MTier.SalesPoint;

namespace SalesPoint.Controllers
{
    [Authorize(Roles = "SalesPoint Administrators, Max Team")]
    public class MerchantDistributorDealerPermissionController : Controller
    {
        [ChildActionOnly]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult IndexPartial(string workgroupId)
        {
            ViewData["WorkgroupId"] = workgroupId;
            return PartialView(ObjectFactory.RetriveDefaultDealerPermission(workgroupId.ToInt32(), true));
        }
    }
}
