﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Masonite.MTier.Utility;
using Masonite.MTier.SalesPoint;

namespace SalesPoint.Controllers
{
    public class CorrespondenceController : Controller
    {
        [Authorize(Roles = "SalesPoint Administrators, Marketing Team, Correspondence Approvers")]
        [AcceptVerbs(new string[] { "Get", "Post" })]
        public ActionResult Index(string searchName)
        {
            searchName = Server.UrlDecode(searchName);
            ViewData["searchName"] = searchName;

            SalesPoint.ViewModels.CorrespondenceIndexModel model = new ViewModels.CorrespondenceIndexModel();
            model.Correspondences = ObjectFactory.RetrieveCorrespondences(searchName);
            model.CorrespondenceApprovals = ObjectFactory.RetrieveCorrespondenceApprovals(User.Identity.Name);

            return View(model);
        }

        [Authorize(Roles = "SalesPoint Administrators, Marketing Team")]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Edit(int? id)
        {
            if (!id.HasValue || id == 0)
            {
                return View(new Correspondence());
            }
            else
            {
                // Editing existing item
                return View(ObjectFactory.RetrieveCorrespondence((int)id));
            }
        }

        [Authorize(Roles = "SalesPoint Administrators, Marketing Team")]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Edit(int? id, FormCollection formCollection)
        {
            if (ModelState.IsValid)
            {
                // Create or retrieve item
                Correspondence item;
                if (id > 0)
                {
                    item = ObjectFactory.RetrieveCorrespondence((int)(id.HasValue ? id : 0));
                }
                else
                {
                    item = new Correspondence();
                }

                // Populate
                item.Description = formCollection["Description"];
                item.Name = formCollection["Name"];
                item.RecipientListSqlStatement = formCollection["RecipientListSqlStatement"];
                item.ScheduledDeliveryDate = formCollection["ScheduledDeliveryDate"].ToDate();

                // Save and redirect
                if (item.Save(User.Identity.Name))
                {
                    return RedirectToAction("Index");
                }
            }

            // Failed
            return View();
        }
    }
}