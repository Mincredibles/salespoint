﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SalesPoint.Controllers
{
    public partial class TabStripController : Controller
    {
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult IndexMerchant(int? tab, int merchantId)
        {
            return PartialView("IndexMerchant", SalesPoint.Models.Navigation.GetMerchantTabCollection((tab.HasValue ? (int)tab : 1), merchantId));
        }
    }
}