﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Masonite.MTier.Utility;
using Masonite.MTier.SalesPoint;

namespace SalesPoint.Controllers
{
    [Authorize(Roles = "SalesPoint Administrators, Max Team")]
    public class MerchantDealerTradeProfessionalController : Controller
    {
        [AcceptVerbs(new string[] { "Get", "Post" })]
        public ActionResult ChooseDealerPartial(string searchName)
        {
            // Note that this method returns dealers only
            System.Collections.Generic.List<Merchant> merchants = ObjectFactory.RetrieveMerchantsForAdministration(searchName, null, 0, null, null, 0, (int)SalesPoint.MerchantClassification.Dealer, false, false, false);
            return PartialView("ChooseDealerPartial", merchants);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult IndexPartial(int merchantId)
        {
            ViewData["MerchantId"] = merchantId;

            using (Merchant merchant = ObjectFactory.RetrieveMerchant(merchantId, User.Identity.Name))
            {
                ViewData["MerchantName"] = merchant.Name.Replace("'", "\\'");
            }

            return PartialView("IndexPartial", ObjectFactory.RetrieveMerchantDealerTradeProfessionals(merchantId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public void EditDealerPartial(int dealerId, int tradeProfessionalId)
        {
            // Create or retrieve item
            MerchantDealerTradeProfessional merchantDealerTradeProfessional = new MerchantDealerTradeProfessional();

            // Populate
            merchantDealerTradeProfessional.MerchantDealerId = dealerId;
            merchantDealerTradeProfessional.MerchantTradeProfessionalId = tradeProfessionalId;

            // Save
            merchantDealerTradeProfessional.Save(User.Identity.Name);
        }
        
        [AcceptVerbs(HttpVerbs.Delete)]
        public void Delete(int id)
        {
            // Delete item
            MerchantDealerTradeProfessional merchantDealerTradeProfessional = ObjectFactory.RetrieveMerchantDealerTradeProfessional(id);
            merchantDealerTradeProfessional.Remove(User.Identity.Name);
        }
    }
}
