﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Masonite.MTier.SalesPoint;

namespace SalesPoint.Controllers
{
    [Authorize(Roles = "SalesPoint Administrators")]
    public class MerchantCategoryController : Controller
    {
        [AcceptVerbs(new string[] { "Get", "Post" })]
        public ActionResult Index(string searchName)
        {
            searchName = Server.UrlDecode(searchName);
            ViewData["searchName"] = searchName;
            return View(ObjectFactory.RetrieveMerchantCategories(searchName));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Edit(int? id)
        {
            if (!id.HasValue || id == 0)
            {
                // Adding new item
                ViewData["NewRecordText"] = Core.NewRecordText;
                return View(new MerchantCategory());
            }
            else
            {
                // Editing existing item
                return View(ObjectFactory.RetrieveMerchantCategory((int)id));
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Edit(int? id, FormCollection formCollection)
        {
            if (ModelState.IsValid)
            {
                // Create or retrieve item
                MerchantCategory merchantCategory;
                if (id > 0)
                {
                    merchantCategory = ObjectFactory.RetrieveMerchantCategory((int)(id.HasValue ? id : 0));
                }
                else
                {
                    merchantCategory = new MerchantCategory();
                }

                // Populate
                merchantCategory.Code = formCollection["Code"];
                merchantCategory.Description = formCollection["Description"];
                merchantCategory.IsActive = formCollection["IsActive"].Contains("true");
                merchantCategory.Name = formCollection["Name"];

                // Save and redirect
                if (merchantCategory.Save())
                {
                    return RedirectToAction("Index", "MerchantCategory", new { searchName = Server.UrlEncode(merchantCategory.Name) });
                }
            }

            // Failed
            return View();
        }

        [AcceptVerbs(HttpVerbs.Delete)]
        public void Delete(int id)
        {
            // Delete item
            MerchantCategory merchantCategory = ObjectFactory.RetrieveMerchantCategory(id);
            merchantCategory.Remove();
        }
    }
}