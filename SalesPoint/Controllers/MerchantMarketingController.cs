﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Masonite.MTier.Utility;
using Masonite.MTier.SalesPoint;

namespace SalesPoint.Controllers
{
    public class MerchantMarketingController : Controller
    {
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult EditPartial(int merchantId)
        {
            ViewData["MerchantId"] = merchantId;

            using (Merchant merchant = ObjectFactory.RetrieveMerchant(merchantId, User.Identity.Name))
            {
                ViewData["MerchantName"] = merchant.Name.Replace("'", "\\'");
            }

            return PartialView("EditPartial", ObjectFactory.RetrieveMerchantMarketing(merchantId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Edit(int merchantId, FormCollection formCollection)
        {
            MerchantMarketing merchantMarketing = new MerchantMarketing();
            
            // Populate
            merchantMarketing.IsCarrierOfEntryDoors = formCollection["IsCarrierOfEntryDoors"].Contains("true");
            merchantMarketing.IsCarrierOfInteriorMoldedDoors = formCollection["IsCarrierOfInteriorMoldedDoors"].Contains("true");
            merchantMarketing.IsCarrierOfInteriorStileAndRailDoors = formCollection["IsCarrierOfInteriorStileAndRailDoors"].Contains("true");
            merchantMarketing.IsCarrierOfSpecialtyGlass = formCollection["IsCarrierOfSpecialtyGlass"].Contains("true");
            merchantMarketing.IsNationalAccount = formCollection["IsNationalAccount"].Contains("true");
            merchantMarketing.IsPromotionParticipationAccessibleToSalesReps = formCollection["IsPromotionParticipationAccessibleToSalesReps"].Contains("true");
            merchantMarketing.MerchantId = merchantId;
            merchantMarketing.Region = formCollection["Region"];

            // Save
            merchantMarketing.Save();

            // Capture MerchantContactMarketing checked values
            List<MerchantContact> contacts = ObjectFactory.RetrieveMerchantContacts(merchantId);
            string[] IsFrenchSpeaking = { };
            string[] IsReceivingPriceIncreasesExteriorDecorativeGlass = { };
            string[] IsReceivingPriceIncreasesEntryDoor = { };
            string[] IsReceivingPriceIncreasesInteriorMoldedDoor = { };
            string[] IsReceivingPriceIncreasesStileAndRail = { };
            string[] IsReceivingProductBulletinsExteriorDecorativeGlass = { };
            string[] IsReceivingProductBulletinsEntryDoor = { };
            string[] IsReceivingProductBulletinsInteriorMoldedDoor = { };
            string[] IsReceivingProductBulletinsStileAndRail = { };
            string[] IsReceivingEmailMarketing = { };
            string[] IsReceivingNewsletter = { };

            if (formCollection["IsFrenchSpeaking"] != null) { IsFrenchSpeaking = formCollection["IsFrenchSpeaking"].Split(char.Parse(",")); }
            if (formCollection["IsReceivingPriceIncreasesExteriorDecorativeGlass"] != null) { IsReceivingPriceIncreasesExteriorDecorativeGlass = formCollection["IsReceivingPriceIncreasesExteriorDecorativeGlass"].Split(char.Parse(",")); }
            if (formCollection["IsReceivingPriceIncreasesEntryDoor"] != null) { IsReceivingPriceIncreasesEntryDoor = formCollection["IsReceivingPriceIncreasesEntryDoor"].Split(char.Parse(",")); }
            if (formCollection["IsReceivingPriceIncreasesInteriorMoldedDoor"] != null) { IsReceivingPriceIncreasesInteriorMoldedDoor = formCollection["IsReceivingPriceIncreasesInteriorMoldedDoor"].Split(char.Parse(",")); }
            if (formCollection["IsReceivingPriceIncreasesStileAndRail"] != null) { IsReceivingPriceIncreasesStileAndRail = formCollection["IsReceivingPriceIncreasesStileAndRail"].Split(char.Parse(",")); }
            if (formCollection["IsReceivingProductBulletinsExteriorDecorativeGlass"] != null) { IsReceivingProductBulletinsExteriorDecorativeGlass = formCollection["IsReceivingProductBulletinsExteriorDecorativeGlass"].Split(char.Parse(",")); }
            if (formCollection["IsReceivingProductBulletinsEntryDoor"] != null) { IsReceivingProductBulletinsEntryDoor = formCollection["IsReceivingProductBulletinsEntryDoor"].Split(char.Parse(",")); }
            if (formCollection["IsReceivingProductBulletinsInteriorMoldedDoor"] != null) { IsReceivingProductBulletinsInteriorMoldedDoor = formCollection["IsReceivingProductBulletinsInteriorMoldedDoor"].Split(char.Parse(",")); }
            if (formCollection["IsReceivingProductBulletinsStileAndRail"] != null) { IsReceivingProductBulletinsStileAndRail = formCollection["IsReceivingProductBulletinsStileAndRail"].Split(char.Parse(",")); }
            if (formCollection["IsReceivingEmailMarketing"] != null) { IsReceivingEmailMarketing = formCollection["IsReceivingEmailMarketing"].Split(char.Parse(",")); }
            if (formCollection["IsReceivingNewsletter"] != null) { IsReceivingNewsletter = formCollection["IsReceivingNewsletter"].Split(char.Parse(",")); }

            foreach (MerchantContact contact in contacts)
            {
                string id = contact.Id.ToString(System.Globalization.CultureInfo.CurrentUICulture);

                contact.MarketingAttributes.IsFrenchSpeaking = IsFrenchSpeaking.Contains(id);
                contact.MarketingAttributes.IsReceivingPriceIncreasesExteriorDecorativeGlass = IsReceivingPriceIncreasesExteriorDecorativeGlass.Contains(id);
                contact.MarketingAttributes.IsReceivingPriceIncreasesEntryDoor = IsReceivingPriceIncreasesEntryDoor.Contains(id);
                contact.MarketingAttributes.IsReceivingPriceIncreasesInteriorMoldedDoor = IsReceivingPriceIncreasesInteriorMoldedDoor.Contains(id);
                contact.MarketingAttributes.IsReceivingPriceIncreasesStileAndRail = IsReceivingPriceIncreasesStileAndRail.Contains(id);
                contact.MarketingAttributes.IsReceivingProductBulletinsExteriorDecorativeGlass = IsReceivingProductBulletinsExteriorDecorativeGlass.Contains(id);
                contact.MarketingAttributes.IsReceivingProductBulletinsEntryDoor = IsReceivingProductBulletinsEntryDoor.Contains(id);
                contact.MarketingAttributes.IsReceivingProductBulletinsInteriorMoldedDoor = IsReceivingProductBulletinsInteriorMoldedDoor.Contains(id);
                contact.MarketingAttributes.IsReceivingProductBulletinsStileAndRail = IsReceivingProductBulletinsStileAndRail.Contains(id);
                contact.MarketingAttributes.IsReceivingEmailMarketing = IsReceivingEmailMarketing.Contains(id);
                contact.MarketingAttributes.IsReceivingNewsletter = IsReceivingNewsletter.Contains(id);

                contact.MarketingAttributes.Save(User.Identity.Name);
            }

            // Return
            return RedirectToAction("Edit", "Merchant", new { id = merchantId, tab = (int)MerchantEditTab.Marketing });
        }
    }
}