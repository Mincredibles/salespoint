﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Masonite.MTier.Utility;
using Masonite.MTier.SalesPoint;

namespace SalesPoint.Controllers
{
    public class MerchantContactConfigureOneController : Controller
    {
        [AcceptVerbs(HttpVerbs.Get)]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult IndexPartial(int merchantContactId)
        {
            ViewData["merchantContactId"] = merchantContactId;
            return PartialView(ObjectFactory.RetrieveMerchantContactConfigureOnes(merchantContactId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public void IndexEdit(int merchantContactId, FormCollection formCollection)
        {
            List<MerchantContactConfigureOne> merchantContactConfigureOnes = ObjectFactory.RetrieveMerchantContactConfigureOnes(merchantContactId);

            string[] IsReceivingCartEmail = { };

            if (formCollection["IsReceivingCartEmail"] != null) { IsReceivingCartEmail = formCollection["IsReceivingCartEmail"].Split(char.Parse(",")); }

            foreach (MerchantContactConfigureOne item in merchantContactConfigureOnes)
            {
                string id = item.Id.ToString(System.Globalization.CultureInfo.CurrentUICulture);

                item.IsReceivingCartEmail = IsReceivingCartEmail.Contains(id);

                item.Save(User.Identity.Name);

                string[] maxPermissions = { };
                if (formCollection["MaxPermission-" + item.ConfigureOneUserId] != null)
                    maxPermissions = formCollection["MaxPermission-" + item.ConfigureOneUserId].Split(',');

                var allPermissions = ObjectFactory.RetriveMerchantContactPermission(item.ConfigureOneUserId.ToInt32());
                foreach (var permission in allPermissions)
                {
                    if (maxPermissions.Contains(permission.PermissionId.ToString()))
                    {
                        if (permission.UserId == 0)
                            ObjectFactory.UpdateConfigureOnePermission(item.ConfigureOneUserId.ToInt32(), permission.PermissionId);
                    }
                    else
                    {
                        if (permission.UserId != 0)
                            ObjectFactory.DeleteConfigureOnePermission(item.ConfigureOneUserId.ToInt32(), permission.PermissionId);
                    }
                }
            }
        }
    }
}