﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Masonite.MTier.SalesPoint;

namespace SalesPoint.Controllers
{
    public class UserMerchantMruController : Controller
    {
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Index(int userId)
        {
            return View(ObjectFactory.RetrieveUserMerchantMru(userId));
        }
    }
}
