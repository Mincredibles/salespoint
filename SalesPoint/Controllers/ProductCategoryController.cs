﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Masonite.MTier.Utility;
using Masonite.MTier.SalesPoint;

namespace SalesPoint.Controllers
{
    [Authorize(Roles = "SalesPoint Administrators")]
    public class ProductCategoryController : Controller
    {
        [AcceptVerbs(new string[] { "Get", "Post" })]
        public ActionResult Index(string searchName)
        {
            searchName = Server.UrlDecode(searchName);
            ViewData["searchName"] = searchName;
            return View(ObjectFactory.RetrieveProductCategories(searchName));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Edit(int? id)
        {
            if (!id.HasValue || id == 0)
            {
                // Adding new item
                ViewData["NewRecordText"] = Core.NewRecordText;
                return View(new ProductCategory());
            }
            else
            {
                // Editing existing item
                return View(ObjectFactory.RetrieveProductCategory((int)id));
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Edit(int? id, FormCollection formCollection)
        {
            if (ModelState.IsValid)
            {
                // Create or retrieve item
                ProductCategory productCategory;
                if (id > 0)
                {
                    productCategory = ObjectFactory.RetrieveProductCategory((int)(id.HasValue ? id : 0));
                }
                else
                {
                    productCategory = new ProductCategory();
                }

                // Populate
                productCategory.Code = formCollection["Code"];
                productCategory.Description = formCollection["Description"];
                productCategory.IsActive = formCollection["IsActive"].Contains("true");
                productCategory.Name = formCollection["Name"];

                // Save and redirect
                if (productCategory.Save())
                {
                    return RedirectToAction("Index", "ProductCategory", new { searchName = Server.UrlEncode(productCategory.Name) });
                }
            }

            // Failed
            return View();
        }

        [AcceptVerbs(HttpVerbs.Delete)]
        public void Delete(int id)
        {
            // Delete item
            ProductCategory productCategory = ObjectFactory.RetrieveProductCategory(id);
            productCategory.Remove();
        }
    }
}