﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Masonite.MTier.Utility;
using Masonite.MTier.SalesPoint;

namespace SalesPoint.Controllers
{
    public class MerchantMerchantController : Controller
    {
        [AcceptVerbs(new string[] { "Get", "Post" })]
        public ActionResult ChooseRelationshipPartial(string searchName, string editing)
        {
            List<Merchant> merchants;
            
            if (string.IsNullOrEmpty(searchName))
            {
                merchants = new List<Merchant>();
            }
            else
            {
                merchants = ObjectFactory.RetrieveMerchantsForAdministration(searchName, null, 0, null, null, 0, 0, true, false, false);
            }
            return PartialView("ChooseRelationshipPartial", merchants);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult IndexPartial(int merchantId)
        {
            ViewData["MerchantId"] = merchantId;

            using (Merchant merchant = ObjectFactory.RetrieveMerchant(merchantId, User.Identity.Name))
            {
                ViewData["MerchantName"] = merchant.Name.Replace("'", "\\'");
            }

            return PartialView("IndexPartial", new Merchant());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public void Edit(int parentMerchantId, int childMerchantId)
        {
            // Create or retrieve item
            MerchantMerchant merchantMerchant = new MerchantMerchant();

            // Populate
            merchantMerchant.ParentMerchantId = parentMerchantId;
            merchantMerchant.ChildMerchantId = childMerchantId;

            // Save
            merchantMerchant.Save();
        }

        [AcceptVerbs(HttpVerbs.Delete)]
        public void Delete(int parentMerchantId, int childMerchantId)
        {
            // Delete item
            MerchantMerchant merchantMerchant = ObjectFactory.RetrieveMerchantMerchant(parentMerchantId, childMerchantId);
            merchantMerchant.Remove();
        }
    }
}