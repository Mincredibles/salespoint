﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Masonite.MTier.Utility;
using Masonite.MTier.SalesPoint;

namespace SalesPoint.Controllers
{
    public class RegistrationCategoryController : Controller
    {
        [AcceptVerbs(new string[] { "Get", "Post" })]
        public ActionResult Index(string searchName)
        {
            searchName = Server.UrlDecode(searchName);
            ViewData["searchName"] = searchName;
            return View(ObjectFactory.RetrieveRegistrationCategories(searchName));
        }

        [ChildActionOnly()]
        [AcceptVerbs(new string[] { "Get", "Post" })]
        public ActionResult IndexPartial(int? selectedValue)
        {
            ViewData["SelectedValue"] = selectedValue.HasValue ? (int)selectedValue : 0;

            return View(ObjectFactory.RetrieveRegistrationCategories());
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Edit(int? id)
        {
            if (!id.HasValue || id == 0)
            {
                // Adding new item
                ViewData["NewRecordText"] = Core.NewRecordText;
                return View(new RegistrationCategory());
            }
            else
            {
                // Editing existing item
                return View(ObjectFactory.RetrieveRegistrationCategory((int)id));
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Edit(int? id, FormCollection formCollection)
        {
            if (ModelState.IsValid)
            {
                // Create or retrieve item
                RegistrationCategory item;
                if (id > 0)
                {
                    item = ObjectFactory.RetrieveRegistrationCategory((int)(id.HasValue ? id : 0));
                }
                else
                {
                    item = new RegistrationCategory();
                }

                // Populate
                item.Code = formCollection["Code"];
                item.Description = formCollection["Description"];
                item.IsActive = formCollection["IsActive"].Contains("true");
                item.Name = formCollection["Name"];

                // Save and redirect
                if (item.Save(User.Identity.Name))
                {
                    return RedirectToAction("Index", "RegistrationCategory", new { searchName = Server.UrlEncode(item.Name) });
                }
            }

            // Failed
            return View();
        }

        [AcceptVerbs(HttpVerbs.Delete)]
        public void Delete(int id)
        {
            // Delete item
            RegistrationCategory item = ObjectFactory.RetrieveRegistrationCategory(id);
            item.Remove();
        }
    }
}
