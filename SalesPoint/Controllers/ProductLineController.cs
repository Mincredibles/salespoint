﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Masonite.MTier.Utility;
using Masonite.MTier.SalesPoint;

namespace SalesPoint.Controllers
{
    [Authorize(Roles = "SalesPoint Administrators, Home Depot Administrators")]
    public class ProductLineController : Controller
    {
        [AcceptVerbs(new string[] { "Get", "Post" })]
        public ActionResult Index(string searchName)
        {
            searchName = Server.UrlDecode(searchName);
            ViewData["searchName"] = searchName;
            return View(ObjectFactory.RetrieveProductLines(searchName));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Edit(int? id)
        {
            if (!id.HasValue || id == 0)
            {
                // Adding new item
                ViewData["NewRecordText"] = Core.NewRecordText;
                return View(new ProductLine());
            }
            else
            {
                // Editing existing item
                return View(ObjectFactory.RetrieveProductLine((int)id));
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Edit(int? id, FormCollection formCollection)
        {
            if (ModelState.IsValid)
            {
                // Create or retrieve item
                ProductLine productLine;
                if (id > 0)
                {
                    productLine = ObjectFactory.RetrieveProductLine((int)(id.HasValue ? id : 0));
                }
                else
                {
                    productLine = new ProductLine();
                }

                // Populate
                productLine.Code = formCollection["Code"];
                productLine.Description = formCollection["Description"];
                productLine.IsActive = formCollection["IsActive"].Contains("true");
                productLine.Name = formCollection["Name"];

                // Save and redirect
                if (productLine.Save())
                {
                    return RedirectToAction("Index", "ProductLine", new { searchName = Server.UrlEncode(productLine.Name) });
                }
            }

            // Failed
            return View();
        }

        [AcceptVerbs(HttpVerbs.Delete)]
        public void Delete(int id)
        {
            // Delete item
            ProductLine productLine = ObjectFactory.RetrieveProductLine(id);
            productLine.Remove();
        }
    }
}