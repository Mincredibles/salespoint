﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SalesPoint.Controllers
{
    [Authorize(Roles = "SalesPoint Administrators, Max Team")]
    public class ConfigureOneViewController : Controller
    {
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult IndexPartial(string defaultMaxView)
        {
            ViewData["DefaultMaxView"] = defaultMaxView;

            return PartialView();
        }
    }
}
