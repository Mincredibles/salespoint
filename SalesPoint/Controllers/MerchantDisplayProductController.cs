﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using Masonite.MTier.Utility;
using Masonite.MTier.SalesPoint;

namespace SalesPoint.Controllers
{
    public class MerchantDisplayProductController : Controller
    {
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult IndexPartial(int merchantId)
        {
            ViewData["MerchantId"] = merchantId;

            using (Merchant merchant = ObjectFactory.RetrieveMerchant(merchantId, User.Identity.Name))
            {
                ViewData["MerchantName"] = merchant.Name.Replace("'", "\\'");
            }

            return PartialView("IndexPartial", ObjectFactory.RetrieveDisplayProducts());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public void Edit(int merchantId, int displayProductId)
        {
            // Create or retrieve item
            MerchantDisplayProduct merchantDisplayProduct = new MerchantDisplayProduct();

            // Populate
            merchantDisplayProduct.MerchantId = merchantId;
            merchantDisplayProduct.DisplayProductId = displayProductId;

            // Save
            merchantDisplayProduct.Save();
        }

        [AcceptVerbs(HttpVerbs.Delete)]
        public void Delete(int merchantId, int displayProductId)
        {
            // Delete item
            MerchantDisplayProduct merchantDisplayProduct = new MerchantDisplayProduct();
            merchantDisplayProduct.MerchantId = merchantId;
            merchantDisplayProduct.DisplayProductId = displayProductId;
            merchantDisplayProduct.Remove();
        }
    }
}