﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Masonite.MTier.Utility;
using Masonite.MTier.SalesPoint;

namespace SalesPoint.Controllers
{
    [Authorize(Roles = "SalesPoint Administrators, Max Team")]
    public class ConfigureOneProductLineController : Controller
    {
        [AcceptVerbs(new string[] { "Get", "Post" })]
        public ActionResult Index(string searchName)
        {
            searchName = Server.UrlDecode(searchName);
            ViewData["searchName"] = searchName;
            return View(ObjectFactory.RetrieveConfigureOneProductLines(searchName));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult IndexPartial(int? configureOneProductLineId)
        {
            ViewData["ConfigureOneProductLineId"] = (int)configureOneProductLineId;

            return PartialView(ObjectFactory.RetrieveConfigureOneProductLines());
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Edit(int? id)
        {
            if (!id.HasValue || id == 0)
            {
                // Adding new item
                ViewData["NewRecordText"] = Core.NewRecordText;
                return View(new ConfigureOneProductLine());
            }
            else
            {
                // Editing existing item
                return View(ObjectFactory.RetrieveConfigureOneProductLine((int)id));
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Edit(int? id, FormCollection formCollection)
        {
            if (ModelState.IsValid)
            {
                // Create or retrieve item
                ConfigureOneProductLine configureOneProductLine;
                if (id > 0)
                {
                    configureOneProductLine = ObjectFactory.RetrieveConfigureOneProductLine((int)(id.HasValue ? id : 0));
                }
                else
                {
                    configureOneProductLine = new ConfigureOneProductLine();
                }

                // Populate
                configureOneProductLine.Code = formCollection["Code"];
                configureOneProductLine.Description = formCollection["Description"];
                configureOneProductLine.IsActive = formCollection["IsActive"].Contains("true");
                configureOneProductLine.Name = formCollection["Name"];

                // Save and redirect
                if (configureOneProductLine.Save())
                {
                    return RedirectToAction("Index", "ConfigureOneProductLine", new { searchName = Server.UrlEncode(configureOneProductLine.Name) });
                }
            }

            // Failed
            return View();
        }

        [AcceptVerbs(HttpVerbs.Delete)]
        public void Delete(int id)
        {
            // Delete item
            ConfigureOneProductLine configureOneProductLine = ObjectFactory.RetrieveConfigureOneProductLine(id);
            configureOneProductLine.Remove();
        }
    }
}
