﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Masonite.MTier.Utility;
using Masonite.MTier.SalesPoint;

namespace SalesPoint.Controllers
{
    [Authorize(Roles = "SalesPoint Administrators")]
    public class AuditController : Controller
    {
        [AcceptVerbs(new string[] { "Get", "Post" })]
        public ActionResult IndexPartial(int merchantId)
        {
            using (Merchant merchant = ObjectFactory.RetrieveMerchant(merchantId, User.Identity.Name))
            {
                ViewData["MerchantName"] = merchant.Name.Replace("'", "\\'");
            }

            return PartialView("IndexPartial", ObjectFactory.RetrieveAudits(merchantId, "Merchant"));
        }
    }
}
