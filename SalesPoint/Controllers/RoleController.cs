﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Masonite.MTier.Utility;
using Masonite.MTier.SalesPoint;

namespace SalesPoint.Controllers
{
    [Authorize(Roles = "SalesPoint Administrators")]
    public class RoleController : Controller
    {
        [AcceptVerbs(new string[] { "Get", "Post" })]
        public ActionResult Index(string searchName)
        {
            searchName = Server.UrlDecode(searchName);
            ViewData["searchName"] = searchName;
            return View(ObjectFactory.RetrieveRoles(searchName));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Edit(int? id)
        {
            if (!id.HasValue || id == 0)
            {
                // Adding new item
                ViewData["NewRecordText"] = Core.NewRecordText;
                return View(new Role());
            }
            else
            {
                // Editing existing item
                return View(ObjectFactory.RetrieveRole((int)id));
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Edit(int? id, FormCollection formCollection)
        {
            if (ModelState.IsValid)
            {
                // Create or retrieve item
                Role role;
                if (id > 0)
                {
                    role = ObjectFactory.RetrieveRole((int)(id.HasValue ? id : 0));
                }
                else
                {
                    role = new Role();
                }

                // Populate
                role.Code = formCollection["Code"];
                role.Description = formCollection["Description"];
                role.IsActive = formCollection["IsActive"].Contains("true");
                role.Name = formCollection["Name"];

                // Save and redirect
                if (role.Save())
                {
                    return RedirectToAction("Index", "Role", new { searchName = Server.UrlEncode(role.Name) });
                }
            }

            // Failed
            return View();
        }

        [AcceptVerbs(HttpVerbs.Delete)]
        public void Delete(int id)
        {
            // Delete item
            Role role = ObjectFactory.RetrieveRole(id);
            role.Remove();
        }
    }
}
