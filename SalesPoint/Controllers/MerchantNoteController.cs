﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Masonite.MTier.Utility;
using Masonite.MTier.SalesPoint;

namespace SalesPoint.Controllers
{
    public class MerchantNoteController : Controller
    {
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult EditPartial(int id, int merchantId)
        {
            if (id > 0)
            {
                return PartialView("EditPartial", ObjectFactory.RetrieveMerchantNote(id));
            }
            else
            {
                return PartialView("EditPartial", new MerchantNote());
            }
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult IndexPartial(int merchantId)
        {
            ViewData["MerchantId"] = merchantId;

            using (Merchant merchant = ObjectFactory.RetrieveMerchant(merchantId, User.Identity.Name))
            {
                ViewData["MerchantName"] = merchant.Name.Replace("'", "\\'");
            }

            return PartialView("IndexPartial", ObjectFactory.RetrieveMerchantNotes(merchantId));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Edit(int? id)
        {
            if (!id.HasValue || id == 0)
            {
                // Adding new item
                ViewData["NewRecordText"] = Core.NewRecordText;
                return View(new MerchantNote());
            }
            else
            {
                // Editing existing item
                return View(ObjectFactory.RetrieveMerchantNote((int)id));
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public void Edit(int? id, int? merchantId, FormCollection formCollection)
        {
            if (ModelState.IsValid)
            {
                // Create or retrieve item
                MerchantNote merchantNote;
                if (id > 0)
                {
                    merchantNote = ObjectFactory.RetrieveMerchantNote((int)id);
                }
                else
                {
                    merchantNote = new MerchantNote();
                }

                // Populate
                merchantNote.IsFlagged = string.Compare("on", formCollection["IsFlagged"], true, System.Globalization.CultureInfo.CurrentUICulture) == 0;
                merchantNote.MerchantId = (int)merchantId;
                merchantNote.Text = formCollection["Text"];
                
                // Save MerchantNote
                merchantNote.Save(User.Identity.Name);

                //// Save Tags (if any)
                //string tags = formCollection["Tags"];
                //if (!string.IsNullOrEmpty(tags))
                //{
                //    List<string> tagList = new List<string>(tags.Split(char.Parse(",")));
                //    foreach (string tag in tagList)
                //    {
                //        MerchantNoteTag merchantNoteTag = new MerchantNoteTag();

                //        merchantNoteTag.MerchantNoteId = merchantNote.Id;
                //        merchantNoteTag.Text = tag.Trim();

                //        // Trim characters over 50
                //        if (merchantNoteTag.Text.Length > 50) { merchantNoteTag.Text = merchantNoteTag.Text.Substring(0, 50); }

                //        merchantNoteTag.Save();
                //    }
                //}
            }
        }

        [AcceptVerbs(HttpVerbs.Delete)]
        public void Delete(int id)
        {
            // Delete item
            using (MerchantNote merchantNote = ObjectFactory.RetrieveMerchantNote(id))
            {
                merchantNote.Remove();
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public void SetFlag(int id, bool isFlagged)
        {
            using (MerchantNote note = ObjectFactory.RetrieveMerchantNote(id))
            {
                note.IsFlagged = isFlagged;
                note.Save(User.Identity.Name);
            }
        }
    }
}