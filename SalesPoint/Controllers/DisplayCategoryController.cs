﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Masonite.MTier.Utility;
using Masonite.MTier.SalesPoint;

namespace SalesPoint.Controllers
{
    [Authorize(Roles = "SalesPoint Administrators, Home Depot Administrators")]
    public class DisplayCategoryController : Controller
    {
        [AcceptVerbs(new string[] { "Get", "Post" })]
        public ActionResult Index(string searchName)
        {
            searchName = Server.UrlDecode(searchName);
            ViewData["searchName"] = searchName;
            return View(ObjectFactory.RetrieveDisplayCategories(searchName));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Edit(int? id)
        {
            if (!id.HasValue || id == 0)
            {
                // Adding new item
                ViewData["NewRecordText"] = Core.NewRecordText;
                return View(new DisplayCategory());
            }
            else
            {
                // Editing existing item
                return View(ObjectFactory.RetrieveDisplayCategory((int)id));
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Edit(int? id, FormCollection formCollection)
        {
            if (ModelState.IsValid)
            {
                // Create or retrieve item
                DisplayCategory displayCategory;
                if (id > 0)
                {
                    displayCategory = ObjectFactory.RetrieveDisplayCategory((int)(id.HasValue ? id : 0));
                }
                else
                {
                    displayCategory = new DisplayCategory();
                }

                // Populate
                displayCategory.Code = formCollection["Code"];
                displayCategory.Description = formCollection["Description"];
                displayCategory.IsActive = formCollection["IsActive"].Contains("true");
                displayCategory.Name = formCollection["Name"];
                
                // Save and redirect
                if (displayCategory.Save())
                {
                    return RedirectToAction("Index", "DisplayCategory", new { searchName = Server.UrlEncode(displayCategory.Name) });
                }
            }

            // Failed
            return View();
        }

        [AcceptVerbs(HttpVerbs.Delete)]
        public void Delete(int id)
        {
            // Delete item
            DisplayCategory displayCategory = ObjectFactory.RetrieveDisplayCategory(id);
            displayCategory.Remove();
        }
    }
}
