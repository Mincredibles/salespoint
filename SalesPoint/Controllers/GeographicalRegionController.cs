﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Masonite.MTier.Utility;
using Masonite.MTier.SalesPoint;

namespace SalesPoint.Controllers
{
    [Authorize(Roles = "SalesPoint Administrators, Home Depot Administrators")]
    public class GeographicalRegionController : Controller
    {
        [AcceptVerbs(new string[] { "Get", "Post" })]
        public ActionResult Index(string searchName)
        {
            searchName = Server.UrlDecode(searchName);
            ViewData["searchName"] = searchName;
            return View(ObjectFactory.RetrieveGeographicalRegions(searchName));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Edit(int? id)
        {
            if (!id.HasValue || id == 0)
            {
                // Adding new item
                ViewData["NewRecordText"] = Core.NewRecordText;
                return View(new GeographicalRegion());
            }
            else
            {
                // Editing existing item
                return View(ObjectFactory.RetrieveGeographicalRegion((int)id));
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Edit(int? id, FormCollection formCollection)
        {
            if (ModelState.IsValid)
            {
                // Create or retrieve item
                GeographicalRegion geographicalRegion;
                if (id > 0)
                {
                    geographicalRegion = ObjectFactory.RetrieveGeographicalRegion((int)(id.HasValue ? id : 0));
                }
                else
                {
                    geographicalRegion = new GeographicalRegion();
                }

                // Populate
                geographicalRegion.Code = formCollection["Code"];
                geographicalRegion.Description = formCollection["Description"];
                geographicalRegion.IsActive = formCollection["IsActive"].Contains("true");
                geographicalRegion.Manager = formCollection["Manager"];
                geographicalRegion.MerchantChainId = formCollection["MerchantChainId"].ToInt32();
                geographicalRegion.Name = formCollection["Name"];
                geographicalRegion.Number = formCollection["Number"].ToInt32();

                // Save and redirect
                if (geographicalRegion.Save())
                {
                    return RedirectToAction("Index", "GeographicalRegion", new { searchName = Server.UrlEncode(geographicalRegion.Name) });
                }
            }

            // Failed
            return View();
        }

        [AcceptVerbs(HttpVerbs.Delete)]
        public void Delete(int id)
        {
            // Delete item
            GeographicalRegion geographicalRegion = ObjectFactory.RetrieveGeographicalRegion(id);
            geographicalRegion.Remove();
        }
    }
}
