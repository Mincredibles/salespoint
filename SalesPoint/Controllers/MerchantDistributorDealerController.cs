﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Masonite.MTier.Utility;
using Masonite.MTier.SalesPoint;

namespace SalesPoint.Controllers
{
    [Authorize(Roles = "SalesPoint Administrators, Max Team")]
    public class MerchantDistributorDealerController : Controller
    {
        [AcceptVerbs(new string[] { "Get", "Post" })]
        public ActionResult ChooseDistributorPartial(int merchantId, string searchName)
        {
            // Note that this method returns ConfigureOneParentMerchants only
            ViewData["MerchantId"] = merchantId;
            System.Collections.Generic.List<Merchant> merchants = ObjectFactory.RetrieveMerchantsForAdministration(searchName, null, 0, null, null, 0, 0, false, true, false);
            return PartialView("ChooseDistributorPartial", merchants);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult IndexPartial(int merchantId)
        {
            ViewData["MerchantId"] = merchantId;

            using (Merchant merchant = ObjectFactory.RetrieveMerchant(merchantId, User.Identity.Name))
            {
                ViewData["MerchantName"] = merchant.Name.Replace("'", "\\'");
            }

            return PartialView("IndexPartial", ObjectFactory.RetrieveMerchantDistributorDealers(merchantId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public void EditDistributorPartial(int distributorId, int dealerId)
        {
            // Create or retrieve item
            MerchantDistributorDealer merchantDistributorDealer = new MerchantDistributorDealer();

            // Populate
            merchantDistributorDealer.MerchantDistributorId = distributorId;
            merchantDistributorDealer.MerchantDealerId = dealerId;

            // Save
            merchantDistributorDealer.Save(User.Identity.Name);
        }
       
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult IndexEdit(int merchantId, FormCollection formCollection)
        {
            // Capture MerchantDistributorDealer checked values
            List<MerchantDistributorDealer> distributorDealers = ObjectFactory.RetrieveMerchantDistributorDealers(merchantId);
            string[] MerchantDistributorDealerId = { };
            string[] IsDistributorSupplyingProductEntry = { };
            string[] IsDistributorSupplyingProductInterior = { };
            string[] IsDealerReceivingCartsEntry = { };
            string[] IsDealerReceivingCartsInterior = { };
            string[] IsDealerAllowedToSwitchMaxView = { };
            string[] ConfigureOneProductLineId = { };
            string[] PurchaseOrderSupportEmailAddress = { };
            string[] DefaultMaxView = { };
            string[] DistributorSalesRepEmailAddress = { };
            string[] DistributorSalesRepName = { };
            string[] PriceList = { };
            string[] SecondaryDiscountType = { };
            string[] MaxVersionId = { };

            if (formCollection["MerchantDistributorDealerId"] != null) { MerchantDistributorDealerId = formCollection["MerchantDistributorDealerId"].Split(char.Parse(",")); }
            if (formCollection["IsDistributorSupplyingProductEntry"] != null) { IsDistributorSupplyingProductEntry = formCollection["IsDistributorSupplyingProductEntry"].Split(char.Parse(",")); }
            if (formCollection["IsDistributorSupplyingProductInterior"] != null) { IsDistributorSupplyingProductInterior = formCollection["IsDistributorSupplyingProductInterior"].Split(char.Parse(",")); }
            if (formCollection["IsDealerReceivingCartsEntry"] != null) { IsDealerReceivingCartsEntry = formCollection["IsDealerReceivingCartsEntry"].Split(char.Parse(",")); }
            if (formCollection["IsDealerReceivingCartsInterior"] != null) { IsDealerReceivingCartsInterior = formCollection["IsDealerReceivingCartsInterior"].Split(char.Parse(",")); }
            if (formCollection["IsDealerAllowedToSwitchMaxView"] != null) { IsDealerAllowedToSwitchMaxView = formCollection["IsDealerAllowedToSwitchMaxView"].Split(char.Parse(",")); }
            if (formCollection["ConfigureOneProductLineId"] != null) { ConfigureOneProductLineId = formCollection["ConfigureOneProductLineId"].Split(char.Parse(",")); }
            if (formCollection["PurchaseOrderSupportEmailAddress"] != null) { PurchaseOrderSupportEmailAddress = formCollection["PurchaseOrderSupportEmailAddress"].Split(char.Parse(",")); }
            if (formCollection["DefaultMaxView"] != null) { DefaultMaxView = formCollection["DefaultMaxView"].Split(char.Parse(",")); }
            if (formCollection["DistributorSalesRepEmailAddress"] != null) { DistributorSalesRepEmailAddress = formCollection["DistributorSalesRepEmailAddress"].Split(char.Parse(",")); }
            if (formCollection["DistributorSalesRepName"] != null) { DistributorSalesRepName = formCollection["DistributorSalesRepName"].Split(char.Parse(",")); }
            if (formCollection["PriceList"] != null) { PriceList = formCollection["PriceList"].Split(char.Parse(",")); }
            if (formCollection["SecondaryDiscountType"] != null) { SecondaryDiscountType = formCollection["SecondaryDiscountType"].Split(char.Parse(",")); }
            if (formCollection["MaxVersionId"] != null) { MaxVersionId = formCollection["MaxVersionId"].Split(char.Parse(",")); }

            foreach (MerchantDistributorDealer distributorDealer in distributorDealers)
            {
                string id = distributorDealer.Id.ToString(System.Globalization.CultureInfo.CurrentUICulture);

                distributorDealer.IsDealerAllowedToSwitchMaxView = IsDealerAllowedToSwitchMaxView.Contains(id);
                distributorDealer.IsDealerReceivingCartsEntry = IsDealerReceivingCartsEntry.Contains(id);
                distributorDealer.IsDealerReceivingCartsInterior = IsDealerReceivingCartsInterior.Contains(id);
                distributorDealer.IsDistributorSupplyingProductEntry = IsDistributorSupplyingProductEntry.Contains(id);
                distributorDealer.IsDistributorSupplyingProductInterior = IsDistributorSupplyingProductInterior.Contains(id);

                int position = 0;
                foreach (string item in MerchantDistributorDealerId)
                {
                    if (string.Compare(item, id, false, System.Globalization.CultureInfo.CurrentUICulture) == 0)
                    {
                        // Found match, so use the item's index to grab the corresponding item from ConfigureOneProductLineId and PurchaseOrderSupportEmailAddress collections
                        distributorDealer.ConfigureOneProductLineId = ConfigureOneProductLineId[position].ToInt32();
                        distributorDealer.DistributorSalesRepEmailAddress = DistributorSalesRepEmailAddress[position];
                        distributorDealer.DistributorSalesRepName = DistributorSalesRepName[position];
                        distributorDealer.DefaultMaxView = DefaultMaxView[position];
                        distributorDealer.PriceList = PriceList[position];
                        distributorDealer.PurchaseOrderSupportEmailAddress = PurchaseOrderSupportEmailAddress[position];
                        distributorDealer.SecondaryDiscountType = SecondaryDiscountType[position];
                        distributorDealer.MaxVersionId = MaxVersionId[position].ToInt32();
                        break;
                    }
                    position++;
                }

                distributorDealer.Save(User.Identity.Name);

                string[] maxPermissions = { };
                if (formCollection["MaxPermission-" + distributorDealer.ConfigureOneWorkgroupId] != null)
                    maxPermissions = formCollection["MaxPermission-" + distributorDealer.ConfigureOneWorkgroupId].Split(',');

                var allPermissions = ObjectFactory.RetriveDefaultDealerPermission(distributorDealer.ConfigureOneWorkgroupId.ToInt32(), true);
                foreach (var permission in allPermissions)
                {
                    if (maxPermissions.Contains(permission.PermissionId.ToString()))
                    {
                        if (permission.WorkgroupId == 0)
                            ObjectFactory.InsertDefaultDealerPermission(distributorDealer.ConfigureOneWorkgroupId.ToInt32(), permission.PermissionId);
                    }
                    else
                    {
                        if (permission.WorkgroupId != 0)
                            ObjectFactory.DeleteMerchantDistributorDealerPermission(distributorDealer.ConfigureOneWorkgroupId.ToInt32(), permission.PermissionId);
                    }
                }
            }

            // Return
            return RedirectToAction("Edit", "Merchant", new { id = merchantId, tab = (int)MerchantEditTab.Distributors });
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult RemoveDistributorDealers(int merchantId, FormCollection formCollection)
        {
            foreach (var removeKey in formCollection.AllKeys)
                if (removeKey.StartsWith("remove-mark-") && formCollection[removeKey] == "1")
                {
                    // Delete item
                    MerchantDistributorDealer merchantDistributorDealer = ObjectFactory.RetrieveMerchantDistributorDealer(removeKey.Replace("remove-mark-", "").ToInt32());
                    merchantDistributorDealer.Remove();
                }

            // Return
            return RedirectToAction("Edit", "Merchant", new { id = merchantId, tab = (int)MerchantEditTab.Distributors });
        }
    }
}
