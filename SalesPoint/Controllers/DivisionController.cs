﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Masonite.MTier.Utility;
using Masonite.MTier.SalesPoint;

namespace SalesPoint.Controllers
{
    [Authorize(Roles = "SalesPoint Administrators, Home Depot Administrators")]
    public class DivisionController : Controller
    {
        [AcceptVerbs(new string[] { "Get", "Post" })]
        public ActionResult Index(string searchName)
        {
            searchName = Server.UrlDecode(searchName);
            ViewData["searchName"] = searchName;
            return View(ObjectFactory.RetrieveDivisions(searchName));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Edit(int? id)
        {
            if (!id.HasValue || id == 0)
            {
                // Adding new item
                ViewData["NewRecordText"] = Core.NewRecordText;
                return View(new Division());
            }
            else
            {
                // Editing existing item
                return View(ObjectFactory.RetrieveDivision((int)id));
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Edit(int? id, FormCollection formCollection)
        {
            if (ModelState.IsValid)
            {
                // Create or retrieve item
                Division division;
                if (id > 0)
                {
                    division = ObjectFactory.RetrieveDivision((int)(id.HasValue ? id : 0));
                }
                else
                {
                    division = new Division();
                }

                // Populate
                division.Code = formCollection["Code"];
                division.Description = formCollection["Description"];
                division.IsActive = formCollection["IsActive"].Contains("true");
                division.Name = formCollection["Name"];
                
                // Save and redirect
                if (division.Save())
                {
                    return RedirectToAction("Index", "Division", new { searchName = Server.UrlEncode(division.Name) });
                }
            }

            // Failed
            return View();
        }

        [AcceptVerbs(HttpVerbs.Delete)]
        public void Delete(int id)
        {
            // Delete item
            Division division = ObjectFactory.RetrieveDivision(id);
            division.Remove();
        }
    }
}
