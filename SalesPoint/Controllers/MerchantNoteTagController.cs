﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Masonite.MTier.Utility;
using Masonite.MTier.SalesPoint;

namespace SalesPoint.Controllers
{
    public class MerchantNoteTagController : Controller
    {
        [AcceptVerbs(new string[] { "Get", "Post" })]
        public ActionResult IndexPartial(int merchantNoteId, string searchName)
        {
            return PartialView(ObjectFactory.RetrieveMerchantNoteTags(merchantNoteId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public void Edit(int? id, int merchantNoteId, string text)
        {
            if (ModelState.IsValid)
            {
                // Create or retrieve item
                MerchantNoteTag merchantNoteTag;
                if (id > 0)
                {
                    merchantNoteTag = ObjectFactory.RetrieveMerchantNoteTag((int)(id.HasValue ? id : 0));
                }
                else
                {
                    merchantNoteTag = new MerchantNoteTag();
                }

                // Populate
                merchantNoteTag.MerchantNoteId = merchantNoteId;
                merchantNoteTag.Text = Server.HtmlEncode(text);

                // Save
                merchantNoteTag.Save();
            }
        }

        [AcceptVerbs(HttpVerbs.Delete)]
        public void Delete(int id)
        {
            // Delete item
            MerchantNoteTag merchantNoteTag = ObjectFactory.RetrieveMerchantNoteTag(id);
            merchantNoteTag.Remove();
        }
    }
}