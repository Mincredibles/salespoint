﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Masonite.MTier.Utility;
using Masonite.MTier.SalesPoint;
using System.IO;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;

namespace SalesPoint.Controllers
{
    public class CorrespondenceRecipientController : Controller
    {
        [Authorize(Roles = "SalesPoint Administrators, Marketing Team, Correspondence Approvers")]
        [AcceptVerbs(new string[] { "Get", "Post" })]
        public void IndexToExcel(int id)
        {
            List<CorrespondenceRecipient> items = new List<CorrespondenceRecipient>();

            string currentUser = User.IsInRole("Correspondence Approvers") ? User.Identity.Name : null;

            // Retrieve Orders + OrderItems by ContactId, then perform a LINQ select so that only the desired fields are bound to the grid
            items = ObjectFactory.RetrieveCorrespondenceRecipients(id, currentUser);
            var recipients = from r in items
                         select new
                         {
                             Address1 = r.Address1,
                             Address2 = r.Address2,
                             City = r.City,
                             CountrySubdivision = r.CountrySubdivision,
                             FirstName = r.FirstName,
                             LastName = r.LastName,
                             EmailAddress = r.EmailAddress,
                             MerchantName = r.MerchantName,
                             PostalCode = r.PostalCode
                         };



            Response.Clear();
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", @"attachment;filename=MerchantExport.xlsx");


            // Keeping the spreadsheet-generation code here in the controller for now, for the sake of simplicity during
            // the development phase.  Once the process for generating spreadsheets has been ironed out more fully, we can 
            // look at moving this to an MTier utility class, and refactor it to be reusable.


            using (MemoryStream mem = new MemoryStream())
            {
                // Create a spreadsheet document by supplying the filepath
                // By default, AutoSave = true, Editable = true, and Type = xlsx.
                SpreadsheetDocument spreadsheetDocument = SpreadsheetDocument.Create(mem, SpreadsheetDocumentType.Workbook);

                // Add a WorkbookPart to the document
                WorkbookPart workbookpart = spreadsheetDocument.AddWorkbookPart();
                workbookpart.Workbook = new Workbook();

                // Add a WorksheetPart to the WorkbookPart
                SheetData sheetData = new SheetData();
                WorksheetPart worksheetPart = workbookpart.AddNewPart<WorksheetPart>();
                Worksheet worksheet = new Worksheet(sheetData);
                worksheetPart.Worksheet = worksheet;

                // Add Sheets to the Workbook
                Sheets sheets = spreadsheetDocument.WorkbookPart.Workbook.AppendChild<Sheets>(new Sheets());

                // Append a new worksheet and associate it with the workbook
                Sheet sheet = new Sheet()
                {
                    Id = spreadsheetDocument.WorkbookPart.GetIdOfPart(worksheetPart),
                    SheetId = 1,
                    Name = "Report"
                };
                sheets.Append(sheet);

                // Add cells and data
                UInt32 rowIndex = 0;
                UInt32 cellIndex = 0;

                // Header row
                Row row = new Row { RowIndex = ++rowIndex };
                sheetData.AppendChild(row);
                                
                row.AppendChild(CreateTextCell(ColumnLetter(cellIndex++), rowIndex, "First Name"));
                row.AppendChild(CreateTextCell(ColumnLetter(cellIndex++), rowIndex, "Last Name"));
                row.AppendChild(CreateTextCell(ColumnLetter(cellIndex++), rowIndex, "Email Address"));
                row.AppendChild(CreateTextCell(ColumnLetter(cellIndex++), rowIndex, "Location"));
                row.AppendChild(CreateTextCell(ColumnLetter(cellIndex++), rowIndex, "Address"));
                row.AppendChild(CreateTextCell(ColumnLetter(cellIndex++), rowIndex, "Address2"));
                row.AppendChild(CreateTextCell(ColumnLetter(cellIndex++), rowIndex, "City"));
                row.AppendChild(CreateTextCell(ColumnLetter(cellIndex++), rowIndex, "State/Prov."));
                row.AppendChild(CreateTextCell(ColumnLetter(cellIndex++), rowIndex, "Postal Code"));
                                
                // Data rows
                foreach (var item in recipients)
                {
                    cellIndex = 0;
                    Row dataRow = new Row { RowIndex = ++rowIndex };
                    sheetData.AppendChild(dataRow);

                    dataRow.AppendChild(CreateTextCell(ColumnLetter(cellIndex++), rowIndex, item.FirstName));
                    dataRow.AppendChild(CreateTextCell(ColumnLetter(cellIndex++), rowIndex, item.LastName));
                    dataRow.AppendChild(CreateTextCell(ColumnLetter(cellIndex++), rowIndex, item.EmailAddress));
                    dataRow.AppendChild(CreateTextCell(ColumnLetter(cellIndex++), rowIndex, item.MerchantName));
                    dataRow.AppendChild(CreateTextCell(ColumnLetter(cellIndex++), rowIndex, item.Address1));
                    dataRow.AppendChild(CreateTextCell(ColumnLetter(cellIndex++), rowIndex, item.Address2));
                    dataRow.AppendChild(CreateTextCell(ColumnLetter(cellIndex++), rowIndex, item.City));
                    dataRow.AppendChild(CreateTextCell(ColumnLetter(cellIndex++), rowIndex, item.CountrySubdivision));
                    dataRow.AppendChild(CreateTextCell(ColumnLetter(cellIndex++), rowIndex, item.PostalCode));
                }

                // Save
                workbookpart.Workbook.Save();

                // Close the document and output
                spreadsheetDocument.Close();
                mem.Position = 0;
                mem.CopyTo(Response.OutputStream);
            }
        }

        private string ColumnLetter(UInt32 columnIndex)
        {
            UInt32 intFirstLetter = ((columnIndex) / 676) + 64;
            UInt32 intSecondLetter = ((columnIndex % 676) / 26) + 64;
            UInt32 intThirdLetter = (columnIndex % 26) + 65;

            UInt32 firstLetter = (intFirstLetter > 64) ? (char)intFirstLetter : ' ';
            UInt32 secondLetter = (intSecondLetter > 64) ? (char)intSecondLetter : ' ';
            UInt32 thirdLetter = (char)intThirdLetter;

            return string.Concat(firstLetter, secondLetter, thirdLetter).Trim();
        }

        private Cell CreateTextCell(string header, UInt32 rowIndex, string text)
        {
            Cell cell = new Cell();
            cell.DataType = CellValues.InlineString;
            cell.CellReference = header + rowIndex;

            InlineString inlineString = new InlineString();
            inlineString.AppendChild(new Text(text));

            cell.AppendChild(inlineString);

            return cell;
        }
    }
}
