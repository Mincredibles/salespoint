﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Masonite.MTier.Utility;
using Masonite.MTier.SalesPoint;

namespace SalesPoint.Controllers
{
    [Authorize(Roles = "SalesPoint Administrators")]
    public class ConfigureOneTimeZoneController : Controller
    {
        [AcceptVerbs(new string[] { "Get", "Post" })]
        public ActionResult Index(string searchName)
        {
            searchName = Server.UrlDecode(searchName);
            ViewData["searchName"] = searchName;
            return View(ObjectFactory.RetrieveConfigureOneTimeZones(searchName));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Edit(int? id)
        {
            if (!id.HasValue || id == 0)
            {
                // Adding new item
                ViewData["NewRecordText"] = Core.NewRecordText;
                return View(new ConfigureOneTimeZone());
            }
            else
            {
                // Editing existing item
                return View(ObjectFactory.RetrieveConfigureOneTimeZone((int)id));
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Edit(int? id, FormCollection formCollection)
        {
            if (ModelState.IsValid)
            {
                // Create or retrieve item
                ConfigureOneTimeZone configureOneTimeZone;
                if (id > 0)
                {
                    configureOneTimeZone = ObjectFactory.RetrieveConfigureOneTimeZone((int)(id.HasValue ? id : 0));
                }
                else
                {
                    configureOneTimeZone = new ConfigureOneTimeZone();
                }

                // Populate
                configureOneTimeZone.Code = formCollection["Code"];
                configureOneTimeZone.Description = formCollection["Description"];
                configureOneTimeZone.IsActive = formCollection["IsActive"].Contains("true");
                configureOneTimeZone.Name = formCollection["Name"];

                // Save and redirect
                if (configureOneTimeZone.Save())
                {
                    return RedirectToAction("Index", "ConfigureOneTimeZone", new { searchName = Server.UrlEncode(configureOneTimeZone.Name) });
                }
            }

            // Failed
            return View();
        }

        [AcceptVerbs(HttpVerbs.Delete)]
        public void Delete(int id)
        {
            // Delete item
            ConfigureOneTimeZone configureOneTimeZone = ObjectFactory.RetrieveConfigureOneTimeZone(id);
            configureOneTimeZone.Remove();
        }
    }
}
