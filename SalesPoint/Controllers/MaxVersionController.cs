﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Masonite.MTier.Utility;
using Masonite.MTier.SalesPoint;

namespace SalesPoint.Controllers
{
    [Authorize(Roles = "SalesPoint Administrators, Max Team")]
    public class MaxVersionController : Controller
    {
        [ChildActionOnly]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult IndexPartial(int? maxVersionId)
        {
            ViewData["SelectedValue"] = maxVersionId.HasValue ? (int)maxVersionId : 0;

            return PartialView(ObjectFactory.RetrieveMaxVersions());
        }
    }
}
