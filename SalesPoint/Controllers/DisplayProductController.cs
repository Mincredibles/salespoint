﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Masonite.MTier.Utility;
using Masonite.MTier.SalesPoint;

namespace SalesPoint.Controllers
{
    [Authorize(Roles = "SalesPoint Administrators, Home Depot Administrators")]
    public class DisplayProductController : Controller
    {
        [AcceptVerbs(new string[] { "Get", "Post" })]
        public ActionResult Index(string searchName)
        {
            searchName = Server.UrlDecode(searchName);
            ViewData["searchName"] = searchName;
            return View(ObjectFactory.RetrieveDisplayProducts(searchName, 0));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Edit(int? id)
        {
            if (!id.HasValue || id == 0)
            {
                // Adding new item
                ViewData["NewRecordText"] = Core.NewRecordText;
                return View(new DisplayProduct());
            }
            else
            {
                // Editing existing item
                return View(ObjectFactory.RetrieveDisplayProduct((int)id));
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Edit(int? id, FormCollection formCollection)
        {
            if (ModelState.IsValid)
            {
                // Create or retrieve item
                DisplayProduct displayProduct;
                if (id > 0)
                {
                    displayProduct = ObjectFactory.RetrieveDisplayProduct((int)(id.HasValue ? id : 0));
                }
                else
                {
                    displayProduct = new DisplayProduct();
                }

                // Populate
                displayProduct.Code = formCollection["Code"];
                displayProduct.Description = formCollection["Description"];
                displayProduct.IsActive = formCollection["IsActive"].Contains("true");
                displayProduct.Name = formCollection["Name"];

                // Save and redirect
                if (displayProduct.Save())
                {
                    return RedirectToAction("Index", "DisplayProduct", new { searchName = Server.UrlEncode(displayProduct.Name) });
                }
            }

            // Failed
            return View();
        }

        [AcceptVerbs(HttpVerbs.Delete)]
        public void Delete(int id)
        {
            // Delete item
            DisplayProduct displayProduct = ObjectFactory.RetrieveDisplayProduct(id);
            displayProduct.Remove();
        }
    }
}
