﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using Masonite.MTier;
using Masonite.MTier.Utility;
using Masonite.MTier.SalesPoint;

namespace SalesPoint.Controllers
{
    public class MerchantController : Controller
    {
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Index(string searchName)
        {
            if (!string.IsNullOrEmpty(searchName))
            {
                // Search
                return IndexHelper(searchName, null, null, null, null, null, null, null, null, false);
            }
            else
            {
                return View("Index", ObjectFactory.RetrieveUserMerchantMru(User.Identity.Name));
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Index(
                                    string searchMerchantName, 
                                    string searchTag,
                                    string searchMasterPackId, 
                                    string searchCity, 
                                    int? searchStateId, 
                                    string searchPostalCode,
                                    string searchContactFirstName,
                                    string searchContactLastName,
                                    string searchContactEmailAddress,
                                    string retrieveOnlyDisabled
                                    )
        {
            return IndexHelper(searchMerchantName, searchTag, searchMasterPackId, searchCity, searchStateId, searchPostalCode, searchContactFirstName, searchContactLastName, searchContactEmailAddress, string.Compare("on", retrieveOnlyDisabled, true, System.Globalization.CultureInfo.CurrentUICulture) == 0);
        }

        private ActionResult IndexHelper(
                                        string searchMerchantName, 
                                        string searchTag,
                                        string searchMasterPackId, 
                                        string searchCity, 
                                        int? searchStateId, 
                                        string searchPostalCode,
                                        string searchContactFirstName,
                                        string searchContactLastName,
                                        string searchContactEmailAddress,
                                        bool retrieveOnlyDisabled
                                        )
        {
            searchMerchantName = Server.UrlDecode(searchMerchantName);

            ViewData["searchMerchantName"] = searchMerchantName;
            ViewData["searchTag"] = searchTag;
            ViewData["searchMasterPackId"] = searchMasterPackId;
            ViewData["searchCity"] = searchCity;
            ViewData["searchStateId"] = searchStateId;
            ViewData["searchPostalCode"] = searchPostalCode;
            ViewData["searchContactFirstName"] = searchContactFirstName;
            ViewData["searchContactLastName"] = searchContactLastName;
            ViewData["searchContactEmailAddress"] = searchContactEmailAddress;
            ViewData["retrieveOnlyDisabled"] = retrieveOnlyDisabled ? "checked='checked'" : "";
            if (string.IsNullOrEmpty(searchMerchantName) && string.IsNullOrEmpty(searchTag) && string.IsNullOrEmpty(searchMasterPackId) && string.IsNullOrEmpty(searchCity) && (searchStateId == 0 || !searchStateId.HasValue) && string.IsNullOrEmpty(searchPostalCode) && string.IsNullOrEmpty(searchContactFirstName) && string.IsNullOrEmpty(searchContactLastName) && string.IsNullOrEmpty(searchContactEmailAddress))
            {
                // If no search criteria provided, then don't show any merchants.
                return View(new List<Merchant>());
            }
            else
            {
                return View(ObjectFactory.RetrieveMerchants(searchMerchantName, searchTag, searchCity, (int)(searchStateId.HasValue ? searchStateId : 0), searchPostalCode, searchMasterPackId, searchContactFirstName, searchContactLastName, searchContactEmailAddress, 0, retrieveOnlyDisabled, false, User.Identity.Name));
            }
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult EditPartial(int id)
        {
            Merchant merchant = null;

            try
            {
                if (id > 0)
                {
                    ViewData["MerchantId"] = id;
                    merchant = ObjectFactory.RetrieveMerchant(id, User.Identity.Name);
                    ViewData["MerchantName"] = merchant.Name.Replace("'", "\\'");
                }
                else
                {
                    ViewData["MerchantName"] = Core.NewRecordText;
                    merchant = new Merchant();
                }
            }
            catch (Exception ex)
            {
                if (string.Compare(ex.Message, "[Validation] The current user does not have permissions to view this item.", true, System.Globalization.CultureInfo.CurrentUICulture) == 0)
                {
                    // User attempted to access a Merchant that doesn't exist, or that they do not have permissions to view
                    ViewData["DeniedAccessToMerchant"] = "1";
                    merchant = new Merchant(); // Instantiate merchant so that the page can finish loading without throwing errors
                }
            }
            finally
            {

            }
            
            return PartialView("EditPartial", merchant);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult EditPartialMasoniteDotCom(int merchantId)
        {
            Merchant merchant = ObjectFactory.RetrieveMerchant(merchantId, User.Identity.Name);
            ViewData["MerchantName"] = merchant.Name.Replace("'", "\\'");
            ViewData["MerchantId"] = merchantId;
            ViewData["IsDistributorSupplyingProductEntry"] = 0;
            ViewData["IsDistributorSupplyingProductInterior"] = 0;
            ViewData["IsDistributorSupplyingProductPatio"] = 0;

            // Retrieve the product lines for this Merchant (non-Max locations only)
            if (!merchant.IsActiveInMax)
            {
                List<MerchantDistributorDealer> distributors = ObjectFactory.RetrieveMerchantDistributorDealers(merchant.Id);

                foreach (MerchantDistributorDealer item in distributors)
                {
                    if (string.Compare("Masonite Configurator Distributor", item.MerchantDistributorName, true, System.Globalization.CultureInfo.CurrentUICulture) == 0)
                    {
                        ViewData["IsDistributorSupplyingProductEntry"] = item.IsDistributorSupplyingProductEntry ? 1 : 0;
                        ViewData["IsDistributorSupplyingProductInterior"] = item.IsDistributorSupplyingProductInterior ? 1 : 0;
                        ViewData["IsDistributorSupplyingProductPatio"] = item.IsDistributorSupplyingProductPatio ? 1 : 0;

                        break;
                    }
                }
            }

            return PartialView("EditPartialMasoniteDotCom", merchant);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Edit(int? id, int? tab)
        {
            ViewData["MerchantId"] = id.HasValue ? (int)id : 0;
            ViewData["TabIndex"] = tab.HasValue ? (int)tab : 1;
            
            return View();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Edit(int? id, FormCollection formCollection, HttpPostedFileBase image)
        {
            // Create or retrieve item
            Merchant merchant;

            if (id > 0)
            {
                merchant = ObjectFactory.RetrieveMerchant((int)(id.HasValue ? id : 0), User.Identity.Name);
            }
            else
            {
                merchant = new Merchant();
            }
                        
            // Populate
            merchant.Address = formCollection["Address"];
            merchant.Address2 = formCollection["Address2"];
            merchant.CategoryId = formCollection["CategoryId"].ToInt32();
            merchant.City = formCollection["City"];
            merchant.Country = formCollection["Country"];
            merchant.FaxNumber = StringHelper.RemoveNonNumericCharacters(formCollection["FaxNumber"]);

            var deleteLogoControl = formCollection["DeleteLogo"];
            var deleteLogo = (deleteLogoControl != null) && deleteLogoControl.Contains("true");
            string originalImageName = null;
            if (!deleteLogo && (image != null))
            {
                originalImageName = merchant.Image;
                merchant.Image = image.FileName;
            }

            merchant.IsActive = formCollection["IsActive"].Contains("true");
            merchant.IsExcludedFromMax = formCollection["IsExcludedFromMax"].Contains("true");
            merchant.IsShownInMaxMetricsReports = formCollection["IsShownInMaxMetricsReports"].Contains("true");
            merchant.MailingAddress = formCollection["MailingAddress"];
            merchant.MailingAddress2 = formCollection["MailingAddress2"];
            merchant.MailingCity = formCollection["MailingCity"];
            merchant.MailingCountry = formCollection["MailingCountry"];
            merchant.MailingPostalCode = formCollection["MailingPostalCode"];
            merchant.MailingState = formCollection["MailingState"];
            merchant.MasoniteFranceCustomerAccountNumber = formCollection["MasoniteFranceCustomerAccountNumber"];
            merchant.MasoniteUnitedKingdomCustomerAccountNumber = formCollection["MasoniteUnitedKingdomCustomerAccountNumber"];
            merchant.MasterPackAccountsPayableNumber = formCollection["MasterPackAccountsPayableNumber"];
            merchant.MasterPackAccountsReceiveableNumber = formCollection["MasterPackAccountsReceiveableNumber"];
            merchant.MasterPackSalesPersonNumber = formCollection["MasterPackSalesPersonNumber"];
            if (merchant.CategoryId == 1)
            {
                merchant.MerchantChainId = formCollection["MerchantChainId"].ToInt32();
            }
            else
            {
                merchant.MerchantChainId = 0;
            }
            merchant.Name = formCollection["Name"];
            merchant.Number = formCollection["Number"];
            merchant.PhoneNumber = StringHelper.RemoveNonNumericCharacters(formCollection["PhoneNumber"]);
            merchant.PostalCode = formCollection["PostalCode"];
            merchant.RegionId = formCollection["RegionId"].ToInt32();
            merchant.StateId = formCollection["StateId"].ToInt32();
            merchant.WebsiteUrl = formCollection["WebsiteUrl"];

            // Save and redirect
            merchant.Save(User.Identity.Name, image?.InputStream, image?.ContentType, deleteLogo, originalImageName);
            
            // Return
            return RedirectToAction(string.Format(
                                                System.Globalization.CultureInfo.CurrentUICulture, 
                                                "Edit/{0}", 
                                                merchant.Id
                                                ));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditMasoniteDotCom(int merchantId, FormCollection formCollection)
        {
            // Create or retrieve item
            Merchant merchant = ObjectFactory.RetrieveMerchant(merchantId, User.Identity.Name);

            // Populate
            merchant.IsCarrierOfHomeDepotGlass = formCollection["IsCarrierOfHomeDepotGlass"].Contains("true");
            merchant.IsCarrierOfLowesGlass = formCollection["IsCarrierOfLowesGlass"].Contains("true");
            merchant.IsCarrierOfSpecialtyGlass = formCollection["IsCarrierOfSpecialtyGlass"].Contains("true");
            merchant.IsDealerInquiriesDesired = formCollection["IsDealerInquiriesDesired"].Contains("true");
            merchant.IsInstallationServicesOffered = formCollection["IsInstallationServicesOffered"].Contains("true");
            merchant.IsPremierStatus = formCollection["IsPremierStatus"].Contains("true");
            merchant.IsProfessionalTradeInquiriesDesired = formCollection["IsProfessionalTradeInquiriesDesired"].Contains("true");
            merchant.IsRetailInquiriesDesired = formCollection["IsRetailInquiriesDesired"].Contains("true");
            merchant.IsSearchableOnMasoniteWebsite = formCollection["IsSearchableOnMasoniteWebsite"].Contains("true");
            merchant.Latitude = formCollection["Latitude"].ToDouble();
            merchant.Longitude = formCollection["Longitude"].ToDouble();

            // Save Merchant
            merchant.Save(User.Identity.Name);

            // Save MerchantDistributorDealer (non-Max Merchants only)
            if (!merchant.IsActiveInMax)
            {
                List<MerchantDistributorDealer> distributors = ObjectFactory.RetrieveMerchantDistributorDealers(merchant.Id);

                // Loop through this Merchant's distributors, and find the generic Masonite distributor
                foreach(MerchantDistributorDealer item in distributors)
                {
                    if (string.Compare("Masonite Configurator Distributor", item.MerchantDistributorName, true, System.Globalization.CultureInfo.CurrentUICulture) == 0)
                    {
                        // Found the generic Masonite distributor, so save and exit from the loop
                        item.IsDistributorSupplyingProductEntry = formCollection["IsDistributorSupplyingProductEntry"] != null ? true : false;
                        item.IsDistributorSupplyingProductInterior = formCollection["IsDistributorSupplyingProductInterior"] != null ? true : false;
                        item.IsDistributorSupplyingProductPatio = formCollection["IsDistributorSupplyingProductPatio"] != null ? true : false;

                        item.Save(User.Identity.Name);

                        break;
                    }
                }
            }
            
            // Return
            return RedirectToAction("Edit", "Merchant", new { id = merchantId, tab = (int)MerchantEditTab.MasoniteDotCom });
        }
        
        [AcceptVerbs(HttpVerbs.Delete)]
        public void Delete(int id)
        {
            // Delete item
            Merchant merchant = ObjectFactory.RetrieveMerchant(id, User.Identity.Name);
            merchant.Remove(User.Identity.Name);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult HasMasterPackNumber(int id)
        {
            Merchant merchant = ObjectFactory.RetrieveMerchantForAdministration(id);

            return this.Json(new { HasMasterPackNumber = !string.IsNullOrEmpty(merchant.MasterPackId) && string.Compare("0", merchant.MasterPackId, false, System.Globalization.CultureInfo.CurrentUICulture) != 0 });
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult MerchantNameDuplicates(string name)
        {
            List<Merchant> merchants = ObjectFactory.RetrieveMerchantsForAdministration(Server.UrlDecode(name), null, 0, null, null, 0, 0, false, false, true);

            if (merchants.Count > 0)
            {
                return this.Json(new { Id = merchants[0].Id, IsActive = merchants[0].IsActive });
            }
            else
            {
                return this.Json(new { Id = 0, IsActive = false });
            }
        }
    }
}