﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Masonite.MTier.Utility;
using Masonite.MTier.SalesPoint;

namespace SalesPoint.Controllers
{
    public class MerchantTagController : Controller
    {
        [AcceptVerbs(new string[] { "Get", "Post" })]
        public ActionResult IndexPartial(int merchantId, string searchName)
        {
            return PartialView(ObjectFactory.RetrieveMerchantTags(merchantId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Edit(int? id, int merchantId, string text)
        {
            if (ModelState.IsValid)
            {
                // Create or retrieve item
                MerchantTag merchantTag;
                if (id > 0)
                {
                    merchantTag = ObjectFactory.RetrieveMerchantTag((int)(id.HasValue ? id : 0));
                }
                else
                {
                    merchantTag = new MerchantTag();
                }

                // Populate
                merchantTag.MerchantId = merchantId;
                merchantTag.Text = Server.HtmlEncode(text);

                // Save
                merchantTag.Save();
            }

            return this.Json(true);
        }

        [AcceptVerbs(HttpVerbs.Delete)]
        public JsonResult Delete(int id)
        {
            // Delete item
            MerchantTag merchantTag = ObjectFactory.RetrieveMerchantTag(id);
            merchantTag.Remove();

            return this.Json(true);
        }
    }
}