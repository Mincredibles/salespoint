﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Masonite.MTier.Utility;
using Masonite.MTier.SalesPoint;

namespace SalesPoint.Controllers
{
    public class CorrespondenceApprovalController : Controller
    {
        [Authorize(Roles = "SalesPoint Administrators, Marketing Team")]
        [AcceptVerbs(new string[] { "Get", "Post" })]
        public ActionResult IndexPartial(int id)
        {
            // id = CorrespondenceId

            return PartialView(ObjectFactory.RetrieveCorrespondenceApprovals(id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public void Approve(int id)
        {
            CorrespondenceApproval item = new CorrespondenceApproval();
            item.CorrespondenceId = id;
            item.Save(User.Identity.Name);
        }
    }
}