﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Masonite.MTier.Utility;
using Masonite.MTier.SalesPoint;

namespace SalesPoint.Controllers
{
    [Authorize(Roles = "SalesPoint Administrators, Wholesale Administrators")]
    public class UserController : Controller
    {
        [AcceptVerbs(new string[] { "Get", "Post" })]
        public ActionResult Index(string searchName)
        {
            searchName = Server.UrlDecode(searchName);
            ViewData["searchName"] = searchName;

            if (string.IsNullOrEmpty(searchName))
            {
                // If no search criteria provided, then don't show any merchants.
                return View(new List<User>());
            }
            else
            {
                return View(ObjectFactory.RetrieveUsers(searchName));
            }       
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Edit(int? id)
        {
            if (!id.HasValue || id == 0)
            {
                // Adding new item
                ViewData["NewRecordText"] = Core.NewRecordText;
                return View(new User());
            }
            else
            {
                // Editing existing item
                return View(ObjectFactory.RetrieveUser((int)id));
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Edit(int? id, FormCollection formCollection)
        {
            if (ModelState.IsValid)
            {
                // Create or retrieve item
                User user;
                if (id > 0)
                {
                    user = ObjectFactory.RetrieveUser((int)(id.HasValue ? id : 0));
                }
                else
                {
                    user = new User();
                }

                // Populate
                user.ActiveDirectoryLogonName = formCollection["ActiveDirectoryLogonName"];
                user.Address1 = formCollection["Address1"];
                user.Address2 = formCollection["Address2"];
                user.CellPhoneNumber = StringHelper.RemoveNonNumericCharacters(formCollection["CellPhoneNumber"]);
                user.City = formCollection["City"];
                user.Country = formCollection["Country"];
                user.EmailAddress = formCollection["EmailAddress"];
                user.FaxNumber = StringHelper.RemoveNonNumericCharacters(formCollection["FaxNumber"]);
                user.FirstName = formCollection["FirstName"];
                user.IsActive = formCollection["IsActive"].Contains("true");
                user.JobTitle = formCollection["JobTitle"];
                user.LastName = formCollection["LastName"];
                user.MasterPackSalesPersonNumber = formCollection["MasterPackSalesPersonNumber"];
                user.PhoneNumber = StringHelper.RemoveNonNumericCharacters(formCollection["PhoneNumber"]);
                user.PostalCode = formCollection["PostalCode"];
                user.State = formCollection["State"];

                // Save and redirect
                if (user.Save())
                {
                    return RedirectToAction("Index", "User", new { searchName = Server.UrlEncode(user.ActiveDirectoryLogonName) });
                }
            }
            
            // Failed
            return View();
        }

        [AcceptVerbs(HttpVerbs.Delete)]
        public void Delete(int id)
        {
            // Delete item
            User user = ObjectFactory.RetrieveUser(id);
            user.Remove();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult LogonNameDuplicates(string logonName)
        {
            User user = ObjectFactory.RetrieveUser(Server.UrlDecode(logonName));

            return this.Json(new { Id = user.Id, IsActive = user.IsActive });
        }
    }
}