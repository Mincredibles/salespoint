﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Masonite.MTier.Utility;
using Masonite.MTier.SalesPoint;

namespace SalesPoint.Controllers
{
    public class MerchantContactController : Controller
    {
        [AcceptVerbs(new string[] { "Get", "Post" })]
        public ActionResult ChooseContactPartial(string searchName)
        {
            List<MerchantContact> merchantContacts;

            if (searchName != null) { searchName = searchName.Trim(); }

            if (string.IsNullOrEmpty(searchName))
            {
                merchantContacts = new List<MerchantContact>();
            }
            else
            {
                merchantContacts = ObjectFactory.RetrieveMerchantContacts(searchName);
            }

            return PartialView("ChooseContactPartial", merchantContacts);
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult EditPartial(int id, int merchantId)
        {
            ViewData["MerchantId"] = merchantId;
            ViewData["id"] = id;
            MerchantContact contact = null;

            if (id > 0)
            { // Existing contact
                contact = ObjectFactory.RetrieveMerchantContact(id);
            }
            else
            { // New contact
                ViewData["NewRecordText"] = Core.NewRecordText;

                contact = new MerchantContact();

                // Default mailing address to the contact's current location address
                using (Merchant merchant = ObjectFactory.RetrieveMerchant(merchantId, User.Identity.Name))
                {
                    contact.Address = merchant.Address;
                    contact.Address2 = merchant.Address2;
                    contact.City = merchant.City;
                    contact.State = merchant.StateName;
                    contact.PostalCode = merchant.PostalCode;
                }
            }

            // Return
            return PartialView("EditPartial", contact);
        }

        [AcceptVerbs(new string[] { "Get", "Post" })]
        public ActionResult IndexPartial(int merchantId, string searchName)
        {
            ViewData["MerchantId"] = merchantId;
            ViewData["id"] = merchantId;
            ViewData["searchName"] = searchName;

            using (Merchant merchant = ObjectFactory.RetrieveMerchant(merchantId, User.Identity.Name))
            {
                ViewData["MerchantName"] = merchant.Name.Replace("'", "\\'");
                ViewData["MerchantHasConfigureOneParent"] = merchant.Distributors.Count > 0 ? "true" : "false";
            }

            return PartialView("IndexPartial", ObjectFactory.RetrieveMerchantContacts(searchName, null, merchantId));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult IndexPartialMarketing(int merchantId)
        {
            return PartialView("IndexPartial", ObjectFactory.RetrieveMerchantContacts(merchantId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Edit(int? id, int? merchantId, FormCollection formCollection)
        {
            if (ModelState.IsValid)
            {
                ViewData["merchantId"] = merchantId;

                // Create or retrieve item
                MerchantContact merchantContact;
                if (id > 0)
                {
                    merchantContact = ObjectFactory.RetrieveMerchantContact((int)(id.HasValue ? id : 0));
                }
                else
                {
                    merchantContact = new MerchantContact();
                }

                // Populate
                merchantContact.Address = formCollection["Address"];
                merchantContact.Address2 = formCollection["Address2"];
                merchantContact.City = formCollection["City"];
                merchantContact.ConfigureOneTimeZoneId = formCollection["ConfigureOneTimeZoneId"].ToInt32();
                merchantContact.County = formCollection["County"];
                merchantContact.DistributorName = formCollection["DistributorName"];
                merchantContact.EmailAddress = formCollection["EmailAddress"];
                merchantContact.FaxNumber = StringHelper.RemoveNonNumericCharacters(formCollection["FaxNumber"]);
                merchantContact.FirstName = formCollection["FirstName"];
                merchantContact.ImageLibraryGroupId = formCollection["ImageLibraryGroupId"];
                merchantContact.IsAllowedAccessToConfigurator = formCollection["IsAllowedAccessToConfigurator"].Contains("true");
                merchantContact.IsAllowedAccessToDealerSite = formCollection["IsAllowedAccessToDealerSite"].Contains("true");
                merchantContact.IsAllowedAccessToOrderTracking = formCollection["IsAllowedAccessToOrderTracking"].Contains("true");
                merchantContact.IsAllowedAccessToVMIOrderTracking = formCollection["IsAllowedAccessToVMIOrderTracking"].Contains("true");
                merchantContact.IsAllowedAccessToTreasureChest = formCollection["IsAllowedAccessToTreasureChest"].Contains("true");
                merchantContact.IsAllowedAccessToGlassXpress = formCollection["IsAllowedAccessToGlassXpress"].Contains("true");
                merchantContact.IsDecliningMail = formCollection["IsDecliningMail"].Contains("true");
                merchantContact.IsMaxAdministrator = formCollection["IsMaxAdministrator"].Contains("true");
                merchantContact.IsPasswordChangeRequired = formCollection["IsPasswordChangeRequired"].Contains("true");
                merchantContact.IsSalesRebateContact = formCollection["IsSalesRebateContact"].Contains("true");
                merchantContact.JobTitle = formCollection["JobTitle"];
                merchantContact.LastName = formCollection["LastName"];
                merchantContact.MailingAddress = formCollection["MailingAddress"];
                merchantContact.MailingAddress2 = formCollection["MailingAddress2"];
                merchantContact.MailingCity = formCollection["MailingCity"];
                merchantContact.MailingPostalCode = formCollection["MailingPostalCode"];
                merchantContact.MailingState = formCollection["MailingState"];
                if (!string.IsNullOrEmpty(formCollection["MasoniteDotComLogon"]))
                {
                    merchantContact.MasoniteDotComLogon = formCollection["MasoniteDotComLogon"];
                }
                if (!string.IsNullOrEmpty(formCollection["MasoniteDotComPassword"])) { merchantContact.MasoniteDotComPassword = formCollection["MasoniteDotComPassword"]; }
                merchantContact.MasoniteFranceSalespersonNumber = formCollection["MasoniteFranceSalespersonNumber"].Replace(",", "|").Replace(" ", "|").Replace("||", "|");
                merchantContact.MasoniteStoreGroupId = formCollection["MasoniteStoreGroupId"].ToInt32();
                merchantContact.MobileNumber = StringHelper.RemoveNonNumericCharacters(formCollection["MobileNumber"]);
                merchantContact.PhoneNumber = StringHelper.RemoveNonNumericCharacters(formCollection["PhoneNumber"]);
                merchantContact.PostalCode = formCollection["PostalCode"];
                merchantContact.RoleId = formCollection["RoleId"].ToInt32();
                merchantContact.State = formCollection["State"];

                // Save
                try
                {
                    merchantContact.Save((int)merchantId, User.Identity.Name);
                }
                catch (Exception ex)
                {
                    if (ex.Message.StartsWith(Core.ValidationPrefix, true, System.Globalization.CultureInfo.CurrentUICulture))
                    {
                        return this.Json(new { success = false, message = ex.Message });
                    }

                    throw;
                }
                finally
                {

                }
            }

            // Return
            return this.Json(new { success = true, message = "" });
        }

        [AcceptVerbs(HttpVerbs.Delete)]
        public void Delete(int id)
        {
            // Delete item
            MerchantContact merchantContact = ObjectFactory.RetrieveMerchantContact(id);
            merchantContact.Remove(User.Identity.Name);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult ResetPassword(int merchantContactId)
        {
            MerchantContact contact = ObjectFactory.RetrieveMerchantContact(merchantContactId);
            if (contact.IsAllowedAccessToDealerSite)
            {
                contact.ResetPassword(User.Identity.Name, true);
            }

            // Return
            return this.Json(true);
        }
    }
}