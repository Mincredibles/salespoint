﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Masonite.MTier.Utility;
using Masonite.MTier.SalesPoint;

namespace SalesPoint.Controllers
{
    [Authorize(Roles = "SalesPoint Administrators, Home Depot Administrators")]
    public class GeographicalDistrictController : Controller
    {
        [AcceptVerbs(new string[] { "Get", "Post" })]
        public ActionResult Index(string searchName)
        {
            searchName = Server.UrlDecode(searchName);
            ViewData["searchName"] = searchName;
            return View(ObjectFactory.RetrieveGeographicalDistricts(searchName));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Edit(int? id)
        {
            if (!id.HasValue || id == 0)
            {
                // Adding new item
                ViewData["NewRecordText"] = Core.NewRecordText;
                return View(new GeographicalDistrict());
            }
            else
            {
                // Editing existing item
                return View(ObjectFactory.RetrieveGeographicalDistrict((int)id));
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Edit(int? id, FormCollection formCollection)
        {
            if (ModelState.IsValid)
            {
                // Create or retrieve item
                GeographicalDistrict geographicalDistrict;
                if (id > 0)
                {
                    geographicalDistrict = ObjectFactory.RetrieveGeographicalDistrict((int)(id.HasValue ? id : 0));
                }
                else
                {
                    geographicalDistrict = new GeographicalDistrict();
                }

                // Populate
                geographicalDistrict.Code = formCollection["Code"];
                geographicalDistrict.Description = formCollection["Description"];
                geographicalDistrict.GeographicalDistrictTypeId = formCollection["GeographicalDistrictTypeId"].ToInt32();
                geographicalDistrict.IsActive = formCollection["IsActive"].Contains("true");
                geographicalDistrict.Name = formCollection["Name"];
                geographicalDistrict.Number = formCollection["Number"].ToInt32();

                // Save and redirect
                if (geographicalDistrict.Save())
                {
                    return RedirectToAction("Index", "GeographicalDistrict", new { searchName = Server.UrlEncode(geographicalDistrict.Name) });
                }
            }

            // Failed
            return View();
        }

        [AcceptVerbs(HttpVerbs.Delete)]
        public void Delete(int id)
        {
            // Delete item
            GeographicalDistrict geographicalDistrict = ObjectFactory.RetrieveGeographicalDistrict(id);
            geographicalDistrict.Remove();
        }
    }
}
