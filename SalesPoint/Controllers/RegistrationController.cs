﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Masonite.MTier.Utility;
using Masonite.MTier.SalesPoint;

namespace SalesPoint.Controllers
{
    [Authorize(Roles = "SalesPoint Administrators, Marketing Team")]
    public class RegistrationController : Controller
    {
        [AcceptVerbs(new string[] { "Get", "Post" })]
        public ActionResult Index(int? registrationCategoryId, string searchName)
        {
            searchName = Server.UrlDecode(searchName);
            ViewData["searchName"] = searchName;
            int categoryId = registrationCategoryId.HasValue ? (int)registrationCategoryId: 0;
            ViewData["RegistrationCategoryId"] = categoryId;

            return View(ObjectFactory.RetrieveRegistrations(categoryId, searchName));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Edit(int? id)
        {
            if (!id.HasValue || id == 0)
            {
                return RedirectToAction("Index");
            }
            else
            {
                // Editing existing item
                SalesPoint.ViewModels.RegistrationModel model = new ViewModels.RegistrationModel();

                model.Registration = ObjectFactory.RetrieveRegistration((int)id);
                model.RegistrationAttributes = model.Registration.Attributes;

                return View(model);
            }
        }
    }
}