﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Masonite.MTier.SalesPoint;

namespace SalesPoint.Controllers
{
    [Authorize(Roles = "SalesPoint Administrators")]
    public class MerchantChainController : Controller
    {
        [AcceptVerbs(new string[] { "Get", "Post" })]
        public ActionResult Index(string searchName)
        {
            searchName = Server.UrlDecode(searchName);
            ViewData["searchName"] = searchName;
            return View(ObjectFactory.RetrieveMerchantChains(searchName));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Edit(int? id)
        {
            if (!id.HasValue || id == 0)
            {
                // Adding new item
                ViewData["NewRecordText"] = Core.NewRecordText;
                return View(new MerchantChain());
            }
            else
            {
                // Editing existing item
                return View(ObjectFactory.RetrieveMerchantChain((int)id));
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Edit(int? id, FormCollection formCollection)
        {
            if (ModelState.IsValid)
            {
                // Create or retrieve item
                MerchantChain merchantChain;
                if (id > 0)
                {
                    merchantChain = ObjectFactory.RetrieveMerchantChain((int)(id.HasValue ? id : 0));
                }
                else
                {
                    merchantChain = new MerchantChain();
                }

                // Populate
                merchantChain.Code = formCollection["Code"];
                merchantChain.Description = formCollection["Description"];
                merchantChain.IsActive = formCollection["IsActive"].Contains("true");
                merchantChain.Name = formCollection["Name"];

                // Save and redirect
                if (merchantChain.Save())
                {
                    return RedirectToAction("Index", "MerchantChain", new { searchName = Server.UrlEncode(merchantChain.Name) });
                }
            }

            // Failed
            return View();
        }

        [AcceptVerbs(HttpVerbs.Delete)]
        public void Delete(int id)
        {
            // Delete item
            MerchantChain merchantChain = ObjectFactory.RetrieveMerchantChain(id);
            merchantChain.Remove();
        }
    }
}