﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Masonite.MTier.Utility;
using Masonite.MTier.SalesPoint;

namespace SalesPoint.Controllers
{
    public class StructureController : Controller
    {
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult EditPartial(int id, int merchantId)
        {
            ViewData["MerchantId"] = merchantId;
            ViewData["id"] = id;
            Structure item = null;

            if (id > 0)
            { // Existing contact
                item = ObjectFactory.RetrieveStructure(id);
            }
            else
            { // New contact
                ViewData["NewRecordText"] = Core.NewRecordText;

                item = new Structure();
            }

            // Return
            return PartialView("EditPartial", item);
        }

        [AcceptVerbs(new string[] { "Get", "Post" })]
        public ActionResult IndexPartial(int merchantId)
        {
            ViewData["MerchantId"] = merchantId;
            ViewData["id"] = merchantId;

            using (Merchant merchant = ObjectFactory.RetrieveMerchant(merchantId, User.Identity.Name))
            {
                ViewData["MerchantName"] = merchant.Name.Replace("'", "\\'");
            }

            return PartialView("IndexPartial", ObjectFactory.RetrieveStructures(merchantId, (int)StructureClassification.ModelHome));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Edit(int? id, int merchantId, FormCollection formCollection)
        {
            if (ModelState.IsValid)
            {
                ViewData["merchantId"] = merchantId;

                // Create or retrieve item
                Structure item;
                if (id > 0)
                {
                    item = ObjectFactory.RetrieveStructure((int)(id.HasValue ? id : 0));
                }
                else
                {
                    item = new Structure();
                }

                // Populate
                item.Address1 = formCollection["Address1"];
                item.Address2 = formCollection["Address2"];
                item.City = formCollection["City"];
                item.Distributor = formCollection["Distributor"];
                item.ExteriorDoorQuantity = formCollection["ExteriorDoorQuantity"].ToInt32();
                item.ExteriorDoors = formCollection["ExteriorDoors"];
                item.HousingDevelopment = formCollection["HousingDevelopment"];
                item.InteriorDoorQuantity = formCollection["InteriorDoorQuantity"].ToInt32();
                item.InteriorDoors = formCollection["InteriorDoors"];
                item.LotNumber = formCollection["LotNumber"];
                item.MerchantId = merchantId;
                item.PostalCode = formCollection["PostalCode"];
                item.State = formCollection["State"];
                item.StructureCategoryId = (int)StructureClassification.ModelHome;
                
                // Save
                try
                {
                    item.Save(User.Identity.Name);
                }
                catch (Exception ex)
                {
                    if (ex.Message.StartsWith(Core.ValidationPrefix, true, System.Globalization.CultureInfo.CurrentUICulture))
                    {
                        return this.Json(new { success = false, message = ex.Message });
                    }

                    throw;
                }
                finally
                {

                }
            }

            // Return
            return this.Json(new { success = true, message = "" });
        }

        [AcceptVerbs(HttpVerbs.Delete)]
        public void Delete(int id)
        {
            // Delete item
            Structure item = ObjectFactory.RetrieveStructure(id);
            item.Remove();
        }
    }
}