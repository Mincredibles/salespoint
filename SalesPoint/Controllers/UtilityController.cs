﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Masonite.MTier.Utility;
using Masonite.MTier.SalesPoint;

namespace SalesPoint.Controllers
{
    public class UtilityController : Controller
    {
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult PostalCodeLookup(string postalCode)
        {
            int stateId = 0;
            string stateCode = null;

            // Retrieves the City/State/Zip combination that correlates to postalCode
            GeographicalLocation geographicalLocation = ObjectFactory.RetrieveGeographicalLocation(postalCode);

            List<GeographicalState> states = ObjectFactory.RetrieveGeographicalStates(geographicalLocation.StateName, true);
            if (states.Count > 0)
            {
                stateId = states[0].Id;
                stateCode = states[0].Code;
            }

            return this.Json(new
                                {
                                    IsValid = !string.IsNullOrEmpty(geographicalLocation.StateName),
                                    PostalCode = geographicalLocation.PostalCode,
                                    City = geographicalLocation.City,
                                    County = geographicalLocation.County,
                                    Country = geographicalLocation.Country,
                                    StateName = geographicalLocation.StateName,
                                    StateId = stateId,
                                    StateCode = stateCode
                                });

        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult LocationAddressLookup(int merchantId)
        {
            // Retrieves the City/State/Zip combination that correlates to postalCode
            Merchant merchant = ObjectFactory.RetrieveMerchant(merchantId, User.Identity.Name);

            return this.Json(new
            {
                IsValid = !string.IsNullOrEmpty(merchant.Address),
                Address = merchant.Address,
                Address2 = merchant.Address2,
                PostalCode = merchant.PostalCode,
                City = merchant.City,
                County = "",
                Country = merchant.Country,
                StateName = merchant.StateName,
                StateId = 0
            });
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult GetGeoLocation(
                                        string address, 
                                        string city, 
                                        string state, 
                                        string postalCode
                                        )
        {
            
                return this.Json(GeographicCoordinateHelper.GetLocation(address, city, state, postalCode));  
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult GenerateRandomPassword()
        {
            //return this.Json(System.Web.Security.Membership.GeneratePassword(8, 0));
            return this.Json(PasswordHelper.GenerateRandomPassword(0));
        }
    }
}