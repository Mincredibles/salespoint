﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Masonite.MTier.SalesPoint;

namespace SalesPoint.Controllers
{
    public class MerchantMerchantContactController : Controller
    {
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult IndexMerchantsPartial(int merchantContactId, int currentMerchantId)
        {
            // If a value is provided for currentMerchantId, then the Merchant with that ID will not be rendered in the HTML output.  To
            // render all Merchants, pass 0.

            ViewData["MerchantId"] = currentMerchantId;
            return PartialView(ObjectFactory.RetrieveMerchantMerchantContacts(0, merchantContactId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public void Edit(int merchantId, int merchantContactId)
        {
            // Attempt to retrieve the existing relationship, if one exists
            MerchantMerchantContact item = ObjectFactory.RetrieveMerchantMerchantContact(merchantId, merchantContactId);

            // If an existing relationship already exists, then exit (no save necessary)
            if (item != null && item.MerchantId > 0)
            {
                return;
            }
            
            // Populate
            item = new MerchantMerchantContact();
            item.MerchantId = merchantId;
            item.MerchantContactId = merchantContactId;

            // Save
            item.Save(User.Identity.Name);
        }

        [AcceptVerbs(HttpVerbs.Delete)]
        public JsonResult Delete(int merchantId, int merchantContactId)
        {
            // Delete item
            MerchantMerchantContact item = ObjectFactory.RetrieveMerchantMerchantContact(merchantId, merchantContactId);
            item.Remove();

            return this.Json(true);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult SetPrimary(int merchantId, int merchantContactId)
        {
            // Retrieve and update item
            MerchantMerchantContact item = ObjectFactory.RetrieveMerchantMerchantContact(merchantId, merchantContactId);
            item.IsPrimaryMerchantForContact = true;
            item.Save(User.Identity.Name);

            return this.Json(true);
        }
    }
}