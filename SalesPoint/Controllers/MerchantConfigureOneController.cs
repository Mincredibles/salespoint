﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Masonite.MTier.Utility;
using Masonite.MTier.SalesPoint;

namespace SalesPoint.Controllers
{
    [Authorize(Roles = "SalesPoint Administrators, Wholesale Administrators")]
    public class MerchantConfigureOneController : Controller
    {
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult EditPartial(int merchantId)
        {
            ViewData["MerchantId"] = merchantId;

            using (Merchant merchant = ObjectFactory.RetrieveMerchant(merchantId, User.Identity.Name))
            {
                ViewData["MerchantName"] = merchant.Name.Replace("'", "\\'");
                ViewData["IsActiveInMax"] = merchant.IsActiveInMax;
            }

            return PartialView("EditPartial", ObjectFactory.RetrieveMerchantConfigureOne(merchantId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Edit(int merchantId, MerchantConfigureOne model)
        {
            // Do some validations
            if (string.Compare("Cost Multiplier", model.PricingMethod, false, System.Globalization.CultureInfo.CurrentUICulture) == 0)
            {
                if (model.PricingDefault < 0)
                {
                    ModelState.AddModelError("PricingDefault", "Pricing Default cannot be negative when Cost Multiplier is selected.");
                }
            }
            if (string.Compare("Gross Margin", model.PricingMethod, false, System.Globalization.CultureInfo.CurrentUICulture) == 0)
            {
                if (model.PricingDefault < 0 || model.PricingDefault > (decimal)99.99999999999)
                {
                    ModelState.AddModelError("PricingDefault", "Pricing Default must be between 0 and 99.9999 when Gross Margin is selected.");
                }
                if (string.Compare("P", model.PricingDefaultCategory, false, System.Globalization.CultureInfo.CurrentUICulture) != 0)
                {
                    ModelState.AddModelError("PricingDefaultCategory", "You cannot choose a dollar value when Gross Margin is selected.");
                }
            }

            if (ModelState.IsValid)
            {
                MerchantConfigureOne item = new MerchantConfigureOne();

                // Populate
                item.ExternalDealerNumber = model.ExternalDealerNumber;
                item.IsPermissionCanEditOtherMembersData = model.IsPermissionCanEditOtherMembersData;
                item.IsPermissionCanReadOtherMembersData = model.IsPermissionCanReadOtherMembersData;
                item.IsPermissionCanEditDescendantData = model.IsPermissionCanEditDescendantData;
                item.IsPermissionCanReadDescendantData = model.IsPermissionCanReadDescendantData;
                item.MerchantId = merchantId;
                item.PricingDefault = model.PricingDefault;
                item.PricingDefaultCategory = model.PricingDefaultCategory;
                item.PricingMethod = model.PricingMethod;
                item.QuoteDuration = model.QuoteDuration;
                item.StandardDisclaimer = model.StandardDisclaimer;
                item.TaxRate1 = model.TaxRate1;
                item.TaxRate1Extension = model.TaxRate1Extension;
                item.TaxRate2 = model.TaxRate2;
                item.TaxRate2Extension = model.TaxRate2Extension;
                item.TaxRate3 = model.TaxRate3;
                item.TaxRate3Extension = model.TaxRate3Extension;

                // Save
                item.Save(User.Identity.Name);
            }

            ViewData["MerchantId"] = merchantId;
            using (Merchant merchant = ObjectFactory.RetrieveMerchant(merchantId, User.Identity.Name))
            {
                ViewData["MerchantName"] = merchant.Name.Replace("'", "\\'");
                ViewData["IsActiveInMax"] = merchant.IsActiveInMax;
            }

            return PartialView("EditPartial", model);
        }
    }
}