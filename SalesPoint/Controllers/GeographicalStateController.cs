﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Masonite.MTier.Utility;
using Masonite.MTier.SalesPoint;

namespace SalesPoint.Controllers
{
    public class GeographicalStateController : Controller
    {
        [AcceptVerbs(new string[] { "Get", "Post" })]
        public ActionResult Index(string searchName)
        {
            searchName = Server.UrlDecode(searchName);
            ViewData["searchName"] = searchName;
            return View(ObjectFactory.RetrieveGeographicalStates(searchName, false));
        }

        [AcceptVerbs(new string[] { "Get", "Post" })]
        public ActionResult IndexPartial(string controlName, bool includeBlankItem, string selectedItemId, bool disableControl)
        {
            ViewData["controlName"] = controlName;
            ViewData["includeBlankItem"] = includeBlankItem;
            ViewData["selectedItemId"] = selectedItemId;
            ViewData["disableControl"] = disableControl;
            return PartialView(ObjectFactory.RetrieveGeographicalStates());
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Edit(int? id)
        {
            if (!id.HasValue || id == 0)
            {
                // Adding new item
                ViewData["NewRecordText"] = Core.NewRecordText;
                return View(new GeographicalState());
            }
            else
            {
                // Editing existing item
                return View(ObjectFactory.RetrieveGeographicalState((int)id));
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Edit(int? id, FormCollection formCollection)
        {
            if (ModelState.IsValid)
            {
                // Create or retrieve item
                GeographicalState geographicalState;
                if (id > 0)
                {
                    geographicalState = ObjectFactory.RetrieveGeographicalState((int)(id.HasValue ? id : 0));
                }
                else
                {
                    geographicalState = new GeographicalState();
                }

                // Populate
                geographicalState.Code = formCollection["Code"];
                geographicalState.Country = formCollection["Country"];
                geographicalState.Description = formCollection["Description"];
                geographicalState.IsActive = formCollection["IsActive"].Contains("true");
                geographicalState.Name = formCollection["Name"];

                // Save and redirect
                if (geographicalState.Save())
                {
                    return RedirectToAction("Index", "GeographicalState", new { searchName = Server.UrlEncode(geographicalState.Name) });
                }
            }

            // Failed
            return View();
        }

        [AcceptVerbs(HttpVerbs.Delete)]
        public void Delete(int id)
        {
            // Delete item
            GeographicalState geographicalState = ObjectFactory.RetrieveGeographicalState(id);
            geographicalState.Remove();
        }
    }
}
