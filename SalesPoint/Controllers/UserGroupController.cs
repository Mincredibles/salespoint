﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Masonite.MTier.SalesPoint;

namespace SalesPoint.Controllers
{
    [Authorize(Roles = "SalesPoint Administrators, Wholesale Administrators")]
    public class UserGroupController : Controller
    {
        [AcceptVerbs(new string[] { "Get", "Post" })]
        public ActionResult Index(string searchName)
        {
            searchName = Server.UrlDecode(searchName);
            ViewData["searchName"] = searchName;
            return View(ObjectFactory.RetrieveUserGroups(searchName, -1, 0 , User.Identity.Name));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Edit(int? id)
        {
            if (!id.HasValue || id == 0)
            {
                // Adding new item
                ViewData["NewRecordText"] = Core.NewRecordText;
                return View(new UserGroup());
            }
            else
            {
                // Editing existing item
                return View(ObjectFactory.RetrieveUserGroup((int)id, User.Identity.Name));
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Edit(int? id, FormCollection formCollection)
        {
            if (ModelState.IsValid)
            {
                // Create or retrieve item
                UserGroup userGroup;
                if (id > 0)
                {
                    userGroup = ObjectFactory.RetrieveUserGroup((int)(id.HasValue ? id : 0), User.Identity.Name);
                }
                else
                {
                    userGroup = new UserGroup();
                }

                // Populate
                userGroup.Description = formCollection["Description"];
                userGroup.Name = formCollection["Name"];
                
                // Save and redirect
                if (userGroup.Save())
                {
                    return RedirectToAction("Index", "UserGroup", new { searchName = Server.UrlEncode(userGroup.Name) });
                }
            }

            // Failed
            return View();
        }

        [AcceptVerbs(HttpVerbs.Delete)]
        public void Delete(int id)
        {
            // Delete item
            UserGroup userGroup = ObjectFactory.RetrieveUserGroup(id, User.Identity.Name);
            userGroup.Remove();
        }
    }
}