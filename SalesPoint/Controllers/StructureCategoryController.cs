﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Masonite.MTier.Utility;
using Masonite.MTier.SalesPoint;

namespace SalesPoint.Controllers
{
    [Authorize(Roles = "SalesPoint Administrators")]
    public class StructureCategoryController : Controller
    {
        [AcceptVerbs(new string[] { "Get", "Post" })]
        public ActionResult Index(string searchName)
        {
            searchName = Server.UrlDecode(searchName);
            ViewData["searchName"] = searchName;
            return View(ObjectFactory.RetrieveStructureCategories(searchName));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Edit(int? id)
        {
            if (!id.HasValue || id == 0)
            {
                // Adding new item
                ViewData["NewRecordText"] = Core.NewRecordText;
                return View(new StructureCategory());
            }
            else
            {
                // Editing existing item
                return View(ObjectFactory.RetrieveStructureCategory((int)id));
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Edit(int? id, FormCollection formCollection)
        {
            if (ModelState.IsValid)
            {
                // Create or retrieve item
                StructureCategory item;
                if (id > 0)
                {
                    item = ObjectFactory.RetrieveStructureCategory((int)(id.HasValue ? id : 0));
                }
                else
                {
                    item = new StructureCategory();
                }

                // Populate
                item.Code = formCollection["Code"];
                item.Description = formCollection["Description"];
                item.IsActive = formCollection["IsActive"].Contains("true");
                item.Name = formCollection["Name"];

                // Save and redirect
                if (item.Save(User.Identity.Name))
                {
                    return RedirectToAction("Index", "StructureCategory", new { searchName = Server.UrlEncode(item.Name) });
                }
            }

            // Failed
            return View();
        }

        [AcceptVerbs(HttpVerbs.Delete)]
        public void Delete(int id)
        {
            // Delete item
            StructureCategory item = ObjectFactory.RetrieveStructureCategory(id);
            item.Remove(User.Identity.Name);
        }
    }
}
