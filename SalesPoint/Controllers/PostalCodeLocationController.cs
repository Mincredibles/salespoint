﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Masonite.MTier.SalesPoint;

namespace SalesPoint.Controllers
{
    public class PostalCodeLocationController : Controller
    {
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Index(string searchPostalCode)
        {
            return PartialView(ObjectFactory.RetrievePostalCodeLocations(searchPostalCode));
        }
    }
}
