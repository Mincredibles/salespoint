﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Masonite.MTier.Utility;
using Masonite.MTier.SalesPoint;

namespace SalesPoint.Controllers
{
    [Authorize(Roles = "SalesPoint Administrators, Max Price Book Publishers")]
    public class ConfigurationController : Controller
    {
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Edit()
        {
            return View(ObjectFactory.RetrieveConfiguration());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Edit(FormCollection formCollection)
        {
            if (ModelState.IsValid)
            {
                // Create or retrieve item
                Configuration configuration = ObjectFactory.RetrieveConfiguration();

                // Populate
                configuration.IsMaxEnabled = formCollection["IsMaxEnabled"].Contains("true");

                // Save and redirect
                if (configuration.Save())
                {
                    return RedirectToAction("Edit");
                }
            }

            // Failed
            return View();
        }
    }
}