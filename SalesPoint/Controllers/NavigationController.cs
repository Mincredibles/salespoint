﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Masonite.MTier.SalesPoint;

namespace SalesPoint.Controllers
{
    [Authorize()]
    public class NavigationController : Controller
    {
        [Authorize(Roles = "SalesPoint Administrators")]
        public ActionResult AdministrationLeft(Core.SelectedNavigationAdministrationLeft selectedMenuItem)
        {
            ViewData["SelectedMenuItem"] = selectedMenuItem;

            return View();
        }
    }
}
