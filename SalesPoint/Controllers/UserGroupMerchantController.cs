﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Masonite.MTier.Utility;
using Masonite.MTier.SalesPoint;

namespace SalesPoint.Controllers
{
    [Authorize(Roles = "SalesPoint Administrators, Wholesale Administrators")]
    public class UserGroupMerchantController : Controller
    {
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult IndexUserGroupPartial(int merchantId)
        {
            ViewData["MerchantId"] = merchantId;

            using (Merchant merchant = ObjectFactory.RetrieveMerchant(merchantId, User.Identity.Name))
            {
                ViewData["MerchantName"] = merchant.Name.Replace("'", "\\'");
            }
            
            System.Collections.Generic.List<UserGroup> userGroups = ObjectFactory.RetrieveUserGroups(merchantId, User.Identity.Name, true);
            return PartialView("IndexUserGroupPartial", userGroups);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult IndexMerchant(string searchName, int userGroupId)
        {
            ViewData["UserGroupId"] = userGroupId;
            List<Merchant> merchants = ObjectFactory.RetrieveMerchantsForAdministration(searchName, null, 0, null, null, userGroupId, 0, false, false, false);
            return View("IndexMerchant", merchants);
        }

        [AcceptVerbs(new string[] { "Get", "Post" })]
        public ActionResult ChooseMerchantsPartial(string searchName)
        {
            List<Merchant> merchants;

            if (string.IsNullOrEmpty(searchName))
            {
                merchants = new List<Merchant>();
            }
            else{
                merchants = ObjectFactory.RetrieveMerchantsForAdministration(searchName, null, 0, null, null, 0, 0, false, false, false);
            }
             
            return PartialView("ChooseMerchantsPartial", merchants);
        }

        [AcceptVerbs(new string[] { "Get", "Post" })]
        public ActionResult ChooseUserGroupsPartial(string searchName)
        {
            List<UserGroup> userGroups;

            if (string.IsNullOrEmpty(searchName))
            {
                userGroups = new List<UserGroup>();
            }
            else
            {
                userGroups = ObjectFactory.RetrieveUserGroups(searchName, User.Identity.Name);
            }
            return PartialView("ChooseUserGroupsPartial", userGroups);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public void Edit(int id, int merchantId)
        {
            // Create or retrieve item
            UserGroupMerchant userGroupMerchant = new UserGroupMerchant();

            // Populate
            userGroupMerchant.UserGroupId = id;
            userGroupMerchant.MerchantId = merchantId;

            // Save
            userGroupMerchant.Save();
        }

        [AcceptVerbs(HttpVerbs.Delete)]
        public void Delete(int id, int merchantId)
        {
            // Delete item
            UserGroupMerchant userGroupMerchant = new UserGroupMerchant();
            userGroupMerchant.UserGroupId = id;
            userGroupMerchant.MerchantId = merchantId;
            userGroupMerchant.Remove();
        }
    }
}