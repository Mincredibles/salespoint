﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Masonite.MTier.Utility;
using Masonite.MTier.SalesPoint;

namespace SalesPoint.Controllers
{
    [Authorize(Roles = "SalesPoint Administrators")]
    public class ConfigureOnePriceBookController : Controller
    {
        [AcceptVerbs(new string[] { "Get", "Post" })]
        public ActionResult Index(string searchName)
        {
            searchName = Server.UrlDecode(searchName);
            ViewData["searchName"] = searchName;
            return View(ObjectFactory.RetrieveConfigureOnePriceBooks(searchName));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult IndexPartial(int? configureOnePriceBookId)
        {
            // No longer showing the pricebook dropdownlist because it can only be set from within Max
            ViewData["ConfigureOnePriceBookId"] = (int)configureOnePriceBookId;

            return PartialView(ObjectFactory.RetrieveConfigureOnePriceBooks());
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Edit(int? id)
        {
            if (!id.HasValue || id == 0)
            {
                // Adding new item
                ViewData["NewRecordText"] = Core.NewRecordText;
                return View(new ConfigureOnePriceBook());
            }
            else
            {
                // Editing existing item
                return View(ObjectFactory.RetrieveConfigureOnePriceBook((int)id));
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Edit(int? id, FormCollection formCollection)
        {
            if (ModelState.IsValid)
            {
                // Create or retrieve item
                ConfigureOnePriceBook configureOnePriceBook;
                if (id > 0)
                {
                    configureOnePriceBook = ObjectFactory.RetrieveConfigureOnePriceBook((int)(id.HasValue ? id : 0));
                }
                else
                {
                    configureOnePriceBook = new ConfigureOnePriceBook();
                }

                // Populate
                configureOnePriceBook.Code = formCollection["Code"];
                configureOnePriceBook.Description = formCollection["Description"];
                configureOnePriceBook.IsActive = formCollection["IsActive"].Contains("true");
                configureOnePriceBook.Name = formCollection["Name"];

                // Save and redirect
                if (configureOnePriceBook.Save())
                {
                    return RedirectToAction("Index", "ConfigureOnePriceBook", new { searchName = Server.UrlEncode(configureOnePriceBook.Name) });
                }
            }

            // Failed
            return View();
        }

        [AcceptVerbs(HttpVerbs.Delete)]
        public void Delete(int id)
        {
            // Delete item
            ConfigureOnePriceBook configureOnePriceBook = ObjectFactory.RetrieveConfigureOnePriceBook(id);
            configureOnePriceBook.Remove();
        }
    }
}
