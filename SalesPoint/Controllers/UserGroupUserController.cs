﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Masonite.MTier.Utility;
using Masonite.MTier.SalesPoint;

namespace SalesPoint.Controllers
{
    [Authorize(Roles = "SalesPoint Administrators, Wholesale Administrators")]
    public class UserGroupUserController : Controller
    {
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Index(int? id)
        {
            ViewData["UserGroupId"] = id;
            UserGroup userGroup = ObjectFactory.RetrieveUserGroup((int)id, User.Identity.Name);
            return View(userGroup.Users);
        }

        [AcceptVerbs(new string[] { "Get", "Post" })]
        public ActionResult ChooseUsersPartial(string searchName)
        {
            List<User> users;

            if (string.IsNullOrEmpty(searchName))
            {
                users = new List<User>();
            }
            else
            {
                users = ObjectFactory.RetrieveUsers(searchName);
            }
             
            return PartialView("ChooseUsersPartial", users);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Edit(int? id)
        {
            return View(new System.Collections.Generic.List<User>());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Edit(int? id, FormCollection formCollection)
        {
            if (ModelState.IsValid)
            {
                // Create or retrieve item
                UserGroupUser userGroupUser = new UserGroupUser();

                // Populate
                userGroupUser.UserGroupId = formCollection["UserGroupId"].ToInt32();
                userGroupUser.UserId = formCollection["UserId"].ToInt32();
                
                // Save and redirect
                if (userGroupUser.Save())
                {
                    return RedirectToAction("Index");
                }
            }

            // Failed
            return View();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public void EditFromPartial(int id, int userId)
        {
            // Create or retrieve item
            UserGroupUser userGroupUser = new UserGroupUser();

            // Populate
            userGroupUser.UserGroupId = id;
            userGroupUser.UserId = userId;

            // Save and redirect
            userGroupUser.Save();
        }

        [AcceptVerbs(HttpVerbs.Delete)]
        public void Delete(int id, int userId)
        {
            // Delete item
            UserGroupUser userGroupUser = new UserGroupUser();
            userGroupUser.UserGroupId = id;
            userGroupUser.UserId = userId;
            userGroupUser.Remove();

        }
    }
}