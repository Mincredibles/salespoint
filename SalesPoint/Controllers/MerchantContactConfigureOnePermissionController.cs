﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Masonite.MTier.Utility;
using Masonite.MTier.SalesPoint;

namespace SalesPoint.Controllers
{
    [Authorize(Roles = "SalesPoint Administrators, Max Team")]
    public class MerchantContactConfigureOnePermissionController : Controller
    {
        [ChildActionOnly]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult IndexPartial(int userId)
        {
            ViewData["UserId"] = userId;
            return PartialView(ObjectFactory.RetriveMerchantContactPermission(userId));
        }
    }
}
