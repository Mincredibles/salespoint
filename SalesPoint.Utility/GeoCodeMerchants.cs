﻿using System;
using System.Collections.Generic;
using Masonite.MTier.Utility;
using Masonite.MTier.Customers;

namespace SalesPoint.Utility
{
    class GeoCodeMerchants
    {
        static void Main(string[] args)
        {
            Console.Write("Enter the number of Merchant records to GeoCode on this pass: ");
            int number = Console.ReadLine().ToInt32();

            List<Merchant> merchants = ObjectFactory.RetrieveMerchants(1);

            foreach (Merchant merchant in merchants)
            {
                // If no Merchant returned, then no more Merchants exist that need geo coding
                if (merchant == null) { Console.Write("All Merchants have been GeoCoded."); break; }
                if (merchant.Id == 0) { Console.Write("All Merchants have been GeoCoded."); break; }

                System.Threading.Thread.Sleep(3000);

                Console.WriteLine("Geo-coding ID: " + merchant.Id);

                GeographicCoordinate coordinate = GeographicCoordinateHelper.GetLocation(merchant.Address1, merchant.City, merchant.CountrySubdivision, merchant.PostalCode);
                if (coordinate.Latitude != 0 && coordinate.Longitude != 0)
                {
                    merchant.Latitude = (decimal)coordinate.Latitude;
                    merchant.Longitude = (decimal)coordinate.Longitude;

                    try
                    {
                        merchant.Save("SalesPoint utility account");
                    }
                    catch (Exception ex)
                    {
                        // Ignore error
                    }

                    Console.WriteLine("ID " + merchant.Id + " Completed.");
                }
            }

            //for (int i = 0; i < number; i++)
            //{
            //    using (Merchant merchant = ObjectFactory.RetrieveMerchant(74))
            //    {
            //        // If no Merchant returned, then no more Merchants exist that need geo coding
            //        if (merchant == null) { Console.Write("All Merchants have been GeoCoded."); break; }
            //        if (merchant.Id == 0) { Console.Write("All Merchants have been GeoCoded."); break; }

            //        GeographicCoordinate coordinate = GeographicCoordinateHelper.GetLocation(merchant.Address1, merchant.City, merchant.CountrySubdivision, merchant.PostalCode);
            //        if (coordinate.Latitude != 0 && coordinate.Longitude != 0)
            //        {
            //            merchant.Latitude = (decimal)coordinate.Latitude;
            //            merchant.Longitude = (decimal)coordinate.Longitude;

            //            try
            //            {
            //                merchant.Save("SalesPoint utility account");
            //            }
            //            catch (Exception ex)
            //            {
            //                // Ignore error
            //            }

            //            Console.WriteLine("Geo-coded id " + merchant.Id);
            //        }
            //    }
            //}

            Console.Write("Finished GeoCoding.  Press [Enter] to close window.");
            Console.ReadLine();
        }
    }
}